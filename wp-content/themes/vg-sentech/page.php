<?php
/**
 * @version    1.1
 * @package    VG Sentech
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */
$sentech_options  = sentech_get_global_variables();

sentech_get_header();
?>
<?php 
	$pagesidebar = 'right';
	if(isset($sentech_options['sidebarse_pos']) && $sentech_options['sidebarse_pos']!=''){
	 $pagesidebar = $sentech_options['sidebarse_pos'];
	}
	if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	 $pagesidebar = $_GET['sidebar'];
	}
?>
<div class="main-container default-page">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<?php sentech_breadcrumb(); ?>
			</div>
			
			<?php if($pagesidebar=='left') : ?>
				<?php get_sidebar('page'); ?>
			<?php endif; ?>
			
			<div class="col-xs-12 <?php if (is_active_sidebar('sidebar-page')) : ?>col-md-9<?php endif; ?>">
				<div class="page-content default-page">
					<?php while (have_posts()) : the_post(); ?>
						<?php get_template_part('content', 'page'); ?>
						<?php comments_template('', true); ?>
					<?php endwhile; // end of the loop. ?>
				</div>
			</div>
			
			<?php if($pagesidebar=='left') : ?>
				<?php get_sidebar('page'); ?>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php sentech_get_footer(); ?>
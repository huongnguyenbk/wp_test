<?php 
/**
* The functions
* @package d4jtheme
* @subpackage d4jtheme
* @since d4jtheme 1.0
*/
require_once ( get_template_directory() . '/includes/menu.php');
require_once ( get_template_directory() . '/includes/sidebars.php');
require_once ( get_template_directory() . '/includes/themeoptions.php');
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'post-thumbnails' ); 
add_image_size( 'cat-thumbnails', 180, 120, true );
add_image_size( 'left-thumbnails', 350, 144, true );
add_filter( 'wp_title', 'baw_hack_wp_title_for_home' );
function baw_hack_wp_title_for_home( $title )
{
  if( empty( $title ) && ( is_home() || is_front_page() ) ) {
    return __( 'Home', 'theme_domain' ) . ' | ' . get_bloginfo( 'description' );
  }
  return $title;
}
function be_mobile_menu() {
	wp_nav_menu( array(
		'theme_location' => 'main_menu',
		'walker'         => new Walker_Nav_Menu_Dropdown(),
		'items_wrap'     => '<div class="mobile-menu"><form><select onchange="if (this.value) window.location.href=this.value"><option value="'.home_url().'">Select menu</option>%3$s</select></form></div>',
	) );	
}
add_action( 'genesis_before_header', 'be_mobile_menu' );
class Walker_Nav_Menu_Dropdown extends Walker_Nav_Menu {
	function start_lvl(&$output, $depth = 0, $args = array()){
		$indent = str_repeat("\t", $depth); // don't output children opening tag (`<ul>`)
	}

	function end_lvl(&$output, $depth = 0, $args = array()){
		$indent = str_repeat("\t", $depth); // don't output children closing tag
	}

	/**
	* Start the element output.
	*
	* @param  string $output Passed by reference. Used to append additional content.
	* @param  object $item   Menu item data object.
	* @param  int $depth     Depth of menu item. May be used for padding.
	* @param  array $args    Additional strings.
	* @return void
	*/
	function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
 		$url = '#' !== $item->url ? $item->url : '';
		$current_url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		$selected = '';
		if($url == $current_url){$selected = 'selected="selected"';}
 		$output .= '<option '.$selected.' value="' . $url . '">' . $item->title;
	}	

	function end_el(&$output, $item, $depth = 0, $args = array(), $id = 0){
		$output .= "</option>\n"; // replace closing </li> with the option tag
	}
}
function kriesi_pagination($pages = '', $range = 2)
{  
     $showitems = ($range * 2)+1;  

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   

     if(1 != $pages)
     {
         echo "<div class='pagination'>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo;</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a>";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a>";
             }
         }

         if ($paged < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($paged + 1)."'>&rsaquo;</a>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>&raquo;</a>";
         echo "</div>\n";
     }
}

<?php

/**
  ReduxFramework Sample Config File
  For full documentation, please visit: https://docs.reduxframework.com
 * */

if (!class_exists('sentech_theme_config')) {

    class sentech_theme_config {

        public $args        = array();
        public $sections    = array();
        public $theme;
        public $ReduxFramework;

        public function __construct() {

            if (!class_exists('ReduxFramework')) {
                return;
            }

            // This is needed. Bah WordPress bugs.  ;)
            if (true == Redux_Helpers::isTheme(__FILE__)) {
                $this->initSettings();
            } else {
                add_action('plugins_loaded', array($this, 'initSettings'), 10);
            }

        }

        public function initSettings() {

            // Just for demo purposes. Not needed per say.
            $this->theme = wp_get_theme();

            // Set the default arguments
            $this->setArguments();

            // Set a few help tabs so you can see how it's done
            $this->setHelpTabs();

            // Create the sections and fields
            $this->setSections();

            if (!isset($this->args['opt_name'])) { // No errors please
                return;
            }
            $this->ReduxFramework = new ReduxFramework($this->sections, $this->args);
        }

        /**

          This is a test function that will let you see when the compiler hook occurs.
          It only runs if a field	set with compiler=>true is changed.

         * */
        function compiler_action($options, $css, $changed_values) {
            echo '<h1>The compiler hook has run!</h1>';
            echo "<pre>";
            print_r($changed_values); // Values that have changed since the last save
            echo "</pre>";
        }

        /**

          Custom function for filtering the sections array. Good for child themes to override or add to the sections.
          Simply include this function in the child themes functions.php file.

          NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
          so you must use get_template_directory_uri() if you want to use any of the built in icons

         * */
        function dynamic_section($sections) {
            //$sections = array();
            $sections[] = array(
                'title' => esc_html__('Section via hook', 'vg-sentech'),
                'desc' => wp_kses(__('<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'vg-sentech'), array('p' => array())),
                'icon' => 'el-icon-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array()
			);

            return $sections;
        }

        /**

          Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.

         * */
        function change_arguments($args) {
            //$args['dev_mode'] = true;

            return $args;
        }

        /**

          Filter hook for filtering the default value of any given field. Very useful in development mode.

         * */
        function change_defaults($defaults) {
            $defaults['str_replace'] = 'Testing filter hook!';

            return $defaults;
        }

        // Remove the demo link and the notice of integrated demo from the redux-framework plugin
        function remove_demo() {

            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if (class_exists('ReduxFrameworkPlugin')) {
                remove_filter('plugin_row_meta', array(ReduxFrameworkPlugin::instance(), 'plugin_metalinks'), null, 2);

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action('admin_notices', array(ReduxFrameworkPlugin::instance(), 'admin_notices'));
            }
        }

        public function setSections() {

            /**
              Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
             * */
            // Background Patterns Reader
            $sample_patterns_path   = ReduxFramework::$_dir . '../sample/patterns/';
            $sample_patterns_url    = ReduxFramework::$_url . '../sample/patterns/';
            $sample_patterns        = array();

            if (is_dir($sample_patterns_path)) :

                if ($sample_patterns_dir = opendir($sample_patterns_path)) :
                    $sample_patterns = array();

                    while (($sample_patterns_file = readdir($sample_patterns_dir)) !== false) {

                        if (stristr($sample_patterns_file, '.png') !== false || stristr($sample_patterns_file, '.jpg') !== false) {
                            $name = explode('.', $sample_patterns_file);
                            $name = str_replace('.' . end($name), '', $sample_patterns_file);
                            $sample_patterns[]  = array('alt' => $name, 'img' => $sample_patterns_url . $sample_patterns_file);
                        }
                    }
                endif;
            endif;

            ob_start();

            $ct             = wp_get_theme();
            $this->theme    = $ct;
            $item_name      = $this->theme->get('Name');
            $tags           = $this->theme->Tags;
            $screenshot     = $this->theme->get_screenshot();
            $class          = $screenshot ? 'has-screenshot' : '';

            $customize_title = sprintf(esc_html__('Customize &#8220;%s&#8221;', 'vg-sentech'), $this->theme->display('Name'));
            
            ?>
            <div id="current-theme" class="<?php echo esc_attr($class); ?>">
            <?php if ($screenshot) : ?>
                <?php if (current_user_can('edit_theme_options')) : ?>
                        <a href="<?php echo wp_customize_url(); ?>" class="load-customize hide-if-no-customize" title="<?php echo esc_attr($customize_title); ?>">
                            <img src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_attr_e('Current theme preview', 'vg-sentech'); ?>" />
                        </a>
                <?php endif; ?>
                    <img class="hide-if-customize" src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_attr_e('Current theme preview', 'vg-sentech'); ?>" />
                <?php endif; ?>

                <h4><?php echo esc_html($this->theme->display('Name')); ?></h4>

                <div>
                    <ul class="theme-info">
                        <li><?php printf(esc_html__('By %s', 'vg-sentech'), $this->theme->display('Author')); ?></li>
                        <li><?php printf(esc_html__('Version %s', 'vg-sentech'), $this->theme->display('Version')); ?></li>
                        <li><?php echo '<strong>' . esc_html__('Tags', 'vg-sentech') . ':</strong> '; ?><?php printf($this->theme->display('Tags')); ?></li>
                    </ul>
                    <p class="theme-description"><?php echo $this->theme->display('Description'); ?></p>
            <?php
            if ($this->theme->parent()) {
                 printf(' <p class="howto">' . wp_kses(__('This <a href="%1$s">child theme</a> requires its parent theme, %2$s.', 'vg-sentech'), array('a' => array('href' => array(),'title' => array()))) . '</p>', esc_html__('http://codex.wordpress.org/Child_Themes', 'vg-sentech'), $this->theme->parent()->display('Name'));
            }
            ?>

                </div>
            </div>

            <?php
            $item_info = ob_get_contents();

            ob_end_clean();

            $sampleHTML = '';
            if (file_exists( get_template_directory() . '/info-html.html')) {
                Redux_Functions::initWpFilesystem();
                
                global $wp_filesystem;

                $sampleHTML = $wp_filesystem->get_contents( get_template_directory() . '/info-html.html');
            }
			
			/**
			 *	General
			 **/
            $this->sections[] = array(
                'title'     => esc_html__('General', 'vg-sentech'),
                'desc'      => esc_html__('General theme options', 'vg-sentech'),
                'icon'      => 'el-icon-cog',
                'fields'    => array(

                    array(
                        'id'        => 'logo_main',
                        'type'      => 'media',
                        'title'     => esc_html__('Logo', 'vg-sentech'),
                        'compiler'  => 'true',
                        'mode'      => false,
                        'desc'      => esc_html__('Upload logo here.', 'vg-sentech'),
					),
					array(
                        'id'        => 'logo_text',
                        'type'      => 'text',
                        'title'     => esc_html__('Logo Text', 'vg-sentech'),
                        'default'   => ''
					),
					array(
                        'id'        => 'logo_erorr',
                        'type'      => 'media',
                        'title'     => esc_html__('Logo for error 404 page', 'vg-sentech'),
                        'compiler'  => 'true',
                        'mode'      => false,
					),
					array(
                        'id'        => 'background_opt',
                        'type'      => 'background',
                        'output'    => array('body'),
                        'title'     => esc_html__('Background', 'vg-sentech'),
                        'subtitle'  => esc_html__('Body background with image, color. Only work with box layout', 'vg-sentech'),
						'default'   => '#ffffff',
					),				
					array(
						'id'		=>'share_head_code',
						'type' 		=> 'textarea',
						'title' 	=> esc_html__('ShareThis/AddThis head tag', 'vg-sentech'), 
						'desc' 		=> esc_html__('Paste your ShareThis or AddThis head tag here', 'vg-sentech'),
						'default' 	=> '<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-553dd7dd1ff880d4" async="async"></script>',
					),
					array(
						'id'		=>'share_code',
						'type' 		=> 'textarea',
						'title' 	=> esc_html__('ShareThis/AddThis code', 'vg-sentech'), 
						'desc' 		=> esc_html__('Paste your ShareThis or AddThis code here', 'vg-sentech'),
						'default' 	=> '<div class="addthis_native_toolbox"></div>'
					),
					array(
                        'id'        => 'sentech_show_author',
                        'type'      => 'switch',
                        'title'     => esc_html__('Show Author Info Page', 'vg-sentech'),
						'default'   => false,
					),		
				),
			);

			//Header
			$this->sections[] = array(
				'title'     => esc_html__('Header', 'vg-sentech'),
				'desc'      => esc_html__('Header options', 'vg-sentech'),
				'icon'      => 'el-icon-tasks',
				'subsection' => true,
				'fields'     => array(
					array(
                        'id'        => 'title_mobile_menu',
                        'type'      => 'text',
                        'title'     => esc_html__('Title Mobile Menu', 'vg-sentech'),
                        'default'   => 'Menu'
					),
					array(
                        'id'        => 'vg_top_contact',
                        'type'      => 'editor',
                        'title'     => esc_html__('Top Contact', 'vg-sentech'),
                        'default'   => '<div class="block-call"><p class="call-us-title">Call us free:</p><span class="call-us-number">(801) 2345 - 6789</span></div>'
					),						
					array(
                        'id'        => 'vg_top_contact_show',
                        'type'      => 'switch',
                        'title'     => esc_html__('Show Top Contact', 'vg-sentech'),
						'default'   => true,
					),
                    array(
                        'id'        => 'show_menu_max',
                        'type'      => 'switch',
                        'title'     => esc_html__('Show Max Category Product Item', 'vg-sentech'),
                        'default'   => true,
                    ),
                    array(
                        'id'        => 'title_more_categories',
                        'type'      => 'text',
                        'title'     => esc_html__('Title More Categories', 'vg-sentech'),
                        'default'   => 'More Categories'
                    ),
                    array(
                        'id'        => 'title_close_menu',
                        'type'      => 'text',
                        'title'     => esc_html__('Title Close Menu', 'vg-sentech'),
                        'default'   => 'Close Menu'
                    ),  
                    array(
                        'id'        => 'menu_max_number',
                        'type'      => 'text',
                        'title'     => esc_html__('Max number item', 'vg-sentech'),
                        'default'   => '7'
                    ),
					
					
					
				),
			);	

			//Footer
			$this->sections[] = array(
                'title'     => esc_html__('Footer', 'vg-sentech'),
                'desc'      => esc_html__('Footer options', 'vg-sentech'),
                'icon'      => 'el-icon-tasks',
				'subsection' => true,
                'fields'    => array(
					array(
                        'id'        => 'top_bottom_show',
                        'type'      => 'switch',
                        'title'     => esc_html__('Top Bottom', 'vg-sentech'),
						'default'   => true,
					),
					array(
                        'id'        => 'top_bottom_content',
                        'type'      => 'editor',
                        'title'     => esc_html__('Top Bottom Content', 'vg-sentech'),
                        'default'   => '
						<div class="col-sm-3 col-md-3 col-sms-6 col-smb-12"><div class="block-wrapper"><span class="fa fa-rocket">&nbsp;</span><div class="media-body"><h3>Free Shipping</h3>on order over $100</div></div></div>
						<div class="col-sm-3 col-md-3 col-sms-6 col-smb-12"><div class="block-wrapper"><span class="fa fa-umbrella">&nbsp;</span><div class="media-body"><h3>24/7 Support</h3>online consultations</div></div></div>
						<div class="col-sm-3 col-md-3 col-sms-6 col-smb-12"><div class="block-wrapper"><span class="fa fa-calendar">&nbsp;</span><div class="media-body"><h3>Daily updates</h3>Check out store for lastes</div></div></div>
						<div class="col-sm-3 col-md-3 col-sms-6 col-smb-12"><div class="block-wrapper"><span class="fa fa-refresh">&nbsp;</span><div class="media-body"><h3>30-day returns</h3>oneyback guarantee</div></div></div>						
						'
                    ),	
					array(
                        'id'        => 'about_us_block',
                        'type'      => 'switch',
                        'title'     => esc_html__('About Us', 'vg-sentech'),
						'default'   => true,
					),
					array(
                        'id'        => 'about_us_title',
                        'type'      => 'text',
                        'title'     => esc_html__('About Us title', 'vg-sentech'),
                        'default'   => 'About Us'
                    ),	
					array(
                        'id'        => 'about_us_des',
                        'type'      => 'editor',
                        'title'     => esc_html__('About Us Description', 'vg-sentech'),
                        'default'   => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.'
                    ),

					array(
                        'id'        => 'contact_info_block',
                        'type'      => 'switch',
                        'title'     => esc_html__('Contact info', 'vg-sentech'),
						'default'   => true,
					),
					array(
                        'id'        => 'contact_info_title',
                        'type'      => 'text',
                        'title'     => esc_html__('Contact info title', 'vg-sentech'),
                        'default'   => 'Contact Info'
                    ),	
					array(
                        'id'        => 'contact_info_des',
                        'type'      => 'editor',
                        'title'     => esc_html__('Contact info Description', 'vg-sentech'),
                        'default'   => '<div class="footer-static-content">
											<ul class="link-contact">
												<li>
													<span class="address_icon contact-icon">1</span>
													<span class="all_wrapper">Address:<span class="text-address">123 Pall Mall, London England</span></span>
												</li>		
												<li>
													<span class="phone_icon contact-icon">2</span>
													<span class="all_wrapper">Phone:<span class="text-address">(012) 345 6789</span></span>
												</li>	
												<li>
													<span class="email_icon contact-icon">3</span>
													<span class="all_wrapper">Email:<span class="text-address">info@plazathemes.com</span></span>
												</li>
											</ul>
										</div>'
                    ),

					array(
                        'id'        => 'map_info_block',
                        'type'      => 'switch',
                        'title'     => esc_html__('Map Block', 'vg-sentech'),
						'default'   => true,
					),	
					array(
                        'id'        => 'map_info_des',
                        'type'      => 'editor',
                        'title'     => esc_html__('Map Description', 'vg-sentech'),
                        'default'   => '<div class="gmap"  data-address="V Tytana St, Manila, Philippines">Gmap</div>'
                    ),					
					
					array(
                        'id'        => 'copyright_show',
                        'type'      => 'switch',
                        'title'     => esc_html__('Show Copyright', 'vg-sentech'),
						'default'   => true,
					),
					array(
                        'id'        => 'copyright-notice',
                        'type'      => 'textarea',
                        'title'     => esc_html__('Copyright Notice', 'vg-sentech'),
                        'default'   => 'Copyright (C) 2016 {VinaGecko.com}. All Rights Reserved.'
					),
					array(
                        'id'        => 'copyright-link',
                        'type'      => 'text',
                        'title'     => esc_html__('Copyright Link', 'vg-sentech'),
                        'default'   => 'http://vinagecko.com'
					),
					array(
                        'id'        => 'footer_payment',
                        'type'      => 'media',
                        'title'     => esc_html__('Image Payment', 'vg-sentech'),
                        'compiler'  => 'true',
                        'mode'      => false,
                        'desc'      => esc_html__('Upload Image Payment here.', 'vg-sentech'),
						'default'  => array( 'url' => get_template_directory_uri() . '/images/payment.png' ),
					),
				),
			);			
						
			//Social network
            $this->sections[] = array(
                'title'     => esc_html__('Social Icons', 'vg-sentech'),
                'desc'      => esc_html__('Social Icons options', 'vg-sentech'),
                'icon'      => 'el-icon-globe-alt',
				'subsection' => true,				
                'fields'    => array(
					array(
                        'id'        => 'social_show',
                        'type'      => 'switch',
                        'title'     => esc_html__('Show Social', 'vg-sentech'),
						'default'   => true,
					),
					array(
						'id'       => 'ftsocial_icons',
						'type'     => 'sortable',
						'title'    => esc_html__('Footer social Icons', 'vg-sentech'),
						'subtitle' => esc_html__('Enter social links', 'vg-sentech'),
						'desc'     => esc_html__('Drag/drop to re-arrange', 'vg-sentech'),
						'mode'     => 'text',
						'options'  => array(
							'facebook'    => 'https://www.facebook.com/vinawebsolutions',
							'twitter'     => 'https://twitter.com/vnwebsolutions',
							'instagram'   => 'Instagram',
							'tumblr'      => 'Tumblr',
							'pinterest'   => 'Pinterest',
							'google-plus' => 'https://plus.google.com/',
							'linkedin'    => 'Linkedin',
							'behance'     => 'Behance',
							'dribbble'    => 'Dribbble',
							'youtube'     => 'https://youtube.com/',
							'vimeo'       => 'Vimeo',
							'rss'         => 'RSS',
						),
						'default' => array(
						    'facebook'    => 'https://www.facebook.com/vinawebsolutions',
							'twitter'     => 'https://twitter.com/vnwebsolutions',
							'instagram'   => '',
							'tumblr'      => '',
							'pinterest'   => '',
							'google-plus' => 'https://plus.google.com/+HieuJa/posts',
							'linkedin'    => '',
							'behance'     => '',
							'dribbble'    => '',
							'youtube'     => 'https://www.youtube.com/user/vinawebsolutions',
							'vimeo'       => '',
							'rss'         => '',
						),
					),			
				)
			);									

			// Newsletter
            $this->sections[] = array(
                'title'     => esc_html__('Newsletter', 'vg-sentech'),
                'desc'      => esc_html__('Newsletter options', 'vg-sentech'),
                'icon'      => 'el-icon-envelope',
				'subsection' => true,				
                'fields'    => array(			
					array(
                        'id'        => 'newsletter_show',
                        'type'      => 'switch',
                        'title'     => esc_html__('Show Newsletter', 'vg-sentech'),
						'default'   => true,
					),
					array(
                        'id'        => 'newsletter_title',
                        'type'      => 'text',
                        'title'     => esc_html__('Newsletter title', 'vg-sentech'),
                        'default'   => 'NEWSLETTER'
                    ),	
					array(
                        'id'        => 'newsletter_des',
                        'type'      => 'editor',
                        'title'     => esc_html__('Newsletter Description', 'vg-sentech'),
                        'default'   => 'Subscribe to the Sentech mailing list to receive updates on new arrivals, special offers and other discount information.'
                    ),
					array(
						'id'       => 'newsletter_form',
						'type'     => 'text',
						'title'    => esc_html__('Newsletter form ID', 'vg-sentech'),
						'subtitle' => esc_html__('The form ID of MailPoet plugin.', 'vg-sentech'),
						'validate' => 'numeric',
						'msg'      => 'Please enter a form ID',
						'default'  => '1'
					),										
				)
			);																
			
			/**
			 *	Presets Manager
			 **/
            $this->sections[] = array(
                'title'     => esc_html__('Presets Manager', 'vg-sentech'),
                'desc'      => esc_html__('Presets options', 'vg-sentech'),
                'icon'      => 'el-icon-tint',
			);
			$this->sections[] = array(
                'title'     	=> esc_html__('Presets1', 'vg-sentech'),
                'desc'     		=> esc_html__('Presets1 options', 'vg-sentech'),
                'icon'      	=> 'el-icon-tint',
				'subsection' 	=> true,
                'fields'    	=> array(
					array(
                        'id'        	=> 'primary_color',
                        'type'      	=> 'link_color',
                        'title'     	=> esc_html__('Primary Color', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for primary color (default: Regular: #009bc2, Hover: #ff5858 ).', 'vg-sentech'),
						'active' 		=> false,
						'visited' 		=> false,
						'default'  => array(
							'regular'  => '#009bc2',
							'hover'    => '#ff5858', 
						)
					),
                    array(
                        'id'            => 'primary_color2',
                        'type'          => 'color',
                        'title'         => esc_html__('Primary Color 2', 'vg-sentech'),
                        'subtitle'      => esc_html__('Pick a color for primary color (default: #ff5858).', 'vg-sentech'),
                        'transparent'   => false,
                        'default'       => '#ff5858',
                        'validate'      => 'color',
                    ),				
					array(
                        'id'        	=> 'text_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Text Color Body', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for Text color Body (default: #333).', 'vg-sentech'),
						'transparent' 	=> false,
						'output'  	  	=> array('body.preset-1'),
                        'default'   	=> '#333',
                        'validate'  	=> 'color',
					),						
					array(
                        'id'        	=> 'top_header_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Color Top Header', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background color top header(default: #3e3e3e).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#3e3e3e',
                        'validate'  	=> 'color',
					),		
					array(
                        'id'        	=> 'header_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Color Header', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background color header(default: #ebebeb).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#ebebeb',
                        'validate'  	=> 'color',
					),		
					array(
                        'id'        	=> 'menu_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Color Menu', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background color Menu(default: #3a3d42).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#3a3d42',
                        'validate'  	=> 'color',
					),
					array(
                        'id'        	=> 'cart_color',
                        'type'      	=> 'link_color',
                        'title'     	=> esc_html__('Background Cart', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background cart (default: Regular: #009bc2, Hover: #ff5858 ).', 'vg-sentech'),			
						'active' 		=> false,
						'visited' 		=> false,
						'default'  => array(
							'regular'  => '#009bc2',
							'hover'    => '#ff5858', 
						)
					),		
					array(
                        'id'        	=> 'botom_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Position Bottom', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background position bottom (default: #2d3035).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#2d3035',
                        'validate'  	=> 'color',
					),			
					array(
                        'id'        	=> 'footer_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Position Footer', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background position Footer (default: #2d3035).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#2d3035',
                        'validate'  	=> 'color',
					),		
					array(
                        'id'        	=> 'buttonqwc_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Color Button Quickview,Wishlist,Compare', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for Background Color Button Quickview,Wishlist,Compare (default: #464646).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#464646',
                        'validate'  	=> 'color',
					),	
					array(
                        'id'        	=> 'button_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Color Button Carousel', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background color button carousel (default: #636363).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#636363',
                        'validate'  	=> 'color',
					),		
					array(
                        'id'        	=> 'pagination_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Color Pagination Carousel and border content product', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background color pagination carousel and border content product (default: #ebebeb).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#ebebeb',
                        'validate'  	=> 'color',
					),		
					array(
                        'id'        	=> 'sale_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Color Label Sale', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for color Label Sale (default: #ff5858).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#ff5858',
                        'validate'  	=> 'color',
					),					
					array(
                        'id'        	=> 'featured_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Color Label Hot', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for color Label Hot (default: #3698db).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#3698db',
                        'validate'  	=> 'color',
					),
					array(
                        'id'        	=> 'rate_color',
                        'type'      	=> 'color',
                        //'output'    	=> array(),
                        'title'     	=> esc_html__('Rating Star Color', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for star of rating (default: #f4ca49).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'  		=> '#f4ca49',
                        'validate'  	=> 'color',
					),
				),
			);			

			$this->sections[] = array(
                'title'     	=> esc_html__('Presets2', 'vg-sentech'),
                'desc'     		=> esc_html__('Presets2 options', 'vg-sentech'),
                'icon'      	=> 'el-icon-tint',
				'subsection' 	=> true,
                'fields'    	=> array(
					array(
                        'id'        	=> 'primary2_color',
                        'type'      	=> 'link_color',
                        'title'     	=> esc_html__('Primary Color', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for primary color (default: Regular: #6bc160, Hover: #ff6d00 ).', 'vg-sentech'),
						'active' 		=> false,
						'visited' 		=> false,
						'default'  => array(
							'regular'  => '#6bc160',
							'hover'    => '#ff6d00', 
						)
					),
                    array(
                        'id'            => 'primary2_color2',
                        'type'          => 'color',
                        'title'         => esc_html__('Primary Color 2', 'vg-sentech'),
                        'subtitle'      => esc_html__('Pick a color for primary color (default: #ff5858).', 'vg-sentech'),
                        'transparent'   => false,
                        'default'       => '#ff5858',
                        'validate'      => 'color',
                    ),					
					array(
                        'id'        	=> 'text2_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Text Color Body', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for Text color Body (default: #333).', 'vg-sentech'),
						'transparent' 	=> false,
						'output'  	  	=> array('body.preset-2'),
                        'default'   	=> '#333',
                        'validate'  	=> 'color',
					),
					array(
                        'id'        	=> 'top_header2_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Color Top Header', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background color top header(default: #3e3e3e).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#3e3e3e',
                        'validate'  	=> 'color',
					),		
					array(
                        'id'        	=> 'header2_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Color Header', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background color header(default: #ebebeb).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#ebebeb',
                        'validate'  	=> 'color',
					),		
					array(
                        'id'        	=> 'menu2_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Color Menu', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background color Menu(default: #3a3d42).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#3a3d42',
                        'validate'  	=> 'color',
					),
					array(
                        'id'        	=> 'cart2_color',
                        'type'      	=> 'link_color',
                        'title'     	=> esc_html__('Background Cart', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background cart (default: Regular: #6bc160, Hover: #ff6d00 ).', 'vg-sentech'),	
						'transparent' 	=> false,						
						'active' 		=> false,
						'visited' 		=> false,
						'default'  => array(
							'regular'  => '#6bc160',
							'hover'    => '#ff6d00', 
						)
					),	
					array(
                        'id'        	=> 'botom2_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Position Bottom', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background position bottom (default: #2d3035).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#2d3035',
                        'validate'  	=> 'color',
					),			
					array(
                        'id'        	=> 'footer2_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Position Footer', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background position Footer (default: #2d3035).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#2d3035',
                        'validate'  	=> 'color',
					),		
					array(
                        'id'        	=> 'buttonqwc2_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Color Button Quickview,Wishlist,Compare', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for Background Color Button Quickview,Wishlist,Compare (default: #464646).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#464646',
                        'validate'  	=> 'color',
					),	
					array(
                        'id'        	=> 'button2_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Color Button Carousel', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background color button carousel (default: #636363).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#636363',
                        'validate'  	=> 'color',
					),	
					array(
                        'id'        	=> 'pagination2_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Color Pagination Carousel and border content product', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background color pagination carousel and border content product (default: #ebebeb).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#ebebeb',
                        'validate'  	=> 'color',
					),							
					array(
                        'id'        	=> 'sale2_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Color Label Sale', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for color Label Sale (default: #ff6d00).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#ff6d00',
                        'validate'  	=> 'color',
					),		
					array(
                        'id'        	=> 'featured2_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Color Label Hot', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for color Label Hot (default: #3698db).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#3698db',
                        'validate'  	=> 'color',
					),
					array(
                        'id'        	=> 'rate2_color',
                        'type'      	=> 'color',
                        //'output'    	=> array(),
                        'title'     	=> esc_html__('Rating Star Color', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for star of rating (default: #f4ca49).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'  		=> '#f4ca49',
                        'validate'  	=> 'color',
					),
				),
			);		

			$this->sections[] = array(
                'title'     	=> esc_html__('Presets3', 'vg-sentech'),
                'desc'     		=> esc_html__('Presets3 options', 'vg-sentech'),
                'icon'      	=> 'el-icon-tint',
				'subsection' 	=> true,
                'fields'    	=> array(
					array(
                        'id'        	=> 'primary3_color',
                        'type'      	=> 'link_color',
                        'title'     	=> esc_html__('Primary Color', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for primary color (default: Regular: #ff9803, Hover: #ff5858 ).', 'vg-sentech'),
						'active' 		=> false,
						'visited' 		=> false,
						'default'  => array(
							'regular'  => '#ff9803',
							'hover'    => '#ff5858', 
						)
					),
                    array(
                        'id'            => 'primary3_color2',
                        'type'          => 'color',
                        'title'         => esc_html__('Primary Color 2', 'vg-sentech'),
                        'subtitle'      => esc_html__('Pick a color for primary color (default: #ff5858).', 'vg-sentech'),
                        'transparent'   => false,
                        'default'       => '#ff5858',
                        'validate'      => 'color',
                    ),					
					array(
                        'id'        	=> 'text3_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Text Color Body', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for Text color Body (default: #333).', 'vg-sentech'),
						'transparent' 	=> false,
						'output'  	  	=> array('body.preset-3'),
                        'default'   	=> '#333',
                        'validate'  	=> 'color',
					),	
					array(
                        'id'        	=> 'top_header3_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Color Top Header', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background color top header(default: #3e3e3e).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#3e3e3e',
                        'validate'  	=> 'color',
					),		
					array(
                        'id'        	=> 'header3_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Color Header', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background color header(default: #ebebeb).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#ebebeb',
                        'validate'  	=> 'color',
					),		
					array(
                        'id'        	=> 'menu3_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Color Menu', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background color Menu(default: #3a3d42).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#3a3d42',
                        'validate'  	=> 'color',
					),
					array(
                        'id'        	=> 'cart3_color',
                        'type'      	=> 'link_color',
                        'title'     	=> esc_html__('Background Cart', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background cart (default: Regular: #ff9803, Hover: #ff5858 ).', 'vg-sentech'),	
						'transparent' 	=> false,						
						'active' 		=> false,
						'visited' 		=> false,
						'default'  => array(
							'regular'  => '#ff9803',
							'hover'    => '#ff5858', 
						)
					),		
					array(
                        'id'        	=> 'botom3_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Position Bottom', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background position bottom (default: #2d3035).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#2d3035',
                        'validate'  	=> 'color',
					),			
					array(
                        'id'        	=> 'footer3_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Position Footer', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background position Footer (default: #2d3035).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#2d3035',
                        'validate'  	=> 'color',
					),
					array(
                        'id'        	=> 'buttonqwc3_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Color Button Quickview,Wishlist,Compare', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for Background Color Button Quickview,Wishlist,Compare (default: #464646).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#464646',
                        'validate'  	=> 'color',
					),						
					array(
                        'id'        	=> 'button3_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Color Button Carousel', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background color button carousel (default: #636363).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#636363',
                        'validate'  	=> 'color',
					),		
					array(
                        'id'        	=> 'pagination3_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Color Pagination Carousel and border content product', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background color pagination carousel and border content product (default: #ebebeb).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#ebebeb',
                        'validate'  	=> 'color',
					),		
					array(
                        'id'        	=> 'sale3_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Color Label Sale', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for color Label Sale (default: #ff5858).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#ff5858',
                        'validate'  	=> 'color',
					),		
					array(
                        'id'        	=> 'featured3_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Color Label Hot', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for color Label Hot (default: #3698db).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#3698db',
                        'validate'  	=> 'color',
					),
					array(
                        'id'        	=> 'rate3_color',
                        'type'      	=> 'color',
                        //'output'    	=> array(),
                        'title'     	=> esc_html__('Rating Star Color', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for star of rating (default: #f4ca49).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'  		=> '#f4ca49',
                        'validate'  	=> 'color',
					),
				),
			);	
			
			$this->sections[] = array(
                'title'     	=> esc_html__('Presets4', 'vg-sentech'),
                'desc'     		=> esc_html__('Presets4 options', 'vg-sentech'),
                'icon'      	=> 'el-icon-tint',
				'subsection' 	=> true,
                'fields'    	=> array(	
					array(
                        'id'        	=> 'primary4_color',
                        'type'      	=> 'link_color',
                        'title'     	=> esc_html__('Primary Color', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for primary color (default: Regular: #f36b6b, Hover: #ff5858 ).', 'vg-sentech'),
						'active' 		=> false,
						'visited' 		=> false,
						'default'  => array(
							'regular'  => '#f36b6b',
							'hover'    => '#ff5858', 
						)
					),	
                    array(
                        'id'            => 'primary4_color2',
                        'type'          => 'color',
                        'title'         => esc_html__('Primary Color 2', 'vg-sentech'),
                        'subtitle'      => esc_html__('Pick a color for primary color (default: #ff5858).', 'vg-sentech'),
                        'transparent'   => false,
                        'default'       => '#ff5858',
                        'validate'      => 'color',
                    ),					
					array(
                        'id'        	=> 'text4_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Text Color Body', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for Text color Body (default: #333).', 'vg-sentech'),
						'transparent' 	=> false,
						'output'  	  	=> array('body.preset-4'),
                        'default'   	=> '#333',
                        'validate'  	=> 'color',
					),
					array(
                        'id'        	=> 'top_header4_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Color Top Header', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background color top header(default: #3e3e3e).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#3e3e3e',
                        'validate'  	=> 'color',
					),		
					array(
                        'id'        	=> 'header4_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Color Header', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background color header(default: #ebebeb).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#ebebeb',
                        'validate'  	=> 'color',
					),		
					array(
                        'id'        	=> 'menu4_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Color Menu', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background color Menu(default: #3a3d42).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#3a3d42',
                        'validate'  	=> 'color',
					),
					array(
                        'id'        	=> 'cart4_color',
                        'type'      	=> 'link_color',
                        'title'     	=> esc_html__('Background Cart', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background cart (default: Regular: #f36b6b, Hover: #ff5858 ).', 'vg-sentech'),			
						'active' 		=> false,
						'visited' 		=> false,
						'default'  => array(
							'regular'  => '#f36b6b',
							'hover'    => '#ff5858', 
						)
					),		
					array(
                        'id'        	=> 'botom4_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Position Bottom', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background position bottom (default: #2d3035).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#2d3035',
                        'validate'  	=> 'color',
					),			
					array(
                        'id'        	=> 'footer4_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Position Footer', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background position Footer (default: #2d3035).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#2d3035',
                        'validate'  	=> 'color',
					),		
					array(
                        'id'        	=> 'buttonqwc4_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Color Button Quickview,Wishlist,Compare', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for Background Color Button Quickview,Wishlist,Compare (default: #464646).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#464646',
                        'validate'  	=> 'color',
					),	
					array(
                        'id'        	=> 'button4_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Color Button Carousel', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background color button carousel (default: #fd7329).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#fd7329',
                        'validate'  	=> 'color',
					),	
					array(
                        'id'        	=> 'pagination4_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Color Pagination Carousel and border content product', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background color pagination carousel and border content product(default: #ffa200).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#ffa200',
                        'validate'  	=> 'color',
					),							
					array(
                        'id'        	=> 'sale4_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Color Label Sale', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for color Label Sale (default: #ff5858).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#ff5858',
                        'validate'  	=> 'color',
					),		
					array(
                        'id'        	=> 'featured4_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Color Label Hot', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for color Label Hot (default: #3698db).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#3698db',
                        'validate'  	=> 'color',
					),
					array(
                        'id'        	=> 'rate4_color',
                        'type'      	=> 'color',
                        //'output'    	=> array(),
                        'title'     	=> esc_html__('Rating Star Color', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for star of rating (default: #f4ca49).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'  		=> '#f4ca49',
                        'validate'  	=> 'color',
					),
				),
			);	

			$this->sections[] = array(
                'title'     	=> esc_html__('Presets5', 'vg-sentech'),
                'desc'     		=> esc_html__('Presets5 options', 'vg-sentech'),
                'icon'      	=> 'el-icon-tint',
				'subsection' 	=> true,
                'fields'    	=> array(	
					array(
                        'id'        	=> 'primary5_color',
                        'type'      	=> 'link_color',
                        'title'     	=> esc_html__('Primary Color', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for primary color (default: Regular: #33b7ad, Hover: #ff5858 ).', 'vg-sentech'),
						'active' 		=> false,
						'visited' 		=> false,
						'default'  => array(
							'regular'  => '#33b7ad',
							'hover'    => '#ff5858', 
						)
					),
                    array(
                        'id'            => 'primary5_color2',
                        'type'          => 'color',
                        'title'         => esc_html__('Primary Color 2', 'vg-sentech'),
                        'subtitle'      => esc_html__('Pick a color for primary color (default: #ff5858).', 'vg-sentech'),
                        'transparent'   => false,
                        'default'       => '#ff5858',
                        'validate'      => 'color',
                    ),					
					array(
                        'id'        	=> 'text5_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Text Color Body', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for Text color Body (default: #333).', 'vg-sentech'),
						'transparent' 	=> false,
						'output'  	  	=> array('body.preset-5'),
                        'default'   	=> '#333',
                        'validate'  	=> 'color',
					),
					array(
                        'id'        	=> 'top_header5_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Color Top Header', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background color top header(default: #3e3e3e).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#3e3e3e',
                        'validate'  	=> 'color',
					),		
					array(
                        'id'        	=> 'header5_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Color Header', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background color header(default: #ebebeb).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#ebebeb',
                        'validate'  	=> 'color',
					),		
					array(
                        'id'        	=> 'menu5_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Color Menu', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background color Menu(default: #3a3d42).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#3a3d42',
                        'validate'  	=> 'color',
					),
					array(
                        'id'        	=> 'cart5_color',
                        'type'      	=> 'link_color',
                        'title'     	=> esc_html__('Background Cart', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background cart (default: Regular: #33b7ad, Hover: #ff5858 ).', 'vg-sentech'),			
						'active' 		=> false,
						'visited' 		=> false,
						'default'  => array(
							'regular'  => '#33b7ad',
							'hover'    => '#ff5858', 
						)
					),		
					array(
                        'id'        	=> 'botom5_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Position Bottom', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background position bottom (default: #2d3035).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#2d3035',
                        'validate'  	=> 'color',
					),			
					array(
                        'id'        	=> 'footer5_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Position Footer', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background position Footer (default: #2d3035).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#2d3035',
                        'validate'  	=> 'color',
					),		
					array(
                        'id'        	=> 'buttonqwc5_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Color Button Quickview,Wishlist,Compare', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for Background Color Button Quickview,Wishlist,Compare (default: #464646).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#464646',
                        'validate'  	=> 'color',
					),	
					array(
                        'id'        	=> 'button5_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Color Button Carousel', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background color button carousel (default: #636363).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#636363',
                        'validate'  	=> 'color',
					),	
					array(
                        'id'        	=> 'pagination5_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Color Pagination Carousel and border content product', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for background color pagination carousel and border content product(default: #ffa200).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#ffa200',
                        'validate'  	=> 'color',
					),							
					array(
                        'id'        	=> 'sale5_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Color Label Sale', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for color Label Sale (default: #ff5858).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#ff5858',
                        'validate'  	=> 'color',
					),		
					array(
                        'id'        	=> 'featured5_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Color Label Hot', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for color Label Hot (default: #3698db).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'   	=> '#3698db',
                        'validate'  	=> 'color',
					),
					array(
                        'id'        	=> 'rate5_color',
                        'type'      	=> 'color',
                        //'output'    	=> array(),
                        'title'     	=> esc_html__('Rating Star Color', 'vg-sentech'),
                        'subtitle'  	=> esc_html__('Pick a color for star of rating (default: #f4ca49).', 'vg-sentech'),
						'transparent' 	=> false,
                        'default'  		=> '#f4ca49',
                        'validate'  	=> 'color',
					),
				),
			);				
																																		
			/**
			 *	Layout
			 **/
            $this->sections[] = array(
                'title'     => esc_html__('Layout', 'vg-sentech'),
                'icon'      => 'el-icon-align-justify',
                'fields'    => array(
					array(
						'id'       => 'page_layout',
						'subtitle' => esc_html__('Select page default page layout.', 'vg-sentech'),
						'type'     => 'select',
						'multi'    => false,
						'title'    => esc_html__('Page Layout', 'vg-sentech'),
						'options'  => array(
							'layout-1' => 'Page Layout 01',
							'layout-2' => 'Page Layout 02',
							'layout-3' => 'Page Layout 03',
							'layout-4' => 'Page Layout 04',
							'layout-5' => 'Page Layout 05',
							'layout-6' => 'Page Layout 06',
						),
						'default'  => 'layout-3'
					),		
					array(
						'id'       => 'page_style',
						'subtitle' => esc_html__('Select layout style: Box or Full Width', 'vg-sentech'),
						'type'     => 'select',
						'multi'    => false,
						'title'    => esc_html__('Layout Style', 'vg-sentech'),
						'options'  => array(
							'full' => 'Full Width',
							'box'  => 'Box'
						),
						'default'  => 'full'
					),	
					array(
                        'id'        => 'preset_option',
                        'type'      => 'select',
                        'title'     => esc_html__('Preset', 'vg-sentech'),
						'subtitle'      => esc_html__('Select a preset to quickly apply pre-defined colors and fonts', 'vg-sentech'),
                        'options'   => array(
							'1' => 'Preset 1',
                            '2' => 'Preset 2',
							'3' => 'Preset 3',
							'4' => 'Preset 4',
							'5' => 'Preset 5',
                      ),
                        'default'   => '3'
					),
					array(
                        'id'        => 'enable_sswitcher',
                        'type'      => 'switch',
                        'title'     => esc_html__('Show Style Switcher', 'vg-sentech'),
						'subtitle'  => esc_html__('The style switcher is only for preview on front-end', 'vg-sentech'),
						'default'   => false,
					),
				),
			);

			/**
			 *	Sidebar
			 **/			
			$this->sections[] = array(
                'title'     => esc_html__('Sidebar', 'vg-sentech'),
                'desc'      => esc_html__('Sidebar options', 'vg-sentech'),
                'icon'      => 'el-icon-th-large',
                'fields'    => array(
					array(
						'id'       	=> 'sidebar_pos',
						'type'     	=> 'radio',
						'title'    	=> esc_html__('Main Sidebar Position', 'vg-sentech'),
						'subtitle'      => esc_html__('Sidebar on category page', 'vg-sentech'),
						'options'  	=> array(
							'left' 	=> 'Left',
							'right' => 'Right'),
						'default'  	=> 'left'
					),
					array(
						'id'       	=> 'sidebar_product',
						'type'     	=> 'radio',
						'title'    	=> esc_html__('Product Sidebar Position', 'vg-sentech'),
						'subtitle'      => esc_html__('Sidebar on product page', 'vg-sentech'),
						'options'  	=> array(
							'left' 	=> 'Left',
							'right' => 'Right'),
						'default'  	=> 'right'
					),
					array(
						'id'       	=> 'sidebarse_pos',
						'type'     	=> 'radio',
						'title'    	=> esc_html__('Secondary Sidebar Position', 'vg-sentech'),
						'subtitle'  => esc_html__('Sidebar on pages', 'vg-sentech'),
						'options'  	=> array(
							'left' 	=> 'Left',
							'right' => 'Right'),
						'default'  	=> 'left'
					),
					array(
						'id'       	=> 'sidebarblog_pos',
						'type'     	=> 'radio',
						'title'    	=> esc_html__('Blog Sidebar Position', 'vg-sentech'),
						'subtitle'  => esc_html__('Sidebar on Blog pages', 'vg-sentech'),
						'options'   => array(
							   'left'  => 'Left',
							   'right' => 'Right',
							   'nosidebar'  => 'Nosidebar',
							   'fullwidth'  => 'Fullwidth',
							  ),							
						'default'  	=> 'right'
					),
				),
			);

			/**
			 *	Typography
			 **/			
			$this->sections[] = array(
                'title'     => esc_html__('Typography', 'vg-sentech'),
                'desc'      => esc_html__('Typography options', 'vg-sentech'),
                'icon'      => 'el-icon-font',
                'fields'    => array(

                    array(
                        'id'            	=> 'bodyfont',
                        'type'          	=> 'typography',
                        'title'         	=> esc_html__('Body font', 'vg-sentech'),
                        //'compiler'      	=> true,  // Use if you want to hook in your own CSS compiler
                        'google'        	=> true,    // Disable google fonts. Won't work if you haven't defined your google api key
                        'font-backup'   	=> false,    // Select a backup non-google font in addition to a google font
                        'font-style'    	=> true, // Includes font-style and weight. Can use font-style or font-weight to declare
                        'subsets'       	=> true, // Only appears if google is true and subsets not set to false
                        //'font-size'     	=> false,
                        //'line-height'   	=> false,
                        //'word-spacing'  	=> true,  // Defaults to false
                        //'letter-spacing'	=> true,  // Defaults to false
                        'color'         	=> false,
                        //'preview'       	=> false, // Disable the previewer
                        'all_styles'    	=> true,    // Enable all Google Font style/weight variations to be added to the page
                        'output'        	=> array('body'), // An array of CSS selectors to apply this font style to dynamically
                        //'compiler'      	=> array('h2.site-description-compiler'), // An array of CSS selectors to apply this font style to dynamically
                        'units'         	=> 'px', // Defaults to px
                        'subtitle'      	=> esc_html__('Main body font.', 'vg-sentech'),
                        'default'       	=> array(
                            'font-weight'   => '400',
                            'font-family'   => 'Roboto Condensed',
                            'google'        => true,
                            'font-size'     => '14px',
                            'line-height'   => '20px'),
					),
				),
			);			
			
			/**
			 *	Brand logos
			 **/			
			$this->sections[] = array(
                'title'     => esc_html__('Brand Logos', 'vg-sentech'),
                'desc'      => esc_html__('Upload brand logos and links', 'vg-sentech'),
                'icon'      => 'el-icon-website',
                'fields'    => array(			
					array(
						'id'          => 'brand_logos',
						'type'        => 'slides',
						'title'       => esc_html__('Logos', 'vg-sentech'),
						'desc'        => esc_html__('Upload logo image and enter logo link.', 'vg-sentech'),
						'placeholder' => array(
							'title'           => esc_html__('Title', 'vg-sentech'),
							'description'     => esc_html__('Description', 'vg-sentech'),
							'url'             => esc_html__('Link', 'vg-sentech'),
						),
					),
				),
			);			
		  
			/**
			 *	Woocommerce
			 **/
			$this->sections[] = array(
                'title'     => esc_html__('Woocommerce', 'vg-sentech'),
                'desc'      => esc_html__('Use this section to select options for product', 'vg-sentech'),
                'icon'      => 'el-icon-shopping-cart',
				'fields'    => array(
					array(
						'id'       => 'second_image',
						'type'     => 'switch',
						'title'    => esc_html__('Use secondary product image', 'vg-sentech'),
						'default'  => true,
					),		
					array(
						'id'       => 'quick_view',
						'type'     => 'switch',
						'title'    => esc_html__('Use quick view product', 'vg-sentech'),
						'default'  => true,
					),		
					array(
						'id'       	=> 'sale_label',
						'type'     	=> 'radio',
						'title'    	=> esc_html__('Sale Label Settings', 'vg-sentech'),
						'subtitle'      => esc_html__('Config Sale Label Settings', 'vg-sentech'),
						'options'  	=> array(
							'Sale' 						=> esc_html__('Sale', 'vg-sentech'),
							'Save {percent-diff}%' 	=> esc_html__('Save {percent-diff}%', 'vg-sentech'),
							'Save ${price-diff}' 	=> esc_html__('Save ${price-diff}', 'vg-sentech'),
							'custom' 				=> esc_html__('Custom (use field below)', 'vg-sentech'),
						),
						'default'  	=> 'custom'
					),
					array(
                        'id'        => 'sale_label_custom',
                        'type'      => 'text',
                        'title'     => esc_html__('Custom Format Label', 'vg-sentech'),
                        'desc'     => esc_html__('{price-diff} inserts the dollar amount off, {percent-diff} inserts the percent reduction (rounded).', 'vg-sentech'),
                        'default'   => '{percent-diff}%'
					),		
					array(
                        'id'        => 'featured_label_custom',
                        'type'      => 'text',
                        'title'     => esc_html__('Custom Featured Label', 'vg-sentech'),
                        'desc'     => esc_html__('Text Custom Featured Label ', 'vg-sentech'),
                        'default'   => 'New'
					),
				),
			);
		  
			// Category
			$this->sections[] = array(
				'icon'       => 'el-icon-tags',
				'title'      => esc_html__( 'Category', 'vg-sentech' ),
				'desc'      => esc_html__('Use this section to select options for Page Category Product', 'vg-sentech'),
				'subsection' => true,	
                'fields'    => array(
					array(
						'id'       	=> 'layout_product',
						'type'     	=> 'radio',
						'title'    	=> esc_html__('Category View Layout', 'vg-sentech'),
						'subtitle'      => esc_html__('View layout on category page', 'vg-sentech'),
						'options'  	=> array(
							'gridview' 	=> 'Grid View',
							'listview' => 'List View'),
						'default'  	=> 'gridview'
					),
					array(
                        'id'        => 'cat_banner_img',
                        'type'      => 'media',
                        'title'     => esc_html__('Banner Header Category', 'vg-sentech'),
                        'compiler'  => 'true',
                        'mode'      => false,
                        'desc'      => esc_html__('Upload banner category here.', 'vg-sentech'),
						'default'  => array( 'url' => get_template_directory_uri() . '/images/banner_static1.jpg' ),
					),
					array(
                        'id'        => 'cat_banner_link',
                        'type'      => 'text',
                        'title'     => esc_html__('Link Banner Category', 'vg-sentech'),
                        'default'   => ''
					),	
					array(
						'id'        	=> 'product_per_page',
						'type'      	=> 'slider',
						'title'     	=> esc_html__('Products per page', 'vg-sentech'),
						'subtitle'  	=> esc_html__('Amount of products per page on category page', 'vg-sentech'),
						"default"   	=> 16,
						"min"       	=> 3,
						"step"      	=> 1,
						"max"       	=> 48,
						'display_value' => 'text'
					),
					array(
						'id'        	=> 'product_per_row',
						'handles'		=>	true,
						'type'      	=> 'select',
						'title'     	=> esc_html__('Products per row', 'vg-sentech'),
						'subtitle'  	=> esc_html__('Products per row on category page', 'vg-sentech'),
                        'options'   => array(
							'3' => '3',
                            '4' => '4',
						),
                        'default'   => '4'
					),
				),
			);
								
			// Single Product
			$this->sections[] = array(
				'icon'       => 'el-icon-tags',
				'title'      => esc_html__( 'Single Product', 'vg-sentech' ),
				'desc'      => esc_html__('Use this section to select options for Page Category Product', 'vg-sentech'),
				'subsection' => true,	
                'fields'    => array(
					array(
						'id'       => 'single_meta',
						'type'     => 'switch',
						'title'    => esc_html__('Use hidden meta tags, category, SKU', 'vg-sentech'),
						'default'  => true,
					),
					array(
                        'id'        => 'upsells_title',
                        'type'      => 'text',
                        'title'     => esc_html__('Upsells products title', 'vg-sentech'),
                        'default'   => 'Upsells Products'
					),
					array(
                        'id'        => 'crosssells_title',
                        'type'      => 'text',
                        'title'     => esc_html__('Cross Sell products title', 'vg-sentech'),
                        'default'   => 'Cross Sell Products'
					),					
					array(
                        'id'        => 'related_title',
                        'type'      => 'text',
                        'title'     => esc_html__('Related products title', 'vg-sentech'),
                        'default'   => 'Related Products'
					),
					array(
						'id'        	=> 'related_amount',
						'type'      	=> 'slider',
						'title'     	=> esc_html__('Number of related products', 'vg-sentech'),
						"default"   	=> 6,
						"min"       	=> 3,
						"step"      	=> 1,
						"max"       	=> 16,
						'display_value' => 'text'
					),
					
				),
			);
			
			// Mini Cart
			$this->sections[] = array(
				'icon'       => 'el-icon-tags',
				'title'      => esc_html__( 'Mini Cart', 'vg-sentech' ),
				'subsection' => true,
				'fields'     => array(
					array(
                        'id'        => 'mini_cart_sub_title',
                        'type'      => 'text',
                        'title'     => esc_html__('Mini cart Sub title', 'vg-sentech'),
                        'default'   => 'My Cart'
					),		
				),
			);				
			
			/**
			 *	Advanced
			 **/
			$this->sections[] = array(
				'id'			=> 'advanced',
				'title'			=> esc_html__( 'Advanced', 'vg-sentech' ),
				'desc'			=> '',
                'icon'      => 'el-icon-wrench',
            );
			
			// Less Compiler
            $this->sections[] = array(
				'icon'      => 'el-icon-brush',
                'title'     => esc_html__('Less Compiler', 'vg-sentech'),
                'desc'      => esc_html__('Turn on this option to apply all theme options. Turn of when you have finished changing theme options and your site is ready.', 'vg-sentech'),
				'subsection' => true,
                'fields'    => array(
					array(
                        'id'        => 'enable_less',
                        'type'      => 'switch',
                        'title'     => esc_html__('Enable Less Compiler', 'vg-sentech'),
						'default'   => true,
					),
				),
			);
			
			/**
			 *	Advanced Custom CSS
			 **/
			$this->sections[] = array(
				'id'			=> 'advanced_css',
				'icon'      => 'el-icon-css',
				'title'			=> esc_html__( 'Custom CSS', 'vg-sentech' ),
				'desc'			=> '',
                'subsection' => true,
				'fields'		=> array(
                    array(
                        'id'       => 'advanced_editor_css',
                        'type'     => 'ace_editor',
                        'title'    => esc_html__( 'CSS Code', 'vg-sentech' ),
                        'subtitle' => esc_html__( 'Paste your CSS code here.', 'vg-sentech' ),
                        'mode'     => 'css',
                        'theme'    => 'monokai',
						'default'  => "body{\n margin: 0 auto;\n}",
                        'full_width' => true
                    ),
                )
            );
            /**
			 *	Advanced Custom CSS
			 **/
			$this->sections[] = array(
				'id'			=> 'advanced_js',
				'icon'      => 'el-icon-barcode',
				'title'			=> esc_html__( 'Custom JS', 'vg-sentech' ),
				'desc'			=> '',
                'subsection' => true,
				'fields'		=> array(
                    array(
                        'id'       => 'advanced_editor_js',
                        'type'     => 'ace_editor',
                        'title'    => esc_html__( 'JS Code', 'vg-sentech' ),
                        'subtitle' => esc_html__( 'Paste your JS code here.', 'vg-sentech' ),
                        'mode'     => 'javascript',
                        'theme'    => 'monokai',
                        'default'  => "jQuery(document).ready(function(){\n\n});",
                        'full_width' => true
                    ),
                )
            );			

			// Loading Page
			$this->sections[] = array(	
				'icon'      => 'el-icon-record',
                'title'     => esc_html__('Loading Page', 'vg-sentech'),
				'subsection' 	=> true,
                'fields'    => array(
					array(
                        'id'        => 'sentech_loading',
                        'type'      => 'switch',
                        'title'     => esc_html__('Show Loading Page', 'vg-sentech'),
						'default'   => false,
					),
				),
			);
			
            $theme_info  = '<div class="redux-framework-section-desc">';
            $theme_info .= '<p class="redux-framework-theme-data description theme-uri">' . wp_kses(__('<strong>Theme URL:</strong> ', 'vg-sentech'), array('strong' => array())) . '<a href="' . $this->theme->get('ThemeURI') . '" target="_blank">' . $this->theme->get('ThemeURI') . '</a></p>';
            $theme_info .= '<p class="redux-framework-theme-data description theme-author">' . wp_kses(__('<strong>Author:</strong> ', 'vg-sentech'), array('strong' => array())) . $this->theme->get('Author') . '</p>';
            $theme_info .= '<p class="redux-framework-theme-data description theme-version">' . wp_kses(__('<strong>Version:</strong> ', 'vg-sentech'), array('strong' => array())) . $this->theme->get('Version') . '</p>';
            $theme_info .= '<p class="redux-framework-theme-data description theme-description">' . $this->theme->get('Description') . '</p>';
            $tabs 		 = $this->theme->get('Tags');
            if (!empty($tabs)) {
                $theme_info .= '<p class="redux-framework-theme-data description theme-tags">' . wp_kses(__('<strong>Tags:</strong> ', 'vg-sentech'), array('strong' => array())) . implode(', ', $tabs) . '</p>';
            }
            $theme_info .= '</div>';

            $this->sections[] = array(
                'icon'              => 'el-icon-list-alt',
                'title'             => esc_html__('Customizer Only', 'vg-sentech'),
                'desc'              => wp_kses(__('<p class="description">This Section should be visible only in Customizer</p>', 'vg-sentech'), array('p' => array('class' => array()))),
                'customizer_only'   => true,
                'fields'    => array(
                    array(
                        'id'        => 'opt-customizer-only',
                        'type'      => 'select',
                        'title'     => esc_html__('Customizer Only Option', 'vg-sentech'),
                        'subtitle'  => esc_html__('The subtitle is NOT visible in customizer', 'vg-sentech'),
                        'desc'      => esc_html__('The field desc is NOT visible in customizer.', 'vg-sentech'),
                        'customizer_only'   => true,

                        //Must provide key => value pairs for select options
                        'options'   => array(
                            '1' => 'Opt 1',
                            '2' => 'Opt 2',
                            '3' => 'Opt 3'
						),
                        'default'   => '2'
					),
				)
			);            
            
            $this->sections[] = array(
                'title'     => esc_html__('Import / Export', 'vg-sentech'),
                'desc'      => esc_html__('Import and Export your Redux Framework settings from file, text or URL.', 'vg-sentech'),
                'icon'      => 'el-icon-refresh',
                'fields'    => array(
                    array(
                        'id'            => 'opt-import-export',
                        'type'          => 'import_export',
                        'title'         => 'Import Export',
                        'subtitle'      => 'Save and restore your Redux options',
                        'full_width'    => false,
					),
				),
			);

            $this->sections[] = array(
                'icon'      => 'el-icon-info-sign',
                'title'     => esc_html__('Theme Information', 'vg-sentech'),
                //'desc'      => esc_html__('<p class="description">This is the Description. Again HTML is allowed</p>', 'vg-sentech'),
                'fields'    => array(
                    array(
                        'id'        => 'opt-raw-info',
                        'type'      => 'raw',
                        'content'   => $item_info,
					)
				),
			);
        }

        public function setHelpTabs() {

            // Custom page help tabs, displayed using the help API. Tabs are shown in order of definition.
            $this->args['help_tabs'][] = array(
                'id'        => 'redux-help-tab-1',
                'title'     => esc_html__('Theme Information 1', 'vg-sentech'),
                'content'   => wp_kses(__('<p>This is the tab content, HTML is allowed.</p>', 'vg-sentech'), array('p' => array()))
			);

            $this->args['help_tabs'][] = array(
                'id'        => 'redux-help-tab-2',
                'title'     => esc_html__('Theme Information 2', 'vg-sentech'),
                'content'   => wp_kses(__('<p>This is the tab content, HTML is allowed.</p>', 'vg-sentech'), array('p' => array()))
			);

            // Set the help sidebar
            $this->args['help_sidebar'] = wp_kses(__('<p>This is the sidebar content, HTML is allowed.</p>', 'vg-sentech'), array('p' => array()));
        }

        /**

          All the possible arguments for Redux.
          For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments

         * */
        public function setArguments() {

            $theme = wp_get_theme(); // For use with some settings. Not necessary.

            $this->args = array(
                // TYPICAL -> Change these values as you need/desire
                'opt_name'          => 'sentech_options',            // This is where your data is stored in the database and also becomes your global variable name.
                'display_name'      => $theme->get('Name'),     // Name that appears at the top of your panel
                'display_version'   => $theme->get('Version'),  // Version that appears at the top of your panel
                'menu_type'         => 'menu',                  //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
                'allow_sub_menu'    => true,                    // Show the sections below the admin menu item or not
                'menu_title'        => esc_html__('Theme Options', 'vg-sentech'),
                'page_title'        => esc_html__('Theme Options', 'vg-sentech'),
                
                // You will need to generate a Google API key to use this feature.
                // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
                'google_api_key' 	=> '', // Must be defined to add google fonts to the typography module
                
                'async_typography'  => true,                    // Use a asynchronous font on the front end or font string
                //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
                'admin_bar'         => true,                    // Show the panel pages on the admin bar
                'global_variable'   => '',                      // Set a different name for your global variable other than the opt_name
                'dev_mode'          => false,                    // Show the time the page took to load, etc
                'customizer'        => true,                    // Enable basic customizer support
                //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
                //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

                // OPTIONAL -> Give you extra features
                'page_priority'     => null,                    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
                'page_parent'       => 'themes.php',            // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
                'page_permissions'  => 'manage_options',        // Permissions needed to access the options panel.
                'menu_icon'         => '',                      // Specify a custom URL to an icon
                'last_tab'          => '',                      // Force your panel to always open to a specific tab (by id)
                'page_icon'         => 'icon-themes',           // Icon displayed in the admin panel next to your menu_title
                'page_slug'         => '_options',              // Page slug used to denote the panel
                'save_defaults'     => true,                    // On load save the defaults to DB before user clicks save or not
                'default_show'      => false,                   // If true, shows the default value next to each field that is not the default value.
                'default_mark'      => '',                      // What to print by the field's title if the value shown is default. Suggested: *
                'show_import_export' => true,                   // Shows the Import/Export panel when not used as a field.
                
                // CAREFUL -> These options are for advanced use only
                'transient_time'    => 60 * MINUTE_IN_SECONDS,
                'output'            => true,                    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
                'output_tag'        => true,                    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
                // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.
                
                // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
                'database'          => '', // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
                'system_info'       => false, // REMOVE

                // HINTS
                'hints' => array(
                    'icon'          => 'icon-question-sign',
                    'icon_position' => 'right',
                    'icon_color'    => 'lightgray',
                    'icon_size'     => 'normal',
                    'tip_style'     => array(
                        'color'         => 'light',
                        'shadow'        => true,
                        'rounded'       => false,
                        'style'         => '',
					),
                    'tip_position'  => array(
                        'my' => 'top left',
                        'at' => 'bottom right',
					),
                    'tip_effect'    => array(
                        'show'          => array(
                            'effect'        => 'slide',
                            'duration'      => '500',
                            'event'         => 'mouseover',
						),
                        'hide'      => array(
                            'effect'    => 'slide',
                            'duration'  => '500',
                            'event'     => 'click mouseleave',
						),
					),
				)
			);


            // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
            $this->args['share_icons'][] = array(
                'url'   => 'https://github.com/ReduxFramework/ReduxFramework',
                'title' => 'Visit us on GitHub',
                'icon'  => 'el-icon-github'
                //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
			);
            $this->args['share_icons'][] = array(
                'url'   => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
                'title' => 'Like us on Facebook',
                'icon'  => 'el-icon-facebook'
			);
            $this->args['share_icons'][] = array(
                'url'   => 'http://twitter.com/reduxframework',
                'title' => 'Follow us on Twitter',
                'icon'  => 'el-icon-twitter'
			);
            $this->args['share_icons'][] = array(
                'url'   => 'http://www.linkedin.com/company/redux-framework',
                'title' => 'Find us on LinkedIn',
                'icon'  => 'el-icon-linkedin'
			);

            // Panel Intro text -> before the form
            if (!isset($this->args['global_variable']) || $this->args['global_variable'] !== false) {
                if (!empty($this->args['global_variable'])) {
                    $v = $this->args['global_variable'];
                } else {
                    $v = str_replace('-', '_', $this->args['opt_name']);
                }
                //$this->args['intro_text'] = sprintf(__('<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'vg-sentech'), $v);
            } else {
                //$this->args['intro_text'] = esc_html__('<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'vg-sentech');
            }

            // Add content after the form.
            //$this->args['footer_text'] = esc_html__('<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'vg-sentech');
        }

    }
    
    global $reduxConfig;
    $reduxConfig = new sentech_theme_config();
}

/**
  Custom function for the callback referenced above
 */
if (!function_exists('redux_my_custom_field')):
    function redux_my_custom_field($field, $value) {
        print_r($field);
        echo '<br/>';
        print_r($value);
    }
endif;

/**
  Custom function for the callback validation referenced above
 * */
if (!function_exists('redux_validate_callback_function')):
    function redux_validate_callback_function($field, $value, $existing_value) {
        $error = false;
        $value = 'just testing';

        /*
          do your validation

          if(something) {
            $value = $value;
          } elseif(something else) {
            $error = true;
            $value = $existing_value;
            $field['msg'] = 'your custom error message';
          }
         */

        $return['value'] = $value;
        if ($error == true) {
            $return['error'] = $field;
        }
        return $return;
    }
endif;


<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * VG AboutMe widget class
 *
 * @since 1.0
 */
class WP_Widget_VG_AboutMe extends WP_Widget {

    public function __construct() {
        $widget_ops = array(
						'classname' => 'widget_vg_aboutme',
						'description' => esc_html__( 'About Me widget.', 'vg-sentech' ) 
					);
        parent::__construct('vg_aboutme', esc_html__('VG: About me', 'vg-sentech' ), $widget_ops);
    }

    public function widget( $args, $instance ) {
		global $sentech_options;

        $title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );
		$vg_social = isset( $instance['vg_social'] ) ? $instance['vg_social'] : 'show';

        echo $args['before_widget'];

        if ( $title ) {
            echo $args['before_title'] . esc_html($title) . $args['after_title'];
        }

		$output = '';
        $output .= '<div class="vg-aboutwidget-content">';
		$output .= ($instance['description']) ? '<div class="description">'.$instance['description'].'</div>' : '';
        $output .= '<ul class="info">';
        $output .= ($instance['address']) ? '<li><i class="fa fa-map-marker"></i>'.$instance['address'].'</li>' : '';
        $output .= ($instance['phone']) ? '<li><i class="fa fa-phone"></i>'.$instance['phone'].'</li>' : '';
        $output .= ($instance['email']) ? '<li><i class="fa fa-envelope"></i>'.$instance['email'].'</li>' : '';
        $output .= '</ul>';
		$sentech_options['ftsocial_icons'] = isset($sentech_options['ftsocial_icons']) ? $sentech_options['ftsocial_icons'] : "";
		if($instance['vg_social'] == 'show') {
			$output .= '<ul class="social-icons">';
				foreach($sentech_options['ftsocial_icons'] as $key=>$value) {
					if($value!=''){
						if($key=='vimeo'){
							$output .= '<li class="'.esc_attr($key).'"><a class="social-icon" href="'.esc_url($value).'" title="'.ucwords(esc_attr($key)).'" target="_blank"><i class="fa fa-vimeo-square"></i></a></li>';
						} else {
							$output .= '<li class="'.esc_attr($key).'"><a class="social-icon" href="'.esc_url($value).'" title="'.ucwords(esc_attr($key)).'" target="_blank"><i class="fa fa-'.esc_attr($key).'"></i></a></li>';
						}
					}
				}
			$output .=  '</ul>';
		}
						
		
        $output .= '</div>';

        echo $output;

        echo $args['after_widget'];

    }

    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] 			= strip_tags($new_instance['title']);
        $instance['description'] 	= $new_instance['description'];
        $instance['address']		= strip_tags($new_instance['address']);
        $instance['phone'] 			= strip_tags($new_instance['phone']);
        $instance['email']			= strip_tags($new_instance['email']);

		if ( in_array( $new_instance['vg_social'], array( 'show', 'hidden', ) )) {
            $instance['vg_social'] = $new_instance['vg_social'];
        } else {
            $instance['vg_social'] = 'show';
        }
		
        return $instance;
    }

    public function form( $instance ) {
		
		$defaults = array(
						'title'			 => esc_html__('About sentech', 'vg-sentech'),
						'description'	 => esc_html__('At vero eos et accusamus et iusto odio dignissimos ducimus qui...', 'vg-sentech'),
						'address' 		 => esc_html__('1234 Heaven Stress, Beverly Hill.', 'vg-sentech'),
						'phone'			 => esc_html__('(800) 0123 4567 890', 'vg-sentech'),
						'email'			 => esc_html__('Support1@domain.com', 'vg-sentech'),
						'vg_social'		 => 'show',
					);
		$instance = wp_parse_args( (array) $instance, $defaults );
        $title = strip_tags($instance['title']);
		$vg_social = isset( $instance['vg_social'] ) ? $instance['vg_social'] : 'show';
		
        ?>

        <p><label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php esc_html_e( 'Title:', 'vg-sentech' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>

		<p>
		<label for="<?php echo esc_attr($this->get_field_id( 'description' )); ?>"><?php esc_html_e( 'Description:', 'vg-sentech' ); ?></label>
		<textarea class="widefat" rows="5" cols="20" id="<?php echo esc_attr($this->get_field_id('description')); ?>" name="<?php echo esc_attr($this->get_field_name('description')); ?>"><?php echo esc_textarea($instance['description']); ?></textarea></p>

			
        <p><label for="<?php echo esc_attr($this->get_field_id( 'address' )); ?>"><?php esc_html_e( 'Address:', 'vg-sentech' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'address' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'address' )); ?>" type="text" value="<?php echo esc_attr($instance['address']); ?>" /></p>

        <p><label for="<?php echo esc_attr($this->get_field_id( 'phone' )); ?>"><?php esc_html_e( 'Phone:', 'vg-sentech' ); ?></label>
            <input  id="<?php echo esc_attr($this->get_field_id( 'phone' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'phone' )); ?>" type="text" value="<?php echo esc_attr( $instance['phone']); ?>" class="widefat" /></p>

        <p><label for="<?php echo esc_attr($this->get_field_id( 'email' )); ?>"><?php esc_html_e( 'Email:', 'vg-sentech' ); ?></label>
            <input  id="<?php echo esc_attr($this->get_field_id( 'email' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'email' )); ?>" type="text" value="<?php echo esc_attr($instance['email']); ?>" class="widefat" /></p>

	    <p><label for="<?php echo esc_attr($this->get_field_id('vg_social')); ?>"><?php esc_html_e('VG Social:', 'vg-sentech'); ?></label>
            <select class="widefat" id="<?php echo esc_attr($this->get_field_id('vg_social')); ?>" name="<?php echo esc_attr($this->get_field_name('vg_social')); ?>">
                <option <?php selected( $vg_social, 'show' ); ?> value="show"><?php esc_html_e('Show','vg-sentech'); ?></option>
                <option <?php selected( $vg_social, 'hidden' ); ?> value="hidden"><?php esc_html_e('Hidden','vg-sentech'); ?></option>
            </select>
        </p>

        <?php
		
		
    }

}


register_widget( 'WP_Widget_VG_AboutMe' );
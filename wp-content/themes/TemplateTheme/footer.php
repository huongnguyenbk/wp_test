<?php 
/**
* The footer
* @package d4jtheme
* @subpackage d4jtheme
* @since d4jtheme 1.0
*/
$options = get_option( 'd4j_theme_options' );
?>
		<div class="container footer">
			<div class="row">
				<div class="col copyright">
					<p><a href="http://DesignForJoomla.com/">Made by D4J</a> &copy; 2015</p>
				</div>
				<div class="col footer-nav">
					<p><a title="Home" href="/">Home</a></p>
				</div>
				<div class="col mediatemple last">					
				</div>
			</div>
		</div>
		<?php wp_footer(); ?>
		<?php if($options[googleanalytic]){ ?>
			<script>
			  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			  ga('create', '<?php echo trim($options[googleanalytic]); ?>', 'auto');
			  ga('send', 'pageview');
			</script>
		<?php } ?>
	</body>
</html>
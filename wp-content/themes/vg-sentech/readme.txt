=== VG Sentech ===
Contributors: Vina Web Solutions
Tags: wordpress theme, woocommerce theme, bootstrap 3, visual composer, product carousel, responsive, fashion, high-tech, watches, interior, product compare, product wishlist, furniture, woocarousel, postcarousel.
Requires at least: 4.5
Tested up to: 4.5.3
Stable tag: 4.5.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==
VG Sentech - Responsive Digital Woocommerce Theme. VG Sentech is a unique and flexible WordPress Theme for marketplace and online stores selling digital products. It is compatible with the latest version of WordPress 4.5+, WooCommerce 2.5+ and built based on the latest Twitter Bootstrap 3 technology. This theme comes with 3 Unique Layouts (with 5 Presets Color for each layout), 3 Box Layouts and 100% responsive that runs smoothly on any type of devices. It also looks sharp and clear on modern devices with high screen resolution because every area of this theme is retina ready. And it comes with an unbranded admin Theme Options panel that gives you full control of the customization of your website also integrated with drag and drop visual composer to help you build unlimited page layout for your website. Aside from just looking beautifully polished and clean, VG Sentech comes packed with features and options. The most powerful and trendy features such as Product Quick View, Product Compare, VG WooCarousel (save $17), Product Tabs Slider, VG PostCarousel (save $15), Ajax Wishlist, Visual Composer (save $34), Ajax Shopping Cart, Revolution Slider (save $19) and Mega Main Menu (Save $15) with many columns under each product categories takes customers to everywhere in your online shops with much more features. We have a dedicated support center for all of your support needs. It includes our Documentation, Community Forum and an advanced Ticket System for any questions you have. We usually get back to you within 14-16 hours. (except holiday seasons which might take longer). Use our theme you will never working alone!

* 4 Unique Theme Layouts
* 100% Responsive WordPress Theme.
* Free Lifetime updates!
* WordPress 4.5+ Ready
* WPML Ready (.pot files included)
* Fully support RTL language!
* SEO Optimised
* Product Quick View, Product Compare and Ajax Wishlist
* WooCommerce 2.6+ Ready
* Visual Composer – $34 saved
* Mega Main Menu – $15 saved
* VG PostCarousel – $15 saved
* VG WooCarousel - $17 saved
* Revolution Slider – $19 saved
* Supports Chrome, Safari, Firefox, IE
* Child Theme included
* Demo content included! (quickstart package)

== Installation ==

1. In your admin panel, go to Appearance -> Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's ZIP file. Click Install Now.
3. Click Activate to use your new theme right away.

== Changelog ==

= 1.0 =
* Released: August 13, 2016

Initial release.


<?php

/* Register Sidebars */

add_action('widgets_init', 'my_register_sidebars');

function my_register_sidebars()
{
    register_sidebar(array(
        'id' => 'left-col',
        'name' => 'Sidebar Widget - Left',
        'before_widget' => '<div class="sidebar-box">',
        'after_widget' => '</div>',
        'before_title' => '<div class="tab tab-light-blue">',
        'after_title' => '</div>'
    ));
	register_sidebar(array(
        'id' => 'right-col',
        'name' => 'Sidebar Widget - right',
        'before_widget' => '<div class="sidebar-box">',
        'after_widget' => '</div>',
        'before_title' => '<div class="tab tab-light-blue">',
        'after_title' => '</div>'
    ));
	
}

?>
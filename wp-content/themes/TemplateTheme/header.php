<?php 
/**
* The header
* @package d4jtheme
* @subpackage d4jtheme
* @since d4jtheme 1.0
*/
$options = get_option( 'd4j_theme_options' );
if(!$options[colorscheme]){$options[colorscheme]='default';}
if ( ! isset( $content_width ) ) $content_width = 1170;
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="UTF-8">
		<meta http-equiv="content-language" content="vi" />
		<link rel="alternate" href="<?php echo esc_url( home_url() ); ?>" hreflang="<?php language_attributes(); ?>" />
		<meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <title><?php wp_title(''); ?></title>
        <link rel="profile" href="http://gmpg.org/xfn/11" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/<?php echo $options[colorscheme]; ?>.css" />
        <?php wp_head();
			if($options[logo]){echo '<style>.logo h1{background: url("'.$options[logo].'") no-repeat}</style>';}
		?>
		<?php if ( is_singular() ) wp_enqueue_script( "comment-reply" ); ?>
    </head>
	<body <?php body_class( $class ); ?>>
		<div class="container login">
			<div class="row">
			</div>
		</div>
		<div class="container logo-search">
			<div class="row">
				<div class="col logo">
					<h1><a href="<?php echo esc_url( home_url() ); ?>/"><?php bloginfo('name'); ?></a></h1>
				</div>
				<div class="col search search-desktop last">
					<form action="/">
						<input value="" placeholder="Search" name="s" id="s" class="placeholder" autocomplete="off">
					</form>
				</div>
			</div>
		</div>
		<div class="container navigation">
			<div class="row">
				<div class="col nav">
					<?php 
						if ( has_nav_menu( 'main_menu' ) ) {
							 wp_nav_menu( array(
							 'container' =>false,
							 'container_class' => 'main-menu-header',
							 'theme_location' => 'main_menu',
							 'echo' => true,
							 'fallback_cb' => 'ideo_fallback_menu',
							 'before' => '',
							 'after' => '',
							 'link_before' => '',
							 'link_after' => '',
							 'depth' => 0,
							 'walker' => new description_walker())
							 ); 
						}
						if ( has_nav_menu( 'main_menu' ) ) {echo be_mobile_menu();}
					?>
				</div>
			</div>
		</div>
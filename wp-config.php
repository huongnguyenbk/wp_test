<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_test');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8x@2[mAKr>E>%g(Q@GPecc3+a1gZB@&K[1pQ`G+)3&Kt2eeKA:yD`Aw&x3r79V=3');
define('SECURE_AUTH_KEY',  '5ReMEXT*q!FCNO.qvNI3oa;yT5TH$B&Q?!HaR@v0t6Q=[Zw+G-*.hvIx(sq&Kg1?');
define('LOGGED_IN_KEY',    '4$;6K@7{.(;<pU<_W#c?j#PW,Y|Z@l6oFMCs*/I3X~U`FuUKF>_zPm*P/v^h?e0I');
define('NONCE_KEY',        'K998?3[t,!D@R1{k{Qj)pi|:5)7/!S_>]`*uGI)7K^!XE80VY?t*`KtH3oJp(0oE');
define('AUTH_SALT',        ':>(k5w?gQJLqrM{3m2VWGoJvE=hGt1_u4Mf`}Yh+Y2I@b.?YVQ3M:Rc6*p{Qg@yi');
define('SECURE_AUTH_SALT', 'cO79UwE~+@z]PF{C{xMBZ{YDI{9U?u![?A{ 1j&RIalU_Enjp)AlaX/$iYA7Xy!v');
define('LOGGED_IN_SALT',   'i0[/c,4: 3&~_fj-TuD5p*-79C/vOM_,EPNIe`u,spY-P,-cG8Ru.6VG4?]Jr[@Q');
define('NONCE_SALT',       '>EYTbw-2V9[+|uF.&5[qUxKl=jGh27eIg,<1;$(dIcRC3OL38jj(q{NOy(uj8{-*');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

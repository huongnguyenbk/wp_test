<?php
/**
 * @version    1.1
 * @package    VG Sentech
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */

//Require plugins
require_once get_template_directory () . '/class-tgm-plugin-activation.php';

function sentech_register_required_plugins() {

    $plugins = array(
		array(
            'name'               => esc_html__('Mega Main Menu', 'vg-sentech'),
            'slug'               => 'mega_main_menu',
            'source'             => esc_url('http://wordpress.vinagecko.net/l/mega_main_menu.zip'),
            'required'           => true,
            'external_url'       => '',
        ),
		array(
            'name'               => esc_html__('Visual Composer', 'vg-sentech'),
            'slug'               => 'js_composer',
            'source'             => esc_url('http://wordpress.vinagecko.net/l/js_composer.zip'),
            'required'           => true,
            'external_url'       => '',
        ),
		array(
            'name'               => esc_html__('Redux Framework', 'vg-sentech'),
            'slug'               => 'redux-framework',
			'source'             => esc_url('http://wordpress.vinagecko.net/l/redux-framework.zip'),
            'required'           => true,
			'force_activation'   => false,
            'force_deactivation' => false,
        ),
		array(
            'name'               => esc_html__('VG WooCarousel', 'vg-sentech'),
            'slug'               => 'vg-woocarousel',
			'source'             => esc_url('http://wordpress.vinagecko.net/l/vg-woocarousel.zip'),
            'required'           => true,
            'external_url'       => '',
        ),
		array(
            'name'               => esc_html__('VG PostCarousel', 'vg-sentech'),
            'slug'               => 'vg-postcarousel',
			'source'             => esc_url('http://wordpress.vinagecko.net/l/vg-postcarousel.zip'),
            'required'           => true,
            'external_url'       => '',
        ),
		array(
            'name'               => esc_html__('Revolution Slider', 'vg-sentech'),
            'slug'               => 'revslider',
            'source'             => esc_url('http://wordpress.vinagecko.net/l/revslider.zip'),
            'required'           => true,
            'external_url'       => '',
        ),
        // Plugins from the WordPress Plugin Repository.
		array(
            'name'               => esc_html__('Shortcodes Ultimate', 'vg-sentech'),
            'slug'               => 'shortcodes-ultimate',
            'required'           => true,
			'force_activation'   => false,
            'force_deactivation' => false,
        ),
        array(
            'name'      => esc_html__('Contact Form 7', 'vg-sentech'),
            'slug'      => 'contact-form-7',
            'required'  => true,
        ),    
		array(
            'name'      => esc_html__('MailPoet Newsletters', 'vg-sentech'),
            'slug'      => 'wysija-newsletters',
            'required'  => true,
        ),	
		array(
            'name'      => esc_html__('Testimonials', 'vg-sentech'),
            'slug'      => 'testimonials-by-woothemes',
            'required'  => true,
        ),
		array(
            'name'      => esc_html__('TinyMCE Advanced', 'vg-sentech'),
            'slug'      => 'tinymce-advanced',
            'required'  => true,
        ),		
		array(
            'name'      => esc_html__('Image Widget', 'vg-sentech'),
            'slug'      => 'image-widget',
            'required'  => true,
        ),
		array(
            'name'      => esc_html__('Widget Importer & Exporter', 'vg-sentech'),
            'slug'      => 'widget-importer-exporter',
            'required'  => true,
        ),	
		array(
            'name'      => esc_html__('WordPress Importer', 'vg-sentech'),
            'slug'      => 'wordpress-importer',
            'required'  => true,
        ),
		array(
            'name'      => esc_html__('WooCommerce', 'vg-sentech'),
            'slug'      => 'woocommerce',
            'required'  => true,
        ),
		array(
            'name'      		 => esc_html__('WordPress AJAX Search & AutoSuggest Plugin', 'vg-sentech'),
            'slug'     			 => 'ajax-search-autosuggest',
            'required' 			 => true,
            'source'             => esc_url('http://wordpress.vinagecko.net/l/ajax-autosuggest-plugin.zip'),
            'external_url'       => '',
        ),		
		array(
            'name'      => esc_html__('YITH WooCommerce Compare', 'vg-sentech'),
            'slug'      => 'yith-woocommerce-compare',
            'required'  => true,
        ),		
		array(
            'name'      => esc_html__('YITH WooCommerce Wishlist', 'vg-sentech'),
            'slug'      => 'yith-woocommerce-wishlist',
            'required'  => true,
        ),
    );

    /**
     * Array of configuration settings. Amend each line as needed.
     * If you want the default strings to be available under your own theme domain,
     * leave the strings uncommented.
     * Some of the strings are added into a sprintf, so see the comments at the
     * end of each line for what each argument will be.
     */
    $config = array(
        'default_path' => '',                      // Default absolute path to pre-packaged plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.
        'strings'      => array(
            'page_title'                      => esc_html__( 'Install Required Plugins', 'vg-sentech' ),
            'menu_title'                      => esc_html__( 'Install Plugins', 'vg-sentech' ),
            'installing'                      => esc_html__( 'Installing Plugin: %s', 'vg-sentech' ), // %s = plugin name.
            'oops'                            => esc_html__( 'Something went wrong with the plugin API.', 'vg-sentech' ),
            'notice_can_install_required'     => _n_noop( 'This g requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'vg-sentech' ), // %1$s = plugin name(s).
            'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'vg-sentech' ), // %1$s = plugin name(s).
            'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'vg-sentech' ), // %1$s = plugin name(s).
            'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'vg-sentech' ), // %1$s = plugin name(s).
            'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'vg-sentech' ), // %1$s = plugin name(s).
            'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'vg-sentech' ), // %1$s = plugin name(s).
            'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'vg-sentech' ), // %1$s = plugin name(s).
            'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'vg-sentech' ), // %1$s = plugin name(s).
            'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'vg-sentech' ),
            'activate_link'                   => _n_noop( 'Begin activating plugin', 'Begin activating plugins', 'vg-sentech' ),
            'return'                          => esc_html__( 'Return to Required Plugins Installer', 'vg-sentech' ),
            'plugin_activated'                => esc_html__( 'Plugin activated successfully.', 'vg-sentech' ),
            'complete'                        => esc_html__( 'All plugins installed and activated successfully. %s', 'vg-sentech' ), // %s = dashboard link.
            'nag_type'                        => 'updated' // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
        )
    );

    tgmpa( $plugins, $config );

}
add_action( 'tgmpa_register', 'sentech_register_required_plugins' ); 

//Init the Redux Framework
if ( class_exists( 'ReduxFramework' ) && !isset( $redux_demo ) && file_exists( get_template_directory().'/theme-config.php' ) ) {
    require_once( get_template_directory().'/theme-config.php' );
}

//Add Woocommerce support
add_theme_support( 'woocommerce' );
add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cart_totals');

function sentech_start_wrapper_here() { // Start wrapper 
	
	if (is_shop()) { // Wrapper for the shop page

		echo '<div class="shop-wrapper">';

	} elseif (is_product_category() || is_product_tag()) {  // Wrapper for a product category page

		echo '<div class="product-category-wrapper">';

	} elseif (is_product()) { // Wrapper for a single product page

		echo '<div class="product-wrapper">';

	}
}

add_action(  'woocommerce_before_main_content', 'sentech_start_wrapper_here', 20  );


add_action(  'woocommerce_before_main_content', 'sentech_start_wrapper_here_end', 50  );
function sentech_start_wrapper_here_end() { // Start wrapper
	echo '</div>';
}

//Override woocommerce widgets
function sentech_override_woocommerce_widgets() {
	//Show mini cart on all pages
	if ( class_exists( 'WC_Widget_Cart' ) ) {
		unregister_widget( 'WC_Widget_Cart' ); 
		include_once( get_template_directory() . '/woocommerce/class-wc-widget-cart.php' );
		register_widget( 'Custom_WC_Widget_Cart' );
	}
}
add_action( 'widgets_init', 'sentech_override_woocommerce_widgets', 15 );

// Swallow code Override ajax search autosuggest widget
//Override woocommerce widgets
function sentech_override_ajaxsearch_autosuggest_widgets() {
	//Show mini cart on all pages
	if ( class_exists( 'Ajax_search_widget' ) ) {
		unregister_widget( 'Ajax_search_widget' ); 
		include_once( get_template_directory() . '/include/ajax-search-autosuggest.php' );
		register_widget( 'Custom_Ajax_search_widget' );
	}
}
add_action( 'widgets_init', 'sentech_override_ajaxsearch_autosuggest_widgets', 15 );

add_filter('woocommerce_product_get_rating_html', 'sentech_get_rating_html', 10, 2);

function sentech_get_rating_html($rating_html, $rating) {
  global $product;
  if ( $rating > 0 ) {
    $title = sprintf( esc_html__( 'Rated %s out of 5', 'vg-sentech' ), $rating );
  } else {
    $title = 'Not yet rated';
    $rating = 0;
  }

  $rating_html  = '<div class="vgwc-product-rating">';
	  $rating_html  .= '<div class="star-rating" title="' . esc_attr($title) . '">';
		$rating_html .= '<span style="width:' . ( ( $rating / 5 ) * 100 ) . '%"><strong class="rating">' . $rating . '</strong> ' . esc_html__( 'out of 5', 'vg-sentech' ) . '</span>';
	  $rating_html .= '</div>';
	  $rating_html .= $product->get_review_count(). esc_html__(" review(s)", "vg-sentech");
  $rating_html .= '</div>';

  return $rating_html;
}

// Ensure cart contents update when products are added to the cart via AJAX (place the following in functions.php)
function sentech_woocommerce_header_add_to_cart_fragment( $fragments ) {
	ob_start();
	?>
	
	<span class="mcart-number"><?php echo WC()->cart->cart_contents_count; ?></span>
	
	<?php
	$fragments['span.mcart-number'] = ob_get_clean();
	
	return $fragments;
} 
add_filter( 'woocommerce_add_to_cart_fragments', 'sentech_woocommerce_header_add_to_cart_fragment' );

//Change price html
add_filter( 'woocommerce_get_price_html', 'sentech_woo_price_html', 100, 2 );
function sentech_woo_price_html( $price, $product ){
	if ( $product->is_type( 'variable' ) ) {
		return '<div class="vgwc-product-price price-variable">'. $price .'</div>';
	}
	else {
		return '<div class="vgwc-product-price">'. $price .'</div>';
	}
}

remove_action( 'woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10 );
// Add image to category description
function sentech_woocommerce_category_image() {
	global $wp_query,$sentech_options;
	if ( is_product_category() || is_shop() || is_product_tag()){
		$image = '';
		if(is_product_category() || is_product_tag()) {
			$cat = $wp_query->get_queried_object();
			$thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
			$image = wp_get_attachment_url( $thumbnail_id );
		}
		if( empty($cat->description) && (isset($sentech_options['cat_description']) && !empty($sentech_options['cat_description']))) {
			?><div class="term-description vg-description"><?php echo  $sentech_options['cat_description']; ?></div><?php
		}
		else if(!empty($cat->description)) {
			?><div class="term-description"><?php echo  $cat->description ?></div><?php	
		}
	    if ( ($image && !empty($image))) {?>
		   <div class="category-image-desc"><img src="<?php echo esc_url($image); ?>" alt=""></div>
		<?php } else if(isset($sentech_options['cat_banner_link']) && !empty($sentech_options['cat_banner_link']) && isset($sentech_options['cat_banner_img']) && !empty($sentech_options['cat_banner_img'])) { ?>
			<div class="category-image-desc vg-cat-img"><a href="<?php echo esc_url($sentech_options['cat_banner_link']); ?>"><img src="<?php echo esc_url($sentech_options['cat_banner_img']['url']); ?>" alt=""></a></div>
		<?php } else if(isset($sentech_options['cat_banner_img']) && !empty($sentech_options['cat_banner_img'])) { ?>
			<div class="category-image-desc vg-cat-img"><img src="<?php echo esc_url($sentech_options['cat_banner_img']['url']); ?>" alt=""></div>
		<?php }
	}
}
add_action( 'woocommerce_archive_description', 'sentech_woocommerce_category_image', 20 );

// Change products per page
function sentech_woo_change_per_page() {
	global $sentech_options;
	$sentech_options['product_per_page'] = isset($sentech_options['product_per_page']) ? $sentech_options['product_per_page'] : "";
	
	return $sentech_options['product_per_page'];
}
add_filter( 'loop_shop_per_page', 'sentech_woo_change_per_page', 20 );

function sentech_copyright() {
	global $sentech_options;
	$copynotice = (isset($sentech_options['copyright-notice'])) ? $sentech_options['copyright-notice'] : '';
	$copylink 	= (isset($sentech_options['copyright-link'])) ? $sentech_options['copyright-link'] : '';
	if(strpos($copynotice,'{') && strpos($copynotice,'}') && $copylink) {
		$copyright = str_ireplace('{','<a href="' . esc_attr($copylink).'">',$copynotice);
		$copyright = str_ireplace('}','</a>',$copyright);
	}else{
		$copyright = $copynotice;
	}
	return $copyright;
}


//Limit number of products by shortcode [products]
//add_filter( 'woocommerce_shortcode_products_query', 'sentech_woocommerce_shortcode_limit' );
function sentech_woocommerce_shortcode_limit( $args ) {
	global $sentech_options, $sentech_productsfound;
	
	if(isset($sentech_options['shortcode_limit']) && $args['posts_per_page']==-1) {
		$args['posts_per_page'] = $sentech_options['shortcode_limit'];
	}
	
	$sentech_productsfound = new WP_Query($args);
	$sentech_productsfound = $sentech_productsfound->post_count;
	
	return $args;
}

// Change number or products per row to 4
function sentech_loop_columns() {
	global $sentech_options;
	$sentech_options['product_per_row'] = isset($sentech_options['product_per_row']) ? $sentech_options['product_per_row'] : "";
	
	return $sentech_options['product_per_row'];
}
add_filter('loop_shop_columns', 'sentech_loop_columns', 999);

//Change number of related products on product page. Set your own value for 'posts_per_page'
function sentech_woo_related_products_limit( $args ) {
	global $product, $sentech_options;
	$sentech_options['related_amount'] = isset($sentech_options['related_amount']) ? $sentech_options['related_amount'] : "";
	$args['posts_per_page'] = $sentech_options['related_amount'];

	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'sentech_woo_related_products_limit' );

//move message to top
remove_action( 'woocommerce_before_shop_loop', 'wc_print_notices', 10 );
add_action( 'woocommerce_show_message', 'wc_print_notices', 10 );


function sentech_view_mode_woocommerce_shop_loop() {
	global $sentech_options;
	
	$layout_product= isset($sentech_options['layout_product']) ? $sentech_options['layout_product'] : 'gridview';
	
	
	$customJSGrid = "
		jQuery(document).ready(function(){
			jQuery('.view-mode').each(function(){
				/* Grid View */					
				jQuery('#archive-product .view-mode').find('.grid').addClass('active');
				jQuery('#archive-product .view-mode').find('.list').removeClass('active');
				
				jQuery('#archive-product .shop-products').removeClass('list-view');
				jQuery('#archive-product .shop-products').addClass('grid-view');
				
				jQuery('#archive-product .list-col4').removeClass('col-xs-12 col-sm-6 col-lg-4');
				jQuery('#archive-product .list-col8').removeClass('col-xs-12 col-sm-6 col-lg-8');
			});
		});
	";
	
	$customJSList = "
		jQuery(document).ready(function(){
			jQuery('.view-mode').each(function(){
				/* List View */								
				jQuery('#archive-product .view-mode').find('.list').addClass('active');
				jQuery('#archive-product .view-mode').find('.grid').removeClass('active');
				
				jQuery('#archive-product .shop-products').addClass('list-view');
				jQuery('#archive-product .shop-products').removeClass('grid-view');
				
				jQuery('#archive-product .list-col4').addClass('col-xs-12 col-sm-6 col-lg-4');
				jQuery('#archive-product .list-col8').addClass('col-xs-12 col-sm-6 col-lg-8');
			});
		});
	";
	
	$customJS = ($layout_product == 'gridview') ? $customJSGrid : $customJSList;
	wp_add_inline_script('sentech-theme-js', $customJS);
}
add_filter( 'woocommerce_before_shop_loop', 'sentech_view_mode_woocommerce_shop_loop', 5 );

function sentech_view_mode_woocommerce_shop_loop_url() {
	$view = 'gridview';
	if(isset($_GET['view']) && $_GET['view']!=''){
		$view = $_GET['view'];
		$customJSGrid = "
			jQuery(document).ready(function(){
				jQuery('.view-mode').each(function(){
					/* Grid View */					
					jQuery('#archive-product .view-mode').find('.grid').addClass('active');
					jQuery('#archive-product .view-mode').find('.list').removeClass('active');
					
					jQuery('#archive-product .shop-products').removeClass('list-view');
					jQuery('#archive-product .shop-products').addClass('grid-view');
					
					jQuery('#archive-product .list-col4').removeClass('col-xs-12 col-sm-6 col-lg-4');
					jQuery('#archive-product .list-col8').removeClass('col-xs-12 col-sm-6 col-lg-8');
				});
			});
		";
		
		$customJSList = "
			jQuery(document).ready(function(){
				jQuery('.view-mode').each(function(){
					/* List View */								
					jQuery('#archive-product .view-mode').find('.list').addClass('active');
					jQuery('#archive-product .view-mode').find('.grid').removeClass('active');
					
					jQuery('#archive-product .shop-products').addClass('list-view');
					jQuery('#archive-product .shop-products').removeClass('grid-view');
					
					jQuery('#archive-product .list-col4').addClass('col-xs-12 col-sm-6 col-lg-4');
					jQuery('#archive-product .list-col8').addClass('col-xs-12 col-sm-6 col-lg-8');
				});
			});
		";
		
		$customJS = ($view == 'gridview') ? $customJSGrid : $customJSList;
		wp_add_inline_script('sentech-theme-js', $customJS);
	}
}
add_filter( 'woocommerce_after_girdview', 'sentech_view_mode_woocommerce_shop_loop_url', 30 );

/*Remove product link open*/
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );


/*Close div*/

//Single product organize
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
add_action	( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 25 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

add_action( 'woocommerce_show_related_products', 'woocommerce_output_related_products', 20 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_short_description', 10 );

// Add previous and next links to products under the product details
add_action( 'woocommerce_single_product_summary', 'sentech_next_prev_products_links', 1 );
function sentech_next_prev_products_links() {
	?><div class="product-nav pull-right">
		<div class="next-prev">
			<div class="prev"><?php previous_post_link('%link'); ?></div>
			<div class="next"><?php next_post_link('%link'); ?></div>
		</div>
	</div>
	<?php
}

function sentech_single_meta(){
	global $sentech_options;
	//remove single meta
	if(isset($sentech_options['single_meta']) && $sentech_options['single_meta']) {
	  remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
	}
}
add_action( 'init', 'sentech_single_meta' );

//Display social sharing on product page
function sentech_woocommerce_social_share(){
	global $sentech_options;
?>
	<div class="share_buttons">
		<?php if ( isset($sentech_options['share_code']) && $sentech_options['share_code']!='') {
			echo wp_kses($sentech_options['share_code'], array(
				'div' => array(
					'class' => array()
				),
				'span' => array(
					'class' => array(),
					'displayText' => array()
				),
			));
		} ?>
	</div>
<?php
}
add_action( 'vg_social_share', 'sentech_woocommerce_social_share', 35 );

//Display stock status on product page
function sentech_product_stock_status(){
	global $product;
	?>
	<div class="in-stock">
		<?php esc_html_e('Availability:', 'vg-sentech');?> 
		<?php if($product->is_in_stock()){ ?>
			<span><?php echo $product->get_stock_quantity()." "; ?><?php esc_html_e('In stock', 'vg-sentech');?></span>
		<?php } else { ?>
			<span class="out-stock"><?php esc_html_e('Out of stock', 'vg-sentech');?></span>
		<?php } ?>	
	</div>
	<?php
}
add_action( 'woocommerce_single_product_summary', 'sentech_product_stock_status', 15 ); 
 
//Show countdown on product page
function sentech_product_countdown(){
	global $product;
	$product_countdown ='';
	if ($product->is_in_stock()) {
	?>
		<?php
		$countdown = false;
		$sale_end = get_post_meta( $product->get_id(), '_sale_price_dates_to', true );
		/* simple product */
		if($sale_end){
			$countdown = true;
			$sale_end = date('Y/m/d', (int)$sale_end);
			?>
			<?php $product_countdown .='<div class="box-timer"><div class="timer-grid" data-time="'.esc_attr($sale_end).'"></div></div>'; ?>
		<?php } ?>
		<?php /* variable product */
		if($product->get_children()){
			$vsale_end = array();
			
			foreach($product->get_children() as $pvariable){
				$vsale_end[] = (int)get_post_meta( $pvariable, '_sale_price_dates_to', true );
				
				if( get_post_meta( $pvariable, '_sale_price_dates_to', true ) ){
					$countdown = true;
				}
			}
			if($countdown){
				/* get the latest time */
					$vsale_end_date = max($vsale_end);
					$vsale_end_date = date('Y/m/d', $vsale_end_date);
					?>
					<?php $product_countdown .='<div class="box-timer"><div class="timer-grid" data-time="'.esc_attr($vsale_end_date).'"></div></div>'; ?>
				<?php
			}
		}
		?>
	<?php
	}
	echo $product_countdown;
}

add_action( 'woocommerce_single_product_summary', 'sentech_product_countdown', 35 ); 
add_action( 'get_sentech_product_countdown', 'sentech_product_countdown' ); 
 
//Show buttons wishlist, compare, email on product page
function sentech_product_buttons(){
	global $product;
	?>
	<?php if(class_exists('YITH_Woocompare') || class_exists('YITH_WCWL'))  : ?>
	<div class="actions">
		<div class="action-buttons">
			<div class="add-to-links">
				<?php if(class_exists('YITH_Woocompare'))  : ?>
				<?php echo do_shortcode('[yith_compare_button]') ?>
				<?php endif; ?>
				<?php if(class_exists('YITH_WCWL')) : ?>
					<?php echo preg_replace("/<img[^>]+\>/i", " ", do_shortcode('[yith_wcwl_add_to_wishlist]')); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<?php endif; ?>
	<?php
}
add_action( 'woocommerce_single_product_summary', 'sentech_product_buttons', 30 );

 
//Change search form
function sentech_search_form( $form ) {
	if(get_search_query()!=''){
		$search_str = get_search_query();
	} else {
		$search_str = esc_html__( 'Search...', 'vg-sentech' );
	}
	
	$form = '<form role="search" method="get" id="blogsearchform" class="searchform" action="' . esc_url(home_url( '/' ) ). '" >
	<div class="form-input">
		<input class="input_text" type="text" value="'.esc_attr($search_str).'" name="s" id="search_input" />
		<button class="button" type="submit" id="blogsearchsubmit"><i class="fa fa-search"></i></button>
		<input type="hidden" name="post_type" value="post" />
		</div>
	</form>';
	$form .= '<script type="text/javascript">';
	$form .= 'jQuery(document).ready(function(){
		jQuery("#search_input").focus(function(){
			if(jQuery(this).val()=="'. esc_html__( 'Search...', 'vg-sentech' ).'"){
				jQuery(this).val("");
			}
		});
		jQuery("#search_input").focusout(function(){
			if(jQuery(this).val()==""){
				jQuery(this).val("'. esc_html__( 'Search...', 'vg-sentech' ).'");
			}
		});
		jQuery("#blogsearchsubmit").click(function(){
			if(jQuery("#search_input").val()=="'. esc_html__( 'Search...', 'vg-sentech' ).'" || jQuery("#search_input").val()==""){
				jQuery("#search_input").focus();
				return false;
			}
		});
	});';
	$form .= '</script>';
	return $form;
}
add_filter( 'get_search_form', 'sentech_search_form' ); 

add_filter( 'woocommerce_breadcrumb_defaults', 'sentech_woocommerce_breadcrumbs', 20, 0  );
function sentech_woocommerce_breadcrumbs() {
    return array(
		'delimiter'   => '<li class="separator"> &gt; </li>',
		'wrap_before' => '<ul id="breadcrumbs" class="breadcrumbs">',
		'wrap_after'  => '</ul>',
		'before'      => '<li class="item">',
		'after'       => '</li>',
		'home'        => _x( 'Home', 'breadcrumb', 'vg-sentech' ),
	);
}


function sentech_limitStringByWord ($string, $maxlength, $suffix = '') {

	if(function_exists( 'mb_strlen' )) {
		// use multibyte functions by Iysov
		if(mb_strlen( $string )<=$maxlength) return $string;
		$string = mb_substr( $string, 0, $maxlength );
		$index = mb_strrpos( $string, ' ' );
		if($index === FALSE) {
			return $string;
		} else {
			return mb_substr( $string, 0, $index ).$suffix;
		}
	} else { // original code here
		if(strlen( $string )<=$maxlength) return $string;
		$string = substr( $string, 0, $maxlength );
		$index = strrpos( $string, ' ' );
		if($index === FALSE) {
			return $string;
		} else {
			return substr( $string, 0, $index ).$suffix;
		}
	}
} 
// Set up the content width value based on the theme's design and stylesheet.
if ( ! isset( $content_width ) )
	$content_width = 625; 


function sentech_setup() {
	/*
	 * Makes sentech Themes available for translation.
	 *
	 * Translations can be added to the /languages/ directory.
	 * If you're building a theme based on VinaGecko, use a find and replace
	 * to change 'vg-sentech' to the name of your theme in all the template files.
	 */
	load_theme_textdomain('vg-sentech', get_template_directory() . '/languages');

	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();

	// Adds RSS feed links to <head> for posts and comments.
	add_theme_support('automatic-feed-links');

	// This theme supports a variety of post formats.
	add_theme_support('post-formats', array('image', 'gallery', 'video', 'audio'));

	// Register menus
	register_nav_menu('primary', esc_html__('Primary Menu', 'vg-sentech'));
	register_nav_menu('top-menu', esc_html__('Top Menu', 'vg-sentech'));
	register_nav_menu('mobilemenu', esc_html__('Mobile Primary Menu', 'vg-sentech'));
	/*swallow code register Category Product*/
	register_nav_menu('mobilemenucategory', esc_html__('Mobile Category Product', 'vg-sentech'));
	register_nav_menu('category-product', esc_html__('Category Product', 'vg-sentech'));

	/*
	 * This theme supports custom background color and image,
	 * and here we also set up the default background color.
	 */
	add_theme_support('custom-background', array(
		'default-color' => 'e6e6e6',
	));
	add_theme_support( "custom-header", array(
		'default-color' => '',
	));
	
	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support('title-tag');
	
	// This theme uses a custom image size for featured images, displayed on "standard" posts.
	add_theme_support('post-thumbnails');

	set_post_thumbnail_size(1170, 9999); // Unlimited height, soft crop
	add_image_size('sentech-category-thumb', 870, 580, true); // (cropped)
	add_image_size('sentech-post-thumb', 300, 200, true); // (cropped)
	add_image_size('sentech-post-thumbwide', 570, 352, true); // (cropped)
}
add_action('after_setup_theme', 'sentech_setup'); 
 
function sentech_get_font_url() {
	$font_url = '';

	/* translators: If there are characters in your language that are not supported
	 * by Open Sans, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Roboto Condensed font: on or off', 'vg-sentech' ) ) {
		$subsets = 'latin,latin-ext';

		/* translators: To add an additional Open Sans character subset specific to your language,
		 * translate this to 'vg-sentech', 'cyrillic' or 'vietnamese'. Do not translate into your own language.
		 */
		$subset = _x( 'no-subset', 'Roboto Condensed font: add new subset (sentech, cyrillic, vietnamese)', 'vg-sentech' );

		if ( 'cyrillic' == $subset )
			$subsets .= ',cyrillic,cyrillic-ext';
		elseif ( 'vg-sentech' == $subset )
			$subsets .= ',sentech,sentech-ext';
		elseif ( 'vietnamese' == $subset )
			$subsets .= ',vietnamese';

		$protocol = is_ssl() ? 'https' : 'http';
		$query_args = array(
			'family' => 'Roboto+Condensed:400,400italic,700,700italic,300italic,300',
			'subset' => $subsets,
		);
		$font_url = add_query_arg( $query_args, "$protocol://fonts.googleapis.com/css" );
	}

	//return $font_url;
}
 
function sentech_scripts_styles() {
	global $wp_styles, $wp_scripts, $sentech_options;
	
	/*
	 * Adds JavaScript to pages with the comment form to support
	 * sites with threaded comments (when in use).
	*/
	
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );
	
	if ( !is_admin()) {
		// Add Bootstrap JavaScript
		wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '3.2.0', true );
		
		// Google Map JS
		wp_enqueue_script( 'google-js', 'http://maps.google.com/maps/api/js', array('jquery'), '3.2.0', true );
		
		// Add jQuery Cookie
		wp_enqueue_script('jquery-cookie', get_template_directory_uri() . '/js/jquery.cookie.js', array('jquery'), '1.4.1', true);	
	
		// Add Fancybox
		wp_enqueue_script( 'jquery-fancybox', get_template_directory_uri() . '/js/fancybox/jquery.fancybox.pack.js', array('jquery'), '2.1.5', true );
		wp_enqueue_style( 'jquery-fancybox-css', get_template_directory_uri() . '/js/fancybox/jquery.fancybox.css', array(), '2.1.5' );
		wp_enqueue_script('jquery-fancybox-buttons', get_template_directory_uri() . '/js/fancybox/helpers/jquery.fancybox-buttons.js', array('jquery'), '1.0.5', true);
		wp_enqueue_style('jquery-fancybox-buttons', get_template_directory_uri() . '/js/fancybox/helpers/jquery.fancybox-buttons.css', array(), '1.0.5');
	
		//Superfish
		wp_enqueue_script( 'superfish-js', get_template_directory_uri() . '/js/superfish/superfish.min.js', array('jquery'), '1.3.15', true );
	
		//Add Shuffle js
		wp_enqueue_script( 'modernizr-custom-js', get_template_directory_uri() . '/js/modernizr.custom.min.js', array('jquery'), '2.6.2', true );
		wp_enqueue_script( 'shuffle-js', get_template_directory_uri() . '/js/jquery.shuffle.min.js', array('jquery'), '3.0.0', true );
	
		// Add owl.carousel files
		wp_enqueue_script('owl.carousel', 	get_template_directory_uri() . '/js/owl.carousel.js', array('jquery'));
		wp_enqueue_style('owl.carousel', 	get_template_directory_uri() . '/css/owl.carousel.css');
		wp_enqueue_style('owl.theme', 		get_template_directory_uri() . '/css/owl.theme.css');
	
		// Add jQuery countdown file
		wp_enqueue_script( 'countdown', get_template_directory_uri() . '/js/jquery.countdown.min.js', array('jquery'), '2.0.4', true );
	
		// Add Plugin JS
		wp_enqueue_script( 'plugins', get_template_directory_uri() . '/js/plugins.js', array('jquery'), '20160115', true );
		
	
		// Add theme.js file
		wp_enqueue_script( 'sentech-theme-js', get_template_directory_uri() . '/js/theme.js', array('jquery'), '20140826', true );
	}
	
	$font_url = sentech_get_font_url();
	if ( ! empty( $font_url ) )
		wp_enqueue_style( 'sentech-fonts', esc_url_raw( $font_url ), array(), null );
	
	if ( !is_admin()) {
	
		//Swallow Code Custom elements Visual Composer
		// Load Icon picker fonts:
		wp_enqueue_style( 'myaddonfont', get_template_directory_uri() . '/css/myaddonfont.min.css' , array(), '1.0' );

		//End Swallow Code Custom elements Visual Composer
	
		// Loads our main stylesheet.
		wp_enqueue_style( 'sentech-style', get_stylesheet_uri() );
		
		// Load bootstrap css
		if ( !is_rtl() ) {
			wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.3.5' );
		}
		
		// Load elegant css
		wp_enqueue_style( 'elegant-css', get_template_directory_uri() . '/css/elegant-style.css', array(), '1.0');
		
		// Load elegant css
		wp_enqueue_style( 'themify-style', get_template_directory_uri() . '/css/themify-icons.css', array(), '1.0');
		
		// Load fontawesome css
		wp_enqueue_style( 'fontawesome-css', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '4.2.0' );
		
		if ( is_rtl() ) {
			wp_enqueue_style( 'sentech-rtl', get_template_directory_uri() . '/css/bootstrap-rtl.min.css', array(), '3.3.2-rc1' );
		}
	}
	// Compile Less to CSS
	
	// HieuJa get Preset Color Option
	$presetopt = sentech_get_preset();	
	// HieuJa end block
	
	if (isset($sentech_options['bodyfont']['font-family']) && $sentech_options['bodyfont']['font-family']) {
		$bodyfont = $sentech_options['bodyfont']['font-family'];
	} else {
		$bodyfont = 'Roboto Condensed';
	}
	
	if(isset($sentech_options['enable_less']) && $sentech_options['enable_less']){
		$themevariables = array(
			'body_font'					=> $bodyfont,
			'primary_color' 			=> $sentech_options['primary_color']['regular'],
			'primary_hover_color' 		=> $sentech_options['primary_color']['hover'],
			'primary_color2' 			=> $sentech_options['primary_color2'],			
			'text_color'				=> $sentech_options['text_color'],
			'top_header_color'			=> $sentech_options['top_header_color'],
			'header_color'				=> $sentech_options['header_color'],
			'menu_color'				=> $sentech_options['menu_color'],
			'cart_color' 				=> $sentech_options['cart_color']['regular'],
			'cart_hover_color' 			=> $sentech_options['cart_color']['hover'],
			'botom_color' 				=> $sentech_options['botom_color'],
			'footer_color' 				=> $sentech_options['footer_color'],
			'buttonqwc_color' 			=> $sentech_options['buttonqwc_color'],
			'button_color' 				=> $sentech_options['button_color'],
			'pagination_color' 			=> $sentech_options['pagination_color'],
			'sale_color'				=> $sentech_options['sale_color'],
			'featured_color'			=> $sentech_options['featured_color'],
			'rate_color' 				=> $sentech_options['rate_color'],
			'presetopt' 				=> $presetopt,
		);
		switch ($presetopt) {
			case 2:
				$themevariables['primary_color'] 		= $sentech_options['primary2_color']['regular'];
				$themevariables['primary_hover_color'] 	= $sentech_options['primary2_color']['hover'];				
				$themevariables['primary_color2'] 		= $sentech_options['primary2_color2'];
				$themevariables['text_color']			= $sentech_options['text2_color'];
				$themevariables['top_header_color']		= $sentech_options['top_header2_color'];
				$themevariables['header_color']			= $sentech_options['header2_color'];
				$themevariables['menu_color']			= $sentech_options['menu2_color'];
				$themevariables['cart_color']			= $sentech_options['cart2_color']['regular'];
				$themevariables['cart_hover_color']		= $sentech_options['cart2_color']['hover'];
				$themevariables['botom_color']			= $sentech_options['botom2_color'];
				$themevariables['footer_color']			= $sentech_options['footer2_color'];
				$themevariables['buttonqwc_color']		= $sentech_options['buttonqwc2_color'];
				$themevariables['button_color']			= $sentech_options['button2_color'];
				$themevariables['pagination_color']		= $sentech_options['pagination2_color'];
				$themevariables['sale_color']			= $sentech_options['sale2_color'];
				$themevariables['featured_color']		= $sentech_options['featured2_color'];
				$themevariables['rate_color']	 		= $sentech_options['rate2_color'];
			break;
			case 3:
				$themevariables['primary_color'] 		= $sentech_options['primary3_color']['regular'];
				$themevariables['primary_hover_color'] 	= $sentech_options['primary3_color']['hover'];
				$themevariables['primary_color2'] 		= $sentech_options['primary3_color2'];
				$themevariables['text_color']			= $sentech_options['text3_color'];
				$themevariables['top_header_color']		= $sentech_options['top_header3_color'];
				$themevariables['header_color']			= $sentech_options['header3_color'];
				$themevariables['menu_color']			= $sentech_options['menu3_color'];
				$themevariables['cart_color']			= $sentech_options['cart3_color']['regular'];
				$themevariables['cart_hover_color']		= $sentech_options['cart3_color']['hover'];
				$themevariables['botom_color']			= $sentech_options['botom3_color'];
				$themevariables['footer_color']			= $sentech_options['footer3_color'];
				$themevariables['buttonqwc_color']		= $sentech_options['buttonqwc3_color'];
				$themevariables['button_color']			= $sentech_options['button3_color'];
				$themevariables['pagination_color']		= $sentech_options['pagination3_color'];
				$themevariables['sale_color']			= $sentech_options['sale3_color'];
				$themevariables['featured_color']		= $sentech_options['featured3_color'];
				$themevariables['rate_color']	 		= $sentech_options['rate3_color'];
			break;
			case 4:
				$themevariables['primary_color'] 		= $sentech_options['primary4_color']['regular'];
				$themevariables['primary_hover_color'] 	= $sentech_options['primary4_color']['hover'];
				$themevariables['primary_color2'] 		= $sentech_options['primary4_color2'];
				$themevariables['text_color']			= $sentech_options['text4_color'];
				$themevariables['top_header_color']		= $sentech_options['top_header4_color'];
				$themevariables['header_color']			= $sentech_options['header4_color'];
				$themevariables['menu_color']			= $sentech_options['menu4_color'];
				$themevariables['cart_color']			= $sentech_options['cart4_color']['regular'];
				$themevariables['cart_hover_color']		= $sentech_options['cart4_color']['hover'];
				$themevariables['botom_color']			= $sentech_options['botom4_color'];
				$themevariables['footer_color']			= $sentech_options['footer4_color'];
				$themevariables['buttonqwc_color']		= $sentech_options['buttonqwc4_color'];
				$themevariables['button_color']			= $sentech_options['button4_color'];
				$themevariables['pagination_color']		= $sentech_options['pagination4_color'];
				$themevariables['sale_color']			= $sentech_options['sale4_color'];
				$themevariables['featured_color']		= $sentech_options['featured4_color'];
				$themevariables['rate_color']	 		= $sentech_options['rate4_color'];
			break;
			case 5:
				$themevariables['primary_color'] 		= $sentech_options['primary5_color']['regular'];
				$themevariables['primary_hover_color'] 	= $sentech_options['primary5_color']['hover'];
				$themevariables['primary_color2'] 		= $sentech_options['primary5_color2'];
				$themevariables['text_color']			= $sentech_options['text5_color'];
				$themevariables['top_header_color']		= $sentech_options['top_header5_color'];
				$themevariables['header_color']			= $sentech_options['header5_color'];
				$themevariables['menu_color']			= $sentech_options['menu5_color'];
				$themevariables['cart_color']			= $sentech_options['cart5_color']['regular'];
				$themevariables['cart_hover_color']		= $sentech_options['cart5_color']['hover'];
				$themevariables['botom_color']			= $sentech_options['botom5_color'];
				$themevariables['footer_color']			= $sentech_options['footer5_color'];
				$themevariables['buttonqwc_color']		= $sentech_options['buttonqwc5_color'];
				$themevariables['button_color']			= $sentech_options['button5_color'];
				$themevariables['pagination_color']		= $sentech_options['pagination5_color'];
				$themevariables['sale_color']			= $sentech_options['sale5_color'];
				$themevariables['featured_color']		= $sentech_options['featured5_color'];
				$themevariables['rate_color']	 		= $sentech_options['rate5_color'];
			break;		
		}
		if( function_exists('compileLessFile') ){
			compileLessFile('theme.less', 'theme'.$presetopt.'.css', $themevariables);
			compileLessFile('compare.less', 'compare'.$presetopt.'.css', $themevariables);
			compileLessFile('ie.less', 'ie'.$presetopt.'.css', $themevariables);
		}
	}
	
	if ( !is_admin()) {
		if(isset($presetopt)){
			// Load main theme css style
			wp_enqueue_style( 'sentech-css', get_template_directory_uri() . '/css/theme'.$presetopt.'.css', array(), '1.0.0' );
			//Compare CSS
			wp_enqueue_style( 'sentech-css', get_template_directory_uri() . '/css/compare'.$presetopt.'.css', array(), '1.0.0' );
			// Loads the Internet Explorer specific stylesheet.
			wp_enqueue_style( 'sentech-ie', get_template_directory_uri() . '/css/ie'.$presetopt.'.css', array( 'sentech-style' ), '20152907' );
		} else {
			// Load main theme css style
			wp_enqueue_style( 'sentech-css', get_template_directory_uri() . '/css/theme1.css', array(), '1.0.0' );
			//Compare CSS
			wp_enqueue_style( 'sentech-css', get_template_directory_uri() . '/css/compare1.css', array(), '1.0.0' );
			// Loads the Internet Explorer specific stylesheet.
			wp_enqueue_style( 'sentech-ie', get_template_directory_uri() . '/css/ie1.css', array( 'sentech-style' ), '20152907' );
		}
		$wp_styles->add_data( 'sentech-ie', 'conditional', 'lte IE 9' );
	}
	
	// Load styleswitcher css style
	wp_enqueue_style( 'sentech-styleswitcher-css', get_template_directory_uri() . '/css/styleswitcher.css', array(), '1.0.0' );
	if(isset($sentech_options['enable_sswitcher']) && $sentech_options['enable_sswitcher']){
		// Add styleswitcher.js file
		wp_enqueue_script( 'sentech-styleswitcher-js', get_template_directory_uri() . '/js/styleswitcher.js', array(), '20140826', true );
	}
	if ( is_rtl() ) {
		wp_enqueue_style( 'sentech-rtl', get_template_directory_uri() . '/rtl.css', array(), '1.0.0' );
	}
}
add_action( 'wp_enqueue_scripts', 'sentech_scripts_styles' );

//Swallow Code Custom elements Visual Composer        
function sentech_load_custom_wp_admin_style() {
	wp_enqueue_style( 'vg_js_composer_icon', get_template_directory_uri() . '/css/vg_js_composer_icon.css', array(), '1.0.0' );
}
add_action( 'admin_enqueue_scripts', 'sentech_load_custom_wp_admin_style' );
//End Swallow Code Custom elements Visual Composer

//Include
if (!class_exists('sentech_widgets') && file_exists(get_template_directory().'/include/vinawidgets.php')) {
    require_once(get_template_directory().'/include/vinawidgets.php');
}

/* Widgets list */
$vg_widgets = array(
    'vg_aboutme.php',
    'vg_contact.php',
    'vg_megamenu.php',
    'vg_woocarousel.php',
    'vg_postcarousel.php',
);

foreach ( $vg_widgets as $widget ) {
	require get_template_directory().'/include/widgets/'. $widget;
}

if (file_exists(get_template_directory().'/include/styleswitcher.php')) {
    require_once(get_template_directory().'/include/styleswitcher.php');
}
if (file_exists(get_template_directory().'/include/breadcrumbs.php')) {
    require_once(get_template_directory().'/include/breadcrumbs.php');
}
if (file_exists(get_template_directory().'/include/wooajax.php')) {
    require_once(get_template_directory().'/include/wooajax.php');
}
if (file_exists(get_template_directory().'/include/shortcodes.php')) {
    require_once(get_template_directory().'/include/shortcodes.php');
}
 
function sentech_mce_css( $mce_css ) {
	$font_url = sentech_get_font_url();

	if ( empty( $font_url ) )
		return $mce_css;

	if ( ! empty( $mce_css ) )
		$mce_css .= ',';

	$mce_css .= esc_url_raw( str_replace( ',', '%2C', $font_url ) );

	return $mce_css;
}
add_filter( 'mce_css', 'sentech_mce_css' ); 

/**
 * Filter the page menu arguments.
 *
 * Makes our wp_nav_menu() fallback -- wp_page_menu() -- show a home link.
 *
 * @since VinaGecko 1.0
 */
function sentech_page_menu_args( $args ) {
	if ( ! isset( $args['show_home'] ) )
		$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'sentech_page_menu_args' );

/**
 * Register sidebars.
 *
 * Registers our main widget area and the front page widget areas.
 *
 * @since VinaGecko 1.0
 */
function sentech_widgets_init() {
	register_sidebar(array(
		'name' => esc_html__('VG Blog Sidebar', 'vg-sentech'),
		'id' => 'sidebar-1',
		'description' => esc_html__('Sidebar on blog page', 'vg-sentech'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<div class="vg-title widget-title"><h3>',
		'after_title' => '</h3></div>',
	));	
	register_sidebar(array(
		'name' => esc_html__('VG Top Header', 'vg-sentech'),
		'id' => 'sidebar-vg-top-header',
		'description' => esc_html__('Sidebar on Top Header', 'vg-sentech'),
		'before_widget' => '<div id="%1$s" class="widget col-md-6 col-sm-12 col-xs-12 %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="vg-title widget-title"><h3>',
		'after_title' => '</h3></div>',
	));	
	
	register_sidebar(array(
		'name' => esc_html__('VG Main Category', 'vg-sentech'),
		'id' => 'category-page',
		'description' => esc_html__('Category on content pages', 'vg-sentech'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<div class="vg-title widget-title"><h3>',
		'after_title' => '</h3></div>',
	));	
	
	register_sidebar(array(
		'name' => esc_html__('VG Category Sidebar', 'vg-sentech'),
		'id' => 'sidebar-category',
		'description' => esc_html__('Sidebar on product category page', 'vg-sentech'),
		'before_widget' => '<aside id="%1$s" class="widget vg-widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<div class="vg-title widget-title"><h3>',
		'after_title' => '</h3></div>',
	));
	register_sidebar(array(
		'name' => esc_html__('VG Layout Product Sidebar', 'vg-sentech'),
		'id' => 'sidebar-product',
		'description' => esc_html__('VG Layout Sidebar on product page', 'vg-sentech'),
		'before_widget' => '<aside id="%1$s" class="widget vg-widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<div class="vg-title widget-title"><h3>',
		'after_title' => '</h3></div>',
	));
	
	register_sidebar(array(
		'name' => esc_html__('VG Twitter Feed Bottom', 'vg-sentech'),
		'id' => 'sidebar-botmain',
		'description' => esc_html__('Twitter Feed Bottom on content pages', 'vg-sentech'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<div class="vg-title widget-title"><h3>',
		'after_title' => '</h3></div>',
	));
		
	register_sidebar(array(
		'name' => esc_html__('VG Widget Bottom', 'vg-sentech'),
		'id' => 'bottom',
		'class' => 'bottom',
		'description' => esc_html__('Widget on bottom', 'vg-sentech'),
		'before_widget' => '<div class="widget vg-bottom-menu text-left col-lg-4 col-md-4 col-sm-4 col-xs-12">',
		'after_widget' => '</div>',
		'before_title' => '<div class="vg-title bottom-static-title"><h3>',
		'after_title' => '</h3></div>',
	));	
}
add_action('widgets_init', 'sentech_widgets_init');

if (! function_exists('sentech_content_nav')) :
/**
 * Displays navigation to next/previous pages when applicable.
 *
 * @since VinaGecko 1.0
 */
function sentech_content_nav( $html_id ) {
	global $wp_query;

	$html_id = esc_attr( $html_id );

	if ( $wp_query->max_num_pages > 1 ) : ?>
		<nav id="<?php echo esc_attr($html_id); ?>" class="navigation" role="navigation">
			<h3 class="assistive-text"><?php esc_html_e( 'Post navigation', 'vg-sentech' ); ?></h3>
			<div class="nav-previous"><?php next_posts_link(wp_kses(__('<span class="meta-nav">&larr;</span> Older posts', 'vg-sentech'), array('span' => array('class' => array())))); ?></div>
			<div class="nav-next"><?php previous_posts_link(wp_kses(__('Newer posts <span class="meta-nav">&rarr;</span>', 'vg-sentech'), array('span' => array('class' => array())))); ?></div>
		</nav><!-- #<?php echo esc_attr($html_id); ?> .navigation -->
	<?php endif;
}
endif;

if ( ! function_exists( 'sentech_pagination' ) ) :
/* Pagination */
function sentech_pagination() {
	global $wp_query;

	$big = 999999999; // need an unlikely integer
	
	echo paginate_links( array(
		'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format' => '?paged=%#%',
		'current' => max( 1, get_query_var('paged') ),
		'total' => $wp_query->max_num_pages,
		'prev_text'    => wp_kses(__('<i class="fa fa-chevron-left"></i>', 'vg-sentech'), array('i' => array('class' => array()))),
		'next_text'    => wp_kses(__('<i class="fa fa-chevron-right"></i>', 'vg-sentech'), array('i' => array('class' => array()))),
	) );
}
endif;

if ( ! function_exists( 'sentech_entry_meta' ) ) :
	function sentech_entry_meta() {
		// Translators: used between list items, there is a space after the comma.
		$categories_list = '<span class="categories-list"><i class="fa fa-folder-o"></i>'. get_the_category_list(esc_html__( ', ', 'vg-sentech' ) ) .'</span>';
		
		$date = sprintf( '<span class="posted-on"><i class="fa fa-calendar"></i><a href="'. get_permalink() .'" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a></span>',
			esc_url( get_permalink() ),
			esc_attr( get_the_time() ),
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() )
		);

		$author = sprintf( '<span class="author vcard"><i class="fa fa-user"></i><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
			esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
			esc_attr( sprintf( esc_html__( 'View all posts by %s', 'vg-sentech' ), get_the_author() ) ),
			get_the_author()
		);
		
		$num_comments = (int)get_comments_number();
		$write_comments = '';
		if ( comments_open() ) {
			if ( $num_comments == 0 ) {
				$comments = esc_html__('0 comments', 'vg-sentech');
			} elseif ( $num_comments > 1 ) {
				$comments = $num_comments . esc_html__(' comments', 'vg-sentech');
			} else {
				$comments = esc_html__('1 comment', 'vg-sentech');
			}
			$write_comments = '<span class="comments-link"><i class="fa fa-comments-o"></i><a href="' . get_comments_link() .'" class="link-comment">'. $comments.'</a></span>';
		}

		// Translators: 1 is author's name, 2 is date, 3 is the tags and 4 is comments.
		
		$utility_text = wp_kses(__( '%1$s%2$s%3$s%4$s', 'vg-sentech' ), array());
		printf( $utility_text, $author, $date, $categories_list, $write_comments );
	}
	
endif;

if ( ! function_exists( 'sentech_entry_meta_tags' ) ) :
	function sentech_entry_meta_tags() {
		// Translators: used between list items, there is a space after the comma.
		$tag_list = '<div class="entry-tags"><span> '. esc_html__( 'Tags: ', 'vg-sentech' ) .' </span>'. get_the_tag_list( '', esc_html__( ', ', 'vg-sentech' ) ) .'</div>';
		
		printf( $tag_list );
	}
endif;

function sentech_entry_meta_small() {
	
	$date = sprintf( '<span class="posted-on"><i class="fa fa-calendar"></i><a href="'. get_permalink() .'" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a></span>',
		esc_url( get_permalink() ),
		esc_attr( get_the_time() ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() )
	);

	$author = sprintf( '<span class="author vcard"><i class="fa fa-user"></i><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
		esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
		esc_attr( sprintf( esc_html__( 'View all posts by %s', 'vg-sentech' ), get_the_author() ) ),
		get_the_author()
	);
	
	$num_comments = (int)get_comments_number();
	$write_comments = '';
	if ( comments_open() ) {
		if ( $num_comments == 0 ) {
			$comments = esc_html__('0 comments', 'vg-sentech');
		} elseif ( $num_comments > 1 ) {
			$comments = $num_comments . esc_html__(' comments', 'vg-sentech');
		} else {
			$comments = esc_html__('1 comment', 'vg-sentech');
		}
		$write_comments = '<span class="comments-link"><i class="fa fa-comments-o"></i><a href="' . get_comments_link() .'" class="link-comment">'. $comments.'</a></span>';
	}
	
	$utility_text = wp_kses(__( '<div class="entry-meta">%1$s%2$s%3$s</div>', 'vg-sentech' ), array('div' => array('class' => array())));
	
	printf( $utility_text, $author, $date, $write_comments );
}

function sentech_add_meta_box() {

	$screens = array( 'post' );

	foreach ( $screens as $screen ) {

		add_meta_box(
			'sentech_post_intro_section',
			esc_html__( 'Post featured content', 'vg-sentech' ),
			'sentech_meta_box_callback',
			$screen
		);
	}
}
add_action( 'add_meta_boxes', 'sentech_add_meta_box' );

function sentech_meta_box_callback( $post ) {

	// Add an nonce field so we can check for it later.
	wp_nonce_field( 'sentech_meta_box', 'sentech_meta_box_nonce' );

	/*
	 * Use get_post_meta() to retrieve an existing value
	 * from the database and use the value for the form.
	 */
	$value = get_post_meta( $post->ID, '_sentech_meta_value_key', true );

	echo '<label for="sentech_post_intro">';
	esc_html_e( 'This content will be used to replace the featured image, use shortcode here', 'vg-sentech' );
	echo '</label><br />';
	//echo '<textarea id="sentech_post_intro" name="sentech_post_intro" rows="5" cols="50" />' . esc_attr( $value ) . '</textarea>';
	wp_editor( $value, 'sentech_post_intro', $settings = array() );
	
	
}

function sentech_save_meta_box_data( $post_id ) {

	/*
	 * We need to verify this came from our screen and with proper authorization,
	 * because the save_post action can be triggered at other times.
	 */

	// Check if our nonce is set.
	if ( ! isset( $_POST['sentech_meta_box_nonce'] ) ) {
		return;
	}

	// Verify that the nonce is valid.
	if ( ! wp_verify_nonce( $_POST['sentech_meta_box_nonce'], 'sentech_meta_box' ) ) {
		return;
	}

	// If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	// Check the user's permissions.
	if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

		if ( ! current_user_can( 'edit_page', $post_id ) ) {
			return;
		}

	} else {

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}
	}

	/* OK, it's safe for us to save the data now. */
	
	// Make sure that it is set.
	if ( ! isset( $_POST['sentech_post_intro'] ) ) {
		return;
	}

	// Sanitize user input.
	$my_data = sanitize_text_field( $_POST['sentech_post_intro'] );

	// Update the meta field in the database.
	update_post_meta( $post_id, '_sentech_meta_value_key', $my_data );
}
add_action( 'save_post', 'sentech_save_meta_box_data' );

if ( ! function_exists( 'sentech_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * To override this walker in a child theme without modifying the comments template
 * simply create your own sentech_comment(), and that function will be used instead.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 *
 * @since VinaGecko 1.0
 */
function sentech_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
		// Display trackbacks differently than normal comments.
	?>
	<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
		<p><?php esc_html_e( 'Pingback:', 'vg-sentech' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link(esc_html__( '(Edit)', 'vg-sentech' ), '<span class="edit-link">', '</span>' ); ?></p>
	<?php
			break;
		default :
		// Proceed with normal comments.
		global $post;
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<article id="comment-<?php comment_ID(); ?>" class="comment">
			<div class="comment-avatar">
				<?php echo get_avatar( $comment, 50 ); ?>
			</div>
			<div class="comment-info">
				<header class="comment-meta comment-author vcard">
					<?php
						
						printf( '<cite><b class="fn">%1$s</b> %2$s</cite>',
							get_comment_author_link(),
							// If current post author is also comment author, make it known visually.
							( $comment->user_id === $post->post_author ) ? '<span>' . esc_html__( 'Post author', 'vg-sentech' ) . '</span>' : ''
						);
						printf( '<time datetime="%1$s">%2$s</time>',
							get_comment_time( 'c' ),
							/* translators: 1: date, 2: time */
							sprintf( esc_html__( '%1$s at %2$s', 'vg-sentech' ), get_comment_date(), get_comment_time() )
						);
					?>
					<div class="reply">
						<?php comment_reply_link( array_merge( $args, array( 'reply_text' => esc_html__( 'Reply', 'vg-sentech' ), 'after' => '', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
					</div><!-- .reply -->
				</header><!-- .comment-meta -->
				<?php if ( '0' == $comment->comment_approved ) : ?>
					<p class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'vg-sentech' ); ?></p>
				<?php endif; ?>

				<section class="comment-content comment">
					<?php comment_text(); ?>
					<?php edit_comment_link( esc_html__( 'Edit', 'vg-sentech' ), '<p class="edit-link">', '</p>' ); ?>
				</section><!-- .comment-content -->
			</div>
		</article><!-- #comment-## -->
	<?php
		break;
	endswitch; // end comment_type check
}
endif;
if ( ! function_exists( 'before_comment_fields' ) &&  ! function_exists( 'after_comment_fields' )) :
//Change comment form
function sentech_before_comment_fields() {
	echo '<div class="comment-input">';
}
add_action('comment_form_before_fields', 'sentech_before_comment_fields');

function sentech_after_comment_fields() {
	echo '</div>';
}
add_action('comment_form_after_fields', 'sentech_after_comment_fields');

endif; 
 
function sentech_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
}
add_action( 'customize_register', 'sentech_customize_register' );

/**
 * Enqueue Javascript postMessage handlers for the Customizer.
 *
 * Binds JS handlers to make the Customizer preview reload changes asynchronously.
 *
 * @since VinaGecko 1.0
 */
 
add_action( 'wp_enqueue_scripts', 'sentech_wcqi_enqueue_polyfill' );
function sentech_wcqi_enqueue_polyfill() {
    wp_enqueue_script( 'wcqi-number-polyfill' );
}

/* Remove Redux Demo Link */
function sentech_removeDemoModeLink()
{
    if(class_exists('ReduxFrameworkPlugin')) {
        remove_filter('plugin_row_meta', array(ReduxFrameworkPlugin::get_instance(), 'plugin_metalinks'), null, 2);
    }
    if(class_exists('ReduxFrameworkPlugin')) {
        remove_action('admin_notices', array(ReduxFrameworkPlugin::get_instance(), 'admin_notices'));    
    }
}
add_action('init', 'sentech_removeDemoModeLink');

// HieuJa add specific class
function sentech_add_query_vars_filter($vars){
	$vars[] = "ilayout";
	$vars[] = "preset";
	return $vars;
}
add_filter('query_vars', 'sentech_add_query_vars_filter');

// Get value from URL and set Cookie, Class Subffix
function sentech_body_class($classes)
{	
	global $sentech_options;
	
	// Set Class Layout for Body Tag
	$classes[] = sentech_get_layout();
	
	// Set Class Preset Color for Body Tag
	$classes[] = "preset-" . sentech_get_preset();
	
	return $classes;
}
add_filter('body_class', 'sentech_body_class');

// Override get_header function
function sentech_get_header()
{	
	// Get Header Name
	$header = sentech_get_layout();
	
	get_header($header);	
}

// Override get_footer function
function sentech_get_footer()
{
	// Get Footer Name
	$footer = sentech_get_layout();
	
	get_footer($footer);	
}

// HieuJa get Layout
function sentech_get_layout()
{
	global $sentech_options;
	
	$ilayout = get_query_var('ilayout', '');
		$sentech_options['page_layout'] = isset($sentech_options['page_layout']) ? $sentech_options['page_layout'] : 'layout-1';
	$layout  = (!empty($ilayout)) ? $ilayout : $sentech_options['page_layout'];
	
	return $layout;
}

// HieuJa get Preset Color
function sentech_get_preset()
{
	global $sentech_options;	
	$preset 		= get_query_var('preset', '');
	$sentech_options['preset_option'] = isset($sentech_options['preset_option']) ? $sentech_options['preset_option'] : '1';	
	$presetColor 	= (!empty($preset)) ? $preset : $sentech_options['preset_option'];	
	return $presetColor;
}

// HieuJa set Cookie for Layout and Preset Color
function sentech_set_layout_preset_color()
{
	// Set Layout Style
	$ilayout = get_query_var('ilayout', '');
	if(!empty($ilayout)) {
		setcookie('page-layout', $ilayout, strtotime('+1 day'), '/');
	}
	
	// Set Preset Color
	$preset = get_query_var('preset', '');
	if(!empty($preset)) {
		setcookie('preset-color', $preset, strtotime('+1 day'), '/');
	}
	
	return true;
}

// HieuJa reset Layout Option and Preset Color
function sentech_reset_layout_preset_color()
{
	global $sentech_options;
	
	$requestURI = $_SERVER['REQUEST_URI'];
	$homeURL	= home_url('/');
	$isHomePage = strpos($homeURL, $requestURI);
	
	if($requestURI == '/' || $isHomePage !== false) 
	{
		$pageLayout  = isset($_COOKIE['page-layout']) ? $_COOKIE['page-layout'] : $sentech_options['page_layout'];
		$presetColor = isset($_COOKIE['preset-color']) ? $_COOKIE['preset-color'] : $sentech_options['preset_option'];
		if( (isset($sentech_options['page_layout']) && $pageLayout != $sentech_options['page_layout']) 
		|| (isset($sentech_options['preset_option']) && $presetColor != $sentech_options['preset_option'])) {
			setcookie('page-layout', $sentech_options['page_layout'], strtotime('+1 day'), '/');
			setcookie('preset-color', $sentech_options['preset_option'], strtotime('+1 day'), '/');
			wp_redirect(home_url());
			exit;
		}
	}
}
add_action('init', 'sentech_reset_layout_preset_color');

// HieuJa get global variables
function sentech_get_global_variables($variable = 'sentech_options')
{
    global $woocommerce, $sentech_productrows, $sentech_options, $sentech_productsfound, $product, $woocommerce_loop, $post, $sentech_projectrows, $sentech_secondimage, $wpdb, $wp_query, $is_IE;
    
    switch($variable)
    {
        case "sentech_options":
            return $sentech_options;
        break;
        case "product":
            return $product;
        break;  
        case "woocommerce":
            return $woocommerce;
        break;          
        case "sentech_productrows":
            return $sentech_productrows;
        break;  
        case "sentech_productsfound":
            return $sentech_productsfound;
        break;  
        case "woocommerce_loop":
            return $woocommerce_loop;
        break;  
        case "post":
            return $post;
        break;  
        case "sentech_secondimage":
            return $sentech_secondimage;
        break;  
        case "wpdb":
            return $wpdb;
        break;      
        case "wp_query":
            return $wp_query;
        break;  
        case "is_IE":
            return $is_IE;
        break;
    }
    
    return false;
}


//swallow code sale and featured %
add_filter('woocommerce_featured_flash', 'sentech_change_featured_flash');
function sentech_change_featured_flash() {
	global $sentech_options;
	$sentech_options['featured_label_custom'] = isset($sentech_options['featured_label_custom']) ? $sentech_options['featured_label_custom'] : "";
	
	return '<div class="vgwc-label vgwc-featured"><span>' . $sentech_options['featured_label_custom'] . '</span></div>';
}

function sentech_woothemes_testimonials_html( $html, $query, $args ) {
	$html = str_replace( 'itemprop', 'data-itemprop', $html );
	return $html;
}

add_filter( 'woothemes_testimonials_html', 'sentech_woothemes_testimonials_html', 10, 3 );

add_filter('woocommerce_sale_flash', 'sentech_change_on_sale_flash');
function sentech_change_on_sale_flash() {
	global $product,$post,$sentech_options;
	$sale = '';
	$sentech_options['sale_label'] = isset($sentech_options['sale_label']) ? $sentech_options['sale_label'] : "";
	$sentech_options['sale_label_custom'] = isset($sentech_options['sale_label_custom']) ? $sentech_options['sale_label_custom'] : "";
	$format = $sentech_options['sale_label'];

	if (!empty($format)) {
		if ($format == 'custom') {
			$format = $sentech_options['sale_label_custom'];
		}
		$priceDiff = 0;
		$percentDiff = 0;
		$regularPrice = '';
		$salePrice = '';
		if (!empty($product)) {
			$salePrice = get_post_meta($product->get_id(), '_price', true);
			$regularPrice = get_post_meta($product->get_id(), '_regular_price', true);
			
			if($product->get_children()) {
				foreach ( $product->get_children() as $child_id ) {
					$all_prices[] = get_post_meta( $child_id, '_price', true );
					$all_prices1[] = get_post_meta( $child_id, '_regular_price', true );
					$all_prices2[] = get_post_meta( $child_id, '_sale_price', true );
					if(get_post_meta( $child_id, '_price', true ) == $salePrice) {
						$regularPrice = get_post_meta( $child_id, '_regular_price', true );
					}
				}
			}
		}

		if (!empty($regularPrice) && !empty($salePrice ) && $regularPrice > $salePrice ) {
			$priceDiff = $regularPrice - $salePrice;
			$percentDiff = round($priceDiff / $regularPrice * 100);
			
			$parsed = str_replace('{price-diff}', number_format((float)$priceDiff, 2, '.', ''), $format);
			$parsed = str_replace('{percent-diff}', $percentDiff, $parsed);
			if($product->is_featured()) {
				$sale = '<div class="vgwc-label vgwc-onsale sale-featured"><span>'. $parsed .'</span></div>';
			}
			else {
				$sale = '<div class="vgwc-label vgwc-onsale"><span>'. $parsed .'</span></div>';
			}
		}
	}
	return $sale;
}

function sentech_custom_script_css () {
	global $sentech_options;	
	
	$customCSS  = (isset($sentech_options['advanced_editor_css'])) ? $sentech_options['advanced_editor_css'] : "";
	$customJS = (isset($sentech_options['advanced_editor_js'])) ? $sentech_options['advanced_editor_js'] : "";	
	
	wp_add_inline_style('sentech-style', $customCSS);
	wp_add_inline_script('sentech-theme-js', $customJS);
}
add_action('wp_enqueue_scripts', 'sentech_custom_script_css');

function sentech_max_menu_item () {
	global $sentech_options;
	?>
	<script type="text/javascript">
		var vg_max_menu = false;
		<?php if((isset($sentech_options['show_menu_max']) && $sentech_options['show_menu_max']) && ($sentech_options['show_menu_max'] == true)) { ?>
			vg_max_menu = true;
			var title_more_categories = "<?php echo isset($sentech_options['title_more_categories']) ? $sentech_options['title_more_categories'] : ""; ?>";
			var title_close_menu = "<?php echo isset($sentech_options['title_close_menu']) ? $sentech_options['title_close_menu'] : "" ; ?>";
			var menu_max_number = <?php echo (isset($sentech_options['menu_max_number']) ? $sentech_options['menu_max_number'] : "") + 1; ?>;
		<?php } else { ?>
			vg_max_menu = false;
		<?php } ?>			
	</script>	
	<?php
}
add_action('wp_head', 'sentech_max_menu_item');

function loop_columns() {
	global $sentech_options;
	
	$column = (isset($sentech_options['product_per_row']) && !empty($sentech_options['product_per_row'])) ? $sentech_options['product_per_row'] : 1;
	
	return $column;
}
add_filter('loop_shop_columns', 'loop_columns', 999);
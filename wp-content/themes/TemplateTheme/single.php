<?php 
/**
* The single
* @package d4jtheme
* @subpackage d4jtheme
* @since d4jtheme 1.0
*/
get_header(); ?>
	<div class="container main">
		<div class="row single_content">
			<div class="col middle-column" itemtype="http://schema.org/Recipe" itemscope="">
				<?php while ( have_posts() ) : the_post(); ?>
					<meta content="<?php the_title(); ?>" itemprop="name">
					<meta content="Vietnamese" itemprop="recipeCuisine">
					<meta content="<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' ); echo $thumb['0']; ?>" itemprop="image">	
					<div class="article">
						<div class="article-heading">
							<h2><?php the_title() ; ?></h2>
							<h3><em>by</em> <a rel="author" title="Posts by <?php echo get_the_author() ; ?>" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><?php echo get_the_author() ; ?></a> <em>on </em><span datetime="<?php echo get_the_date(); ?>" itemprop="datePublished"><?php echo get_the_date(); ?></span><em> Posted in</em> <?php the_category(', '); ?></h3>
						</div>
						<div class="article-body">
							<div class="article-intro">
								<?php the_content(); ?>								
								<div class="tag">
									<?php the_tags( 'Tags: ', ', ', '<br />' ); ?> 
								</div>								
							</div>
						</div>
					</div>
				<?php endwhile; ?>
				<div class="article article_meta">
					<div class="article-body">
						<?php comments_template(); ?>
						<div itemtype="http://schema.org/AggregateRating" itemscope="" itemprop="aggregateRating">
							<span class="ratings-metadata">
							  <meta content="4.8" itemprop="ratingValue">
							  <span itemprop="ratingValue" class="ratings-metadata">4.8</span>
							  <span itemprop="bestRating" class="ratings-metadata">5</span>
							</span>
							<span itemprop="reviewCount" class="ratings-metadata"><?php the_ID(); ?></span>
						</div>
					</div>
				</div>
			</div>
			<div class="fivecol last right-column">
				<?php if ( dynamic_sidebar('right-col') ) : else : endif; ?>
			</div>
		</div>
	</div>
<?php get_footer(); ?>

<?php
/**
 * Single Product Up-Sells
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if (! defined('ABSPATH')) exit; // Exit if accessed directly

$woocommerce_loop  	=  sentech_get_global_variables('woocommerce_loop');
$sentech_options  	=  sentech_get_global_variables();
$product  			=  sentech_get_global_variables('product');

if ( $upsells ) : ?>

<div class="widget upsells_products_widget">
	<div class="vg-title">
		<h3><?php echo isset($sentech_options['upsells_title']) ? esc_html($sentech_options['upsells_title']) : "You may also like&hellip:"; ?></h3>
	</div>
	
	<div class="upsells products">

		<?php woocommerce_product_loop_start(); ?>

			<?php foreach ( $upsells as $upsell ) : ?>

				<?php
				 	$post_object = get_post( $upsell->get_id() );

					setup_postdata( $GLOBALS['post'] =& $post_object );

					wc_get_template_part( 'content', 'product' ); ?>

			<?php endforeach; ?>

		<?php woocommerce_product_loop_end(); ?>

	</div>
</div>
<?php endif;

wp_reset_postdata();

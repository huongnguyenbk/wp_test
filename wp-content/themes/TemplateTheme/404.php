<?php
/**
* The template for displaying 404 pages (not found)
* @package d4jtheme
* @subpackage d4jtheme
* @since d4jtheme 1.0
*/
	get_header(); ?>
	<div class="container main">
		<div class="row single_content">
			<div class="col middle-column" itemtype="http://schema.org/Recipe" itemscope="">
				<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'd4jtheme' ); ?></h1>
				<p><?php _e( 'It looks like nothing was found at this location. Report to Webmaster: <a href="http://designforjoomla.com">D4J</a>', 'd4j' ); ?></p>
			</div>
			<div class="fivecol last right-column">
				<?php if ( dynamic_sidebar('right-col') ) : else : endif; ?>
			</div>
		</div>
	</div>
<?php get_footer(); ?>
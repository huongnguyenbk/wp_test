<?php
/**
 * @version    1.1
 * @package    VG Sentech
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */
$sentech_options  = sentech_get_global_variables(); 
$sentech_options['newsletter_form'] = isset($sentech_options['newsletter_form']) ? $sentech_options['newsletter_form'] : "";
$sentech_options['ftsocial_icons'] = isset($sentech_options['ftsocial_icons']) ? $sentech_options['ftsocial_icons'] : "";
// HieuJa get Preset Color Option
$presetopt = sentech_get_preset();
// HieuJa end block

?>

		<?php if( isset($sentech_options['top_bottom_show']) && $sentech_options['top_bottom_show']): ?>
			<div id="footer-corporate">
				<div class="container">
					<div class="row">
						<?php echo isset($sentech_options['top_bottom_content']) ? $sentech_options['top_bottom_content'] : ""; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>

		<div class="top-footer">
			<div class="container">
				<div class="footer-static">
					<div class="row">
						<?php if((isset($sentech_options['about_us_block']) && $sentech_options['about_us_block']) || class_exists('WYSIJA_NL_Widget')): ?>
						<div id="left-footer" class="col-xs-12 col-sm-3">
							<div class="wrapper-left-ft <?php if(class_exists('WYSIJA_NL_Widget')){ echo "vg-width-full"; }else{ echo "vg-width-min" ;} ?>">
								<?php if (class_exists('WYSIJA_NL_Widget')): ?>
									<div class="news-letter">
										<div class="vg-title header-newsletter">
											<h2><?php echo isset($sentech_options['newsletter_title']) ? $sentech_options['newsletter_title'] : ""; ?></h2>
											<p class="des"><?php echo isset($sentech_options['newsletter_des']) ? $sentech_options['newsletter_des'] : ""; ?></p>
										</div>
										<?php echo do_shortcode("[wysija_form id='" . esc_attr($sentech_options['newsletter_form']) . "']"); ?>								
									</div>
								<?php endif; ?>

								<?php if (isset($sentech_options['about_us_block']) && $sentech_options['about_us_block']): ?>
									<div class="vg-title header-about-us">
										<h2><?php echo isset($sentech_options['about_us_title']) ? $sentech_options['about_us_title'] : ""; ?></h2>
										<p class="des"><?php echo isset($sentech_options['about_us_des']) ? $sentech_options['about_us_des'] : ""; ?></p>
									</div>							
								<?php endif; ?>

								<?php if (isset($sentech_options['social_show']) && $sentech_options['social_show']): ?>
									<div class="vg-social social-network">
										<!-- Social -->
										<?php
												echo '<ul class="social-icons">';
												foreach($sentech_options['ftsocial_icons'] as $key=>$value) {
													if($value!=''){
														if($key=='vimeo'){
															echo '<li><a class="'.esc_attr($key).' social-icon" href="'.esc_url($value).'" title="'.ucwords(esc_attr($key)).'" target="_blank"><i class="fa fa-vimeo-square"></i></a></li>';
														} else {
															echo '<li><a class="'.esc_attr($key).' social-icon" href="'.esc_url($value).'" title="'.ucwords(esc_attr($key)).'" target="_blank"><i class="fa fa-'.esc_attr($key).'"></i></a></li>';
														}
													}
												}
												echo '</ul>';
											?>
									</div>
								<?php endif; ?>						
							</div>
						</div><!-- The end Left Footer -->
						<?php endif; ?>	
						
						<div id="right-footer" class="col-xs-12 <?php if ( (isset($sentech_options['newsletter_show']) && $sentech_options['newsletter_show'])   || (isset($sentech_options['about_us_block']) && $sentech_options['about_us_block']) || (isset($sentech_options['social_show']) && $sentech_options['social_show']) ){ echo "col-sm-9"; } ?> ">

							<?php if (is_active_sidebar('sidebar-botmain') || is_active_sidebar('contact-botmain')): ?>
								<div id="top-right-footer">
									<?php if (is_active_sidebar('sidebar-botmain')): ?>
										<div id="tweet-block" class="col-xs-12 <?php 
											if( (isset($sentech_options['contact_info_block']) && $sentech_options['contact_info_block']) && (isset($sentech_options['contact_info_block']) && $sentech_options['contact_info_block'])){ echo "col-sm-4";}else if( (isset($sentech_options['contact_info_block']) && $sentech_options['contact_info_block']) || (isset($sentech_options['contact_info_block']) && $sentech_options['contact_info_block']) ){ echo "col-sm-6"; }else{echo "col-sm-12";}											
										?>">
											<?php dynamic_sidebar('sidebar-botmain'); ?>
										</div><!-- The end tweet-block -->
									<?php endif; ?>

									<?php if (isset($sentech_options['contact_info_block']) && $sentech_options['contact_info_block']): ?>
										<div id="contact-block" class="col-xs-12 <?php
											if(is_active_sidebar('sidebar-botmain') && (isset($sentech_options['contact_info_block']) && $sentech_options['contact_info_block'])){ echo "col-sm-4";}else if(is_active_sidebar('sidebar-botmain') || (isset($sentech_options['contact_info_block']) && $sentech_options['contact_info_block'])){ echo "col-sm-6"; }else{echo "col-sm-12";}
										?>">
											<aside class="widget widget_text">
												<div class="vg-title widget-title">
													<h3><?php if(isset($sentech_options['contact_info_title']) && $sentech_options['contact_info_title']){ echo $sentech_options['contact_info_title']; } ?></h3>
												</div>
												<div class="textwidget">
													<div class="footer-static-content">
														<?php if(isset($sentech_options['contact_info_des']) && $sentech_options['contact_info_des']){ echo $sentech_options['contact_info_des']; } ?>
													</div>
												</div>
											</aside>																				
										</div><!--  The end contact-block -->
									<?php endif ?>
									
									<?php if (isset($sentech_options['map_info_block']) && $sentech_options['map_info_block']): ?>
										<div id="map-block" class="col-xs-12 <?php
											if(is_active_sidebar('sidebar-botmain') && (isset($sentech_options['contact_info_block']) && $sentech_options['contact_info_block'])){ echo "col-sm-4";}else if(is_active_sidebar('sidebar-botmain') || (isset($sentech_options['contact_info_block']) && $sentech_options['contact_info_block'])){ echo "col-sm-6"; }else{ echo "col-sm-12"; }										
										?>">
											<aside class="widget widget_text">
												<div class="textwidget">
													<?php if(isset($sentech_options['map_info_des']) && $sentech_options['map_info_des']){ echo $sentech_options['map_info_des']; } ?>	
												</div>
											</aside>	
										</div><!-- 	The end map-block -->
									<?php endif; ?>
									
								</div><!-- The end Top Right Footer -->
							<?php endif; ?>

							<?php if(is_active_sidebar('bottom')): ?>
								<div id="second-right-footer">
									<?php dynamic_sidebar('bottom'); ?>
								</div><!-- The end second-right-footer -->
							<?php endif; ?>

							<?php if (isset($sentech_options['footer_payment']['url']) && $sentech_options['footer_payment']['url']): ?>
								<div class="vg-payment">
									<img class="payment" src="<?php echo esc_url($sentech_options['footer_payment']['url']); ?>" alt="" />
								</div>
							<?php endif ?>

						</div><!-- The end Right Footer -->
					</div>			
				</div>
			</div>	
		</div><!--  The end Top Footer -->

		<?php if(isset($sentech_options['copyright_show']) && $sentech_options['copyright_show']) { ?>
			<div class="second-footer-wrapper">
				<div class="container">
					<div class="row">
							<div class="col-xs-12 text-center">
								<div class="copyright">
									<?php echo sentech_copyright(); ?>
								</div>
							</div>
					</div>
				</div>
			</div><!-- The end Second Footer -->
		<?php } ?>
			
	</div><!-- .wrapper -->
	<div class="to-top"><i class="fa fa-chevron-up"></i></div>
	<?php wp_footer(); ?>
</body>
</html>
<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global  $woocommerce_loop;

$sentech_options  		= sentech_get_global_variables();
$product  				= sentech_get_global_variables('product');
$sentech_productsfound  	= sentech_get_global_variables('sentech_productsfound');
$post  					= sentech_get_global_variables('post');
//HieuJa check Undefile
$sentech_options['words_short_des'] 	= isset($sentech_options['words_short_des']) ? $sentech_options['words_short_des'] : ""; 
// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) ) {
	$woocommerce_loop['loop'] = 0;
}

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) ) {
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 3 );
}

// Ensure visibility
if ( ! $product || ! $product->is_visible() ) {
	return;
}

// Extra post classes
$classes = array();
if (0 == ($woocommerce_loop['loop']) % $woocommerce_loop['columns'] || 0 == $woocommerce_loop['columns']) {
 $classes[] = 'first';
}

if (0 == ($woocommerce_loop['loop'] + 1) % $woocommerce_loop['columns']) {
 $classes[] = 'last';
}

if(is_product_category()) {
	if (  1 === ( $woocommerce_loop['loop'] % ($woocommerce_loop['columns'] - 1))) {
		$classes[] = 'first-sm';
	}
}

if ($woocommerce_loop['columns'] == 3 ) {
	$colwidth = 4;
	$colwidth_sm = 6;
} else {
	$colwidth = 3;
	$colwidth_sm = 4;
}

$classes[] = ' item-col col-lg-'. esc_attr($colwidth) .' col-sm-'. esc_attr($colwidth_sm) .' col-xs-6';?>

<div <?php post_class($classes); ?>>
	<div class="vgwc-item vertical_1 style1">
		<div class="ma-box-content">
			<?php do_action('woocommerce_before_shop_loop_item'); ?>
			
			<div class="list-col4">
				<div class="vgwc-image-block">
					
					<a href="<?php echo esc_url( get_permalink( $product->get_id() ) ); ?>" title="<?php echo esc_attr( $product->get_title() ); ?>">
						<?php 
						echo $product->get_image('shop_catalog', array('class'=>'primary_image'));
						$image_second = '';
						if(isset($sentech_options['second_image']) && $sentech_options['second_image']){
							$attachment_ids = $product-> get_gallery_image_ids();
							if((isset($attachment_ids[0]) && $attachment_ids[0]) && ($attachment_ids[0] != get_post_thumbnail_id(get_the_ID()))) {
								$image_second = wp_get_attachment_image( $attachment_ids[0], apply_filters( 'single_product_small_thumbnail_size', 'shop_catalog' ), false, array('class'=>'secondary_image') );
							}
							elseif(isset($attachment_ids[1]) && $attachment_ids[1]){
								$image_second = wp_get_attachment_image( $attachment_ids[1], apply_filters( 'single_product_small_thumbnail_size', 'shop_catalog' ), false, array('class'=>'secondary_image') );
							}	
						}
						echo $image_second;
						?>
					</a>
					
					<?php if ($product->is_featured()) : ?>
						<?php echo apply_filters('woocommerce_featured_flash', '<div class="vgwc-label vgwc-featured">' . esc_html__('Hot', 'vg-sentech') . '</div>', $post, $product); ?>
					<?php endif; ?>
					
					<?php if ($product->is_on_sale() && $product->is_featured() ) : ?>
						<?php echo apply_filters('woocommerce_sale_flash', '<div class="vgwc-label vgwc-onsale sale-featured">' . esc_html__('Sale', 'vg-sentech') . '</div>', $post, $product); ?>
					<?php elseif ($product->is_on_sale()) : ?>
						<?php echo apply_filters('woocommerce_sale_flash', '<div class="vgwc-label vgwc-onsale">' . esc_html__('Sale', 'vg-sentech') . '</div>', $post, $product); ?>
					<?php endif; ?>
					
					
					<?php if (class_exists('YITH_WCWL') || class_exists('YITH_Woocompare') || (isset($sentech_options['quick_view']) && $sentech_options['quick_view'])) : ?>
					<div class="vgwc-button-group">											
						<div class="vgwc-wrapper-button">
							<div class="vgwc-add-to-cart">
								<?php echo do_shortcode('[add_to_cart id="'.esc_attr($product->get_id()).'"  style="none" show_price="false"]') ?>
							</div>
							<?php if (class_exists('YITH_WCWL') || class_exists('YITH_Woocompare')) : ?>
								<div class="add-to-links">
								
									<?php if(isset($sentech_options['quick_view']) && $sentech_options['quick_view']) : ?>
										<div class="vgwc-quick">
											<a class="quickview quick-view" data-quick-id="<?php the_ID();?>" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php esc_html_e('Quick View', 'vg-sentech');?></a>
										</div>
									<?php endif; ?>
								
									<?php if (class_exists('YITH_WCWL')) {
										echo '<div class="vgwc-wishlist">'.preg_replace("/<img[^>]+\>/i", " ", do_shortcode('[yith_wcwl_add_to_wishlist]')). '</div>';
									} ?>
									<?php if(class_exists('YITH_Woocompare')) {
										echo '<div class="vgwc-compare">'. do_shortcode('[yith_compare_button]') . '</div>';
									} ?>
									
								</div>
							<?php endif; ?>					
						</div>						
					</div>
					<?php endif; ?>
					
					
					
					<?php do_action('get_sentech_product_countdown');?>
				</div>
			</div>
			<div class="list-col8">
				<div class="gridview">
					<div class="vgwc-text-block">
						<h3 class="vgwc-product-title">
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						</h3>
						<?php echo wc_get_rating_html( $product->get_average_rating()); ?>	
						<?php echo $product->get_price_html(); ?>	
					</div>		
				</div>
				<div class="listview">
					<div class="vgwc-text-block">
						<div class="vgwc-product-title">
							<h3>
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<?php echo wc_get_rating_html( $product->get_average_rating()); ?>	
						</div>
						
						<div class="product-desc"><?php the_excerpt(); ?></div>
						
						<?php echo $product->get_price_html(); ?>
						
						<div class="vgwc-button-group2">
							<div class="vgwc-add-to-cart">
								<?php echo do_shortcode('[add_to_cart id="'.esc_attr($product->get_id()).'"  style="none" show_price="false"]') ?>
							</div>
									
							<div class="add-to-links">
								
								<?php if (class_exists('YITH_WCWL')) {
									echo '<div class="vgwc-wishlist">'.preg_replace("/<img[^>]+\>/i", " ", do_shortcode('[yith_wcwl_add_to_wishlist]')). '</div>';
								} ?>
								<?php if(class_exists('YITH_Woocompare')) {
									echo '<div class="vgwc-compare">'. do_shortcode('[yith_compare_button]') . '</div>';
								} ?>
								<?php if(isset($sentech_options['quick_view']) && $sentech_options['quick_view']) { ?>
									<?php if(($sentech_options['quick_view']) == true) { ?>
										<div class="quick-wrapper">
											<a class="quickview quick-view" data-quick-id="<?php the_ID();?>" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php esc_html_e('Quick View', 'vg-sentech');?></a>
										</div>
									<?php } ?>
								<?php } ?>								
							</div>
							
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<?php //do_action('woocommerce_after_shop_loop_item'); ?>
		</div>
	</div>
</div>

<?php 
/**
* The template for displaying homepage
* @package d4jtheme
* @subpackage d4jtheme
* @since d4jtheme 1.0
*/
get_header(); ?>
	<div class="container main">
		<div class="row">
			<div class="col left-column">
				<div class="tab">Featured Posts</div>
				<div class="sidebar-box gallery-list">
					<?php 
						 $cat_posts = new WP_Query(array('showposts'=>10,'cat'=>$options[leftcat]));
						 while ( $cat_posts->have_posts() ){
						 $cat_posts->the_post();
						 ?>
							<div class="design">
							<h2><a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
							<?php if ( has_post_thumbnail() ){ ?>
								<a href="<?php the_permalink(); ?>">
									<?php the_post_thumbnail('left-thumbnails', array('class' => 'gallery-image')); ?>
								</a>
							<?php }else{ ?>
							<a href="<?php the_permalink(); ?>">
								<img class="gallery-image wp-post-image" src="<?php echo get_template_directory_uri(); ?>/images/key-board.jpg">
							</a>
							<?php } ?>
							<a class="author" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><?php echo get_the_author() ; ?></a>
							</div>
						 <?php
						 }
						 wp_reset_postdata();
					?>
				</div>
			</div>
			<div class="col middle-column-home">
				<div class="tab">Lastest Posts</div>
				<div class="article-feature-home">
					<?php if ( have_posts() ) { $i=1; ?>
						<?php while ( have_posts() ) : the_post(); ?>
							<div class="article-feature-heading">
								<h2><a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
								<h3>by <a rel="author" title="Posts by <?php echo get_the_author() ; ?>" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><?php echo get_the_author() ; ?></a> on <em><?php echo get_the_date(); ?></em></h3>
							</div>
							<div class="article-feature-body">
								<?php if ( has_post_thumbnail()){ ?>
									<a href="<?php the_permalink(); ?>">
									<?php the_post_thumbnail(); ?>
									</a>
								<?php } ?>
								<?php the_excerpt(); ?>
								<div class="tag">
									<?php the_tags( 'Tags: ', ', ', '<br />' ); ?> 
								</div>
							</div>
						<?php $i++; endwhile; ?>
						<?php kriesi_pagination();?>
					<?php } ?>
				</div>
				
			</div>
			<div class="fivecol last right-column">
				<?php if ( dynamic_sidebar('right-col') ) : else : endif; ?>
			</div>
		</div>
	</div>
<?php get_footer(); ?>		
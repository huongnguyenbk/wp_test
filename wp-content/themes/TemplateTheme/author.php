<?php
/**
* The template for displaying author pages
* @package d4jtheme
* @subpackage d4jtheme
* @since d4jtheme 1.0
*/
get_header(); ?>
	<div class="container main">
		<div class="row">
			<div class="col middle-column">
				<?php if ( have_posts() ) : ?>
					<div class="archive-options">
					<?php

					$fname = get_the_author_meta('first_name');
					$lname = get_the_author_meta('last_name');
					$full_name = '';

					if( empty($fname)){
						$full_name = $lname;
					} elseif( empty( $lname )){
						$full_name = $fname;
					} else {
						//both first name and last name are present
						$full_name = "{$lname} {$fname}";
					}

					//echo $full_name;
					?>
						<h2><?php if($full_name){ echo $full_name;}else{ echo get_the_author() ;} ?></h2>
						<div class="author-avatar">
							<?php
							$author_bio_avatar_size = apply_filters( 'd4jtheme_author_bio_avatar_size', 56 );

							echo get_avatar( get_the_author_meta( 'user_email' ), $author_bio_avatar_size );
							?>
						</div>
						<?php if ( get_the_author_meta( 'description' ) ) : ?>
							<?php the_author_meta( 'description' ); ?>
						<?php endif; ?>
					</div>
					<div class="archive-list">
						<?php $i=1; ?>
						<?php while ( have_posts() ) : the_post(); ?>
							<div class="archive-list-item">
								<?php if ( has_post_thumbnail() ){ ?>
									<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('cat-thumbnails'); ?></a>
								<?php } ?>								
								<div class="post-intro">
									<h2><a title="<?php the_title(); ?>" rel="bookmark" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
									<?php the_excerpt(); ?>
								</div>
								<span class="author"><a rel="author" title="Posts by <?php echo get_the_author() ; ?>" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><?php echo get_the_author() ; ?></a></span>
								<span class="date"><?php echo get_the_date(); ?></span>
								<span class="categories">Posted in <?php the_category(', '); ?></span>
							</div>
						<?php $i++; endwhile; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="fivecol last right-column">
				<?php if ( dynamic_sidebar('right-col') ) : else : endif; ?>
			</div>
		</div>
	</div>
<?php get_footer(); ?>
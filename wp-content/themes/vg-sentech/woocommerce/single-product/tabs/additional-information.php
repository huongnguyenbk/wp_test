<?php
/**
 * Additional Information tab
 * 
 * @author        WooThemes
 * @package       WooCommerce/Templates
 * @version       3.0.0
 */

if (! defined('ABSPATH')) {
	// Exit if accessed directly
	exit;
}
global $product;

$heading = apply_filters('woocommerce_product_additional_information_heading', esc_html__('Additional Information', 'vg-sentech'));
?>

<?php do_action( 'woocommerce_product_additional_information', $product ); ?>
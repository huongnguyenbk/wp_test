<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * VG AboutMe widget class
 *
 * @since 1.0
 */
class WP_Widget_VG_Contact extends WP_Widget {

    public function __construct() {
        $widget_ops = array(
						'classname' => 'widget_vg_contact',
						'description' => esc_html__( 'Contact widget.', 'vg-sentech' ) 
					);
        parent::__construct('VG_Contact', esc_html__('VG: Contact Me', 'vg-sentech' ), $widget_ops);
    }

    public function widget( $args, $instance ) {
		global $sentech_options;

        $title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

        echo $args['before_widget'];

        if ( $title ) {
            echo $args['before_title'] . esc_html($title ) . $args['after_title'];
        }

		$output = '';
        $output .= '<div class="vg-contact-content">';
        $output .= '<ul class="info">';
        $output .= ($instance['phone']) ? '<li class="sp-contact-phone"><i class="fa fa-phone"></i>'.$instance['phone'].'</li>' : '';
        $output .= ($instance['email']) ? '<li class="sp-contact-email"><i class="fa fa-envelope"></i>'.$instance['email'].'</li>' : '';
        $output .= '</ul>';			
		
        $output .= '</div>';

        echo $output;

        echo $args['after_widget'];

    }

    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] 			= strip_tags($new_instance['title']);
        $instance['phone'] 			= strip_tags($new_instance['phone']);
        $instance['email']			= strip_tags($new_instance['email']);

		
        return $instance;
    }

    public function form( $instance ) {
		
		$defaults = array(
						'title' => esc_html__('About sentech', 'vg-sentech'),
						'phone' => esc_html__('(+800) 123 456 7890', 'vg-sentech'), 
						'email' => esc_html__('Contact@Domain.Com', 'vg-sentech'),
					);
		$instance = wp_parse_args( (array) $instance, $defaults );
        $title = strip_tags($instance['title']);
		
        ?>

        <p><label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php esc_html_e( 'Title:', 'vg-sentech' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>

        <p><label for="<?php echo esc_attr($this->get_field_id( 'phone' )); ?>"><?php esc_html_e( 'Phone:', 'vg-sentech' ); ?></label>
            <input  id="<?php echo esc_attr($this->get_field_id( 'phone' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'phone' )); ?>" type="text" value="<?php echo esc_attr( $instance['phone']); ?>" class="widefat" /></p>

        <p><label for="<?php echo esc_attr($this->get_field_id( 'email' )); ?>"><?php esc_html_e( 'Email:', 'vg-sentech' ); ?></label>
            <input  id="<?php echo esc_attr($this->get_field_id( 'email' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'email' )); ?>" type="text" value="<?php echo esc_attr($instance['email']); ?>" class="widefat" /></p>

        <?php
		
		
    }

}


register_widget( 'WP_Widget_VG_Contact' );
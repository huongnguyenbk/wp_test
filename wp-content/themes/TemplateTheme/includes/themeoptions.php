<?php
add_action( 'admin_init', 'theme_options_init' );
add_action( 'admin_menu', 'theme_options_add_page' ); 

function theme_options_init(){
 register_setting( 'd4jtheme_options', 'd4j_theme_options');
} 

function theme_options_add_page() {
 add_theme_page( __( 'Theme Options', 'd4jtheme' ), __( 'Theme Options', 'd4jtheme' ), 'edit_theme_options', 'theme_options', 'theme_options_do_page' );
}
function theme_options_do_page() { 
	global $select_options; 
	if ( ! isset( $_REQUEST['settings-updated'] ) ) $_REQUEST['settings-updated'] = false; 
	?>
	<div>
		<?php echo "<h2>". __( 'D4J Theme Options', 'd4jtheme' ) . "</h2>"; ?>
		<?php if ( false !== $_REQUEST['settings-updated'] ) : ?>
			<div><p><strong><?php _e( 'Options saved', 'd4jtheme' ); ?></strong></p></div>
		<?php endif; ?>
		<form method="post" action="options.php" enctype="multipart/form-data">
			<?php settings_fields( 'd4jtheme_options' ); ?>  
			<?php $options = get_option( 'd4j_theme_options' ); ?> 
			<table>
				<tr valign="top"><th scope="row"><?php echo 'Logo'; ?></th>
				<td>
					<input type="text" id="logo_url" name="d4j_theme_options[logo]" value="<?php echo esc_url( $options['logo'] ); ?>" />
					<input id="upload_logo_button" type="button" class="button" value="<?php _e( 'Upload Logo', 'd4jtheme' ); ?>" />
					<span class="description"><?php _e('Upload an image for the logo.', 'd4jtheme' ); ?></span>
					<div id="upload_logo_preview" style="min-height: 100px;">
						<?php if(esc_url( $options['logo'] )){ ?>
							<img style="max-width:100%;" src="<?php echo esc_url( $options['logo'] ); ?>" />
						<?php }else{ ?>
							<img style="max-width:100%;" src="<?php echo get_template_directory_uri().'/images/logo.png'; ?>" />
						<?php } ?>
					</div>
				</td>
				</tr>
				<tr valign="top">
					<th scope="row"><?php echo 'Color Scheme'; ?></th>
					<td>
						<select id="d4j_theme_options[colorscheme]" class="large-text" name="d4j_theme_options[colorscheme]">
							<option value="default">Default</option>
							<option value="blue" <?php if($options[colorscheme] =='blue') echo 'selected'; ?>>Blue</option>
							<option value="darkblue" <?php if($options[colorscheme] =='darkblue') echo 'selected'; ?>>Dark Blue</option>
							<option value="green" <?php if($options[colorscheme] =='green') echo 'selected'; ?>>Green</option>
							<option value="darkgreen" <?php if($options[colorscheme] =='darkgreen') echo 'selected'; ?>>Dark Green</option>
							<option value="lightgreen" <?php if($options[colorscheme] =='lightgreen') echo 'selected'; ?>>Light Green</option>
							<option value="orange" <?php if($options[colorscheme] =='orange') echo 'selected'; ?>>Orange</option>
							<option value="purple" <?php if($options[colorscheme] =='purple') echo 'selected'; ?>>Purple</option>
						</select>
						<label style="color:#003600;font-weight:bold;" for="d4j_theme_options[colorscheme]">Select a color scheme</label> 
					</td>
				</tr>
				<tr valign="top">
					<th scope="row"><?php echo 'Select category for Left Bar'; ?></th>
					<td>
						<?php wp_dropdown_categories( "show_count=1&hierarchical=1&name=d4j_theme_options[leftcat]" ); ?>
						<label style="color:#003600;font-weight:bold;" for="d4j_theme_options[leftcat]">Select a category</label> 
					</td>
				</tr>
				<tr valign="top">
					<th scope="row"><?php echo 'Google analytic'; ?></th>
					<td>
						<input type="text" id="googleanalytic" name="d4j_theme_options[googleanalytic]" rows="4" cols="50" value="<?php echo $options['googleanalytic']; ?>" />
						<label style="color:#003600;font-weight:bold;" for="d4j_theme_options[googleanalytic]">Google analytic ID only example: UA-67626933-1</label> 
					</td>
				</tr>
			</table> 
			<p>
			<input type="submit" value="<?php echo 'Save Options'; ?>" />
			</p>
		</form>
	</div>
<?php }


function d4jtheme_options_enqueue_scripts() {
	wp_register_script( 'd4jtheme-upload', get_template_directory_uri() .'/js/d4jtheme-upload.js', array('jquery','media-upload','thickbox') );
	if ( 'appearance_page_theme_options' == get_current_screen() -> id ) {
		wp_enqueue_script('jquery');
 
		wp_enqueue_script('thickbox');
		wp_enqueue_style('thickbox');
 
		wp_enqueue_script('media-upload');
		wp_enqueue_script('d4jtheme-upload');
 
	}
 
}
add_action('admin_enqueue_scripts', 'd4jtheme_options_enqueue_scripts');

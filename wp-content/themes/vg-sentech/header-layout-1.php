<?php
/**
 * @version    1.1
 * @package    VG Sentech
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->

<head>

<?php
	$sentech_options  = sentech_get_global_variables(); 
	$woocommerce 	= sentech_get_global_variables('woocommerce'); 	
	$sentech_options['mini_cart_title'] = isset($sentech_options['mini_cart_title']) ? $sentech_options['mini_cart_title'] : "";
	$sentech_options['page_style'] = isset($sentech_options['page_style']) ? $sentech_options['page_style'] : ""; 
	$sentech_options['sentech_loading'] = isset($sentech_options['sentech_loading']) ? $sentech_options['sentech_loading'] : ""; 
	$sentech_options['share_head_code'] = isset($sentech_options['share_head_code']) ? $sentech_options['share_head_code'] : "";	
	
	
?>

<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<?php if (isset($sentech_options['share_head_code']) && $sentech_options['share_head_code']!='') {
	echo wp_kses($sentech_options['share_head_code'], array(
		'script' => array(
			'type' 	=> array(),
			'src' 	=> array(),
			'async' => array()
		),
	));
} ?>

<?php 
	// HieuJa get Preset Color Option
	$presetopt = sentech_get_preset();
	// HieuJa end block
?>

<?php wp_head(); ?>
</head>

<!-- Body Start Block -->
<body <?php body_class(); ?>>

<!-- Page Loader Block -->
<?php if (isset($sentech_options['sentech_loading']) && $sentech_options['sentech_loading']) : ?>
	<div id="pageloader">
		<div id="loader"></div>
		<div class="loader-section left"></div>
		<div class="loader-section right"></div>
	</div>
<?php endif; ?>

<div id="yith-wcwl-popup-message"><div id="yith-wcwl-message"></div></div>
<div class="wrapper <?php if(isset($sentech_options['page_style']) && $sentech_options['page_style']=='box'){echo 'box-layout';}?>">

	<!-- Top Header -->
	<div class="top-wrapper">
		<div class="header-container">
			<?php if (is_active_sidebar('sidebar-vg-top-header')) : ?>
			<div class="top-bar">
				<div class="container">
					<div id="top">
						<div class="row">
							<?php dynamic_sidebar('sidebar-vg-top-header'); ?>
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
			
			<div class="header">
				<div class="container">
					<div class="row">
						
						<div id="sp-logo" class=" col-sm-12 col-xs-12 <?php if(class_exists('Custom_Ajax_search_widget') && class_exists('WC_Widget_Cart')){ echo "col-lg-4 col-md-4"; }else{ echo "vg-logo-center";} ?>">
						<?php if(isset($sentech_options['logo_main']['url']) && !empty($sentech_options['logo_main']['url'])){ ?>
							<div class="logo">
								<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
									<img src="<?php echo esc_url($sentech_options['logo_main']['url']); ?>" alt="" />
								</a>
							</div>
						<?php } else if( isset($sentech_options['logo_text']) && !empty($sentech_options['logo_text'])) {?>
							<h1 class="logo"><a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><?php echo esc_html($sentech_options['logo_text']); ?></a></h1>
						<?php } else { ?>
							<div class="logo">
								<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
									<img src="<?php echo esc_url(get_template_directory_uri() . '/images/presets/preset'. $presetopt .'/logo.png' ); ?>" alt="" />
								</a>
							</div>
						<?php } ?>
						</div>																
							
						<?php if(class_exists('Custom_Ajax_search_widget') || class_exists('WC_Widget_Cart')) : ?>
						<div id="sp-search-cart" class="col-lg-8 col-md-8 col-sm-12 col-xs-12 <?php echo (class_exists('Custom_Ajax_search_widget') && class_exists('WC_Widget_Cart')) ? 'media' : ''; ?>">
							
							<?php if (class_exists('Custom_Ajax_search_widget')) { ?>
							<div id="sp-search" class="<?php echo (class_exists('WC_Widget_Cart')) ? 'media-body' : ''; ?>">
								<div class="vg-search">
									  <?php ajax_autosuggest_form(); ?>
								</div>		
							</div>		
							<?php } ?>
							
							
							<?php if (class_exists('WC_Widget_Cart')) { ?>
							<div class="sp-cart <?php echo (class_exists('Custom_Ajax_search_widget')) ? 'media-right' : ''; ?>">
								<?php the_widget('Custom_WC_Widget_Cart'); ?>
							</div>
							<?php } ?>
							
						</div>
						<?php endif; ?>
					
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Menu -->
	<div class="sp-main-menu-wrapper home1">
		<div class="container">
			<div class="wrapper-main-menu">
				<div class="row">
					<?php if(is_active_sidebar('category-page')){?>
						<div class="category-main-menu col-xs-12 col-sm-12 col-md-3">
							<?php dynamic_sidebar('category-page'); ?>
						</div>
					<?php }; ?>
					<div class="menu-wrapper col-xs-12 col-sm-12 <?php if(is_active_sidebar('category-page') &&  (isset($sentech_options['vg_top_contact_show']) && !empty($sentech_options['vg_top_contact_show']))){ echo "col-md-6"; } else if( is_active_sidebar('category-page') || (isset($sentech_options['vg_top_contact_show']) && !empty($sentech_options['vg_top_contact_show']))){ echo "col-md-9"; } else{ echo "col-md-12"; } ?>">		
						<div id="menu-container">
							<div id="header-menu">
								<div class="header-menu visible-large">
									<?php echo wp_nav_menu(array("theme_location" => "primary","")); ?>
								</div>
								<div class="visible-small">
									<div class="mbmenu-toggler"><span class="title"><?php echo esc_html($sentech_options['title_mobile_menu']); ?></span><span class="mbmenu-icon"></span></div>
									<div class="nav-container">
										<?php wp_nav_menu(array('theme_location' => 'mobilemenu', 'container_class' => 'mobile-menu-container', 'menu_class' => 'nav-menu')); ?>
									</div>
								</div>
							</div>					
						</div>			
					</div>
					<?php if(isset($sentech_options['vg_top_contact_show']) && $sentech_options['vg_top_contact_show']) :?>
						<div id="sp-call" class="col-xs-12 col-sm-12 col-md-3">								
							<div class="call_sp">
								<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
									<img src="<?php echo esc_url(get_template_directory_uri() . '/images/presets/preset'. $presetopt .'/icon-call.png' ); ?>" alt="" />
								</a>						
								<div class="sp-top-contact">
									<?php if( isset($sentech_options['vg_top_contact']) && $sentech_options['vg_top_contact'] ){ echo $sentech_options['vg_top_contact'];} ?>
								</div>												
							</div>
						</div>				
					<?php endif;?>
					
				</div>	
			</div>
		</div>
	</div>
<?php 
/**
* The template for displaying page
* @package d4jtheme
* @subpackage d4jtheme
* @since d4jtheme 1.0
*/
get_header(); ?>
	<div class="container main">
		<div class="row">
			<div class="col middle-column">
				<?php while ( have_posts() ) : the_post(); ?>
					<div class="article">
						<div class="article-heading">
							<h2><?php the_title() ; ?></h2>
						</div>
						<div class="article-body">
							<div class="article-intro">
								<?php the_content(); ?>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
			<div class="fivecol last right-column">
				<?php if ( dynamic_sidebar('right-col') ) : else : endif; ?>
			</div>
		</div>
	</div>
<?php get_footer(); ?>
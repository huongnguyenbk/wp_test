<?php
/**
 * Template Name: Shop Template
 *
 * Description: Shop Template
 *
* @package    VG Sentech
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */
if (! defined('ABSPATH')) exit; // Exit if accessed directly

sentech_get_header(); ?>
<?php
	$sentech_options  = sentech_get_global_variables(); 
	$sentech_productrows  = sentech_get_global_variables('sentech_productrows'); 
?>
<div class="main-container">
	<div class="container">
		<?php sentech_breadcrumb(); ?>
		<div class="page-content row">
				<?php if( isset($sentech_options['sidebar_pos']) && $sentech_options['sidebar_pos']=='left') :?>
					<?php get_sidebar('category'); ?>
				<?php endif; ?>
				<div id="archive-product" class="col-xs-12 <?php if (is_active_sidebar('sidebar-category')) : ?>col-md-9<?php endif; ?>">
					<div class="default-shop">
						<?php while (have_posts()) : the_post(); ?>
							<?php get_template_part('content', 'page'); ?>
						<?php endwhile; // end of the loop. ?>
					</div>
				</div>
				<?php if( isset($sentech_options['sidebar_pos']) && $sentech_options['sidebar_pos']=='right') :?>
					<?php get_sidebar('category'); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer('shop'); ?>
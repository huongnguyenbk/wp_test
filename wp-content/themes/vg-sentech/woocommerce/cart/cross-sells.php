
<?php
/**
 * Cross-sells
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if (! defined('ABSPATH')) exit; // Exit if accessed directly



if ( $cross_sells ) : ?>

	<div class="cross-sells">
		<div class="vg-title">
			<h3><?php echo isset($sentech_options['crosssells_title']) ? esc_html($sentech_options['crosssells_title']) : "Base on your selection, you may be interested in the following items:"; ?></h3>
		</div>
		<!--<p><?php esc_html__('Base on your selection, you may be interested in the 
		following items:', 'vg-sentech');?></p>-->
		<div class="cross-carousel">
			<?php woocommerce_product_loop_start(); ?>

			<?php foreach ( $cross_sells as $cross_sell ) : ?>

				<?php
				 	$post_object = get_post( $cross_sell->get_id() );

					setup_postdata( $GLOBALS['post'] =& $post_object );

					wc_get_template_part( 'content', 'product' ); ?>

			<?php endforeach; ?>

		<?php woocommerce_product_loop_end(); ?>
		</div>
	</div>

<?php endif;

wp_reset_postdata();
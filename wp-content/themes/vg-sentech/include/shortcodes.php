<?php
//Shortcodes for Visual Composer

add_action('vc_before_init', 'sentech_vc_shortcodes');
function sentech_vc_shortcodes() {
	
	//Swallow Code Custom elements Visual Composer
	if ( is_plugin_active( 'vg-woocarousel/vg-woocarousel.php' ) ) {
		$args = array(
			'post_type' => 'vgwc',
			'posts_per_page' => -1,
		);

		$vgwc = new WP_Query($args);
		
		if ($vgwc->have_posts()) : 
			while ($vgwc->have_posts()) : $vgwc->the_post(); 
				$vgwctitle[get_the_title()]= get_the_ID();
			endwhile;
		endif;
		$vgwctitle = (isset($vgwctitle)) ? $vgwctitle : '';
	}	
	
	if ( is_plugin_active( 'vg-postcarousel/vg-postcarousel.php' ) ) {
		$args = array(
			'post_type' => 'vgpc',
			'posts_per_page' => -1,
		);

		$vgpc = new WP_Query($args);
		
		if ($vgpc->have_posts()) : 
			while ($vgpc->have_posts()) : $vgpc->the_post(); 
				$vgpctitle[get_the_title()]= get_the_ID();
			endwhile;
		endif;
		$vgwctitle = (isset($vgwctitle)) ? $vgwctitle : '';
	}
	if ( is_plugin_active( 'mega_main_menu/mega_main_menu.php' ) ) {
		foreach ( get_registered_nav_menus() as $key => $value ){
			$key = str_replace( ' ', '-', $key );
			$theme_menu_locations[ $key ] = $key;
		}
		foreach ( get_nav_menu_locations() as $key => $value ){
			$key = str_replace( ' ', '-', $key );
			$theme_menu_locations[ $key ] = $key;
		}
	}
	//End Swallow Code Custom elements Visual Composer
	
	if ( is_plugin_active( 'mega_main_menu/mega_main_menu.php' ) ) {
		//VG MegaMenu && Custom Menu
		vc_map(array(
			'name' => esc_html__('VG MegaMenu', 'vg-sentech'),
			'base' => 'vgmegamenu',
			'icon' => 'icon-wpb-vg icon-wpb-megamenu',
			'class' => '',
			'category' => esc_html__('VG Sentech', 'vg-sentech'),
			'description' => esc_html__('VG MegaMain Extensions Replace', 'vg-sentech'),
			'admin_label' => true,
			'params' => array(
				array(
					'type' => 'textfield',
					'heading' =>  esc_html__( 'Widget title', 'vg-sentech' ),
					'param_name' => 'title',
					'description' =>  esc_html__( 'What text use as a widget title. Leave blank to use default widget title.', 'vg-sentech' ),
				),
				array(
					'type' => 'css_editor',
					'heading' =>  esc_html__( 'CSS box', 'vg-sentech' ),
					'param_name' => 'css',
					'group' =>  esc_html__( 'Design Options', 'vg-sentech' ),
				),
				array(
					'heading' =>  esc_html__( 'Select Mega Menu Desktop', 'vg-sentech' ),
					'description' =>  esc_html__( 'Select Mega menu in desktop.', 'vg-sentech' ),
					'param_name' => 'mega_menu',
					'type' => 'dropdown',
					'value' => $theme_menu_locations,
					'admin_label' => true,
					'save_always' => true,
				),	
				array(
					'heading' =>  esc_html__( 'Select Treeview Menu Mobile', 'vg-sentech' ),
					'description' => empty( $custom_menus ) ?  esc_html__( 'Custom menus not found. Please visit <b>Appearance > Menus</b> page to create new menu.', 'vg-sentech' ) :  esc_html__( 'Select menu to display.', 'vg-sentech' ),
					'param_name' => 'nav_menu',
					'type' => 'dropdown',
					'value' => $theme_menu_locations,
					'admin_label' => true,
					'save_always' => true,
				),	
				array(
					'type' => 'textfield',
					'heading' =>  esc_html__( 'Extra class name', 'vg-sentech' ),
					'param_name' => 'el_class',
					'description' =>  esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'vg-sentech' ),
				),
			)
		));
	}
	//Brand logos
	vc_map(array(
		'name' => esc_html__('Brand Logos', 'vg-sentech'),
		'base' => 'ourbrands',
		'icon' => 'icon-wpb-vg icon-wpb-ourbrands',
		'class' => '',
		'category' => esc_html__('VG Sentech', 'vg-sentech'),
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' =>  esc_html__( 'Widget title', 'vg-sentech' ),
				'param_name' => 'title',
				'description' =>  esc_html__( 'Enter text used as widget title (Note: located above content element).', 'vg-sentech' ),
			),
			array(
				'type' => 'css_editor',
				'heading' =>  esc_html__( 'CSS box', 'vg-sentech' ),
				'param_name' => 'css',
				'group' =>  esc_html__( 'Design Options', 'vg-sentech' ),
			),
			array(
				'type' => 'dropdown',
				'holder' => 'div',
				'class' => '',
				'heading' => esc_html__('Number of rows', 'vg-sentech'),
				'param_name' => 'rowsnumber',
				'value' => array(
					'one'	=> 'one',
					'two'	=> 'two',
					'four'	=> 'four',
				),
			),
			array(
				'type' 		 => 'textfield',
				'heading' 	 =>  esc_html__( 'Items Visible:', 'vg-sentech' ),
				'param_name' => 'items_visible',
				'value'		 => '4',
			),		
			array(
				'type' 		 => 'textfield',
				'heading' 	 =>  esc_html__( 'Desktop - Column Number:', 'vg-sentech' ),
				'param_name' => 'desktop_number',
				'value' 	 =>	 esc_html__('1170,3', 'vg-sentech'),
			),		
			array(
				'type' 		 => 'textfield',
				'heading' 	 =>  esc_html__( 'S Desktop - Column Number:', 'vg-sentech' ),
				'param_name' => 'sdesktop_number',
				'value' 	 =>	 esc_html__('992,2', 'vg-sentech'),
			),		
			array(
				'type' 		 => 'textfield',
				'heading' 	 =>  esc_html__( 'Tablet - Column Number:', 'vg-sentech' ),
				'param_name' => 'tablet_number',
				'value' 	 =>	 esc_html__('800,2', 'vg-sentech'),
			),		
			array(
				'type' 		 => 'textfield',
				'heading' 	 =>  esc_html__( 'S Tablet - Column Number:', 'vg-sentech' ),
				'param_name' => 'stablet_number',
				'value' 	 =>	 esc_html__('650,2', 'vg-sentech'),
			),		
			array(
				'type' 		 => 'textfield',
				'heading' 	 =>  esc_html__( 'Mobile - Column Number:', 'vg-sentech' ),
				'param_name' => 'mobile_number',
				'value' 	 =>	 esc_html__('599,1', 'vg-sentech'),
			),
			array(
				'type' => 'checkbox',
				'heading' =>  esc_html__( 'Show Icon?', 'vg-sentech' ),
				'param_name' => 'add_show_icon',
				'description' =>  esc_html__( 'Show Icon.', 'vg-sentech' ),
				'value' => array(  esc_html__( 'Yes', 'vg-sentech' ) => 'Yes' ),
				'save_always' => true,
			),
			array(
				'type' => 'dropdown',
				'heading' =>  esc_html__( 'Icon library', 'vg-sentech' ),
				'value' => array(
					 esc_html__( 'Font Awesome', 'vg-sentech' ) => 'fontawesome',
					 esc_html__( 'Open Iconic', 'vg-sentech' ) => 'openiconic',
					 esc_html__( 'Typicons', 'vg-sentech' ) => 'typicons',
					 esc_html__( 'Entypo', 'vg-sentech' ) => 'entypo',
					 esc_html__( 'Linecons', 'vg-sentech' ) => 'linecons',
				),
				'save_always' => true,
				'admin_label' => true,
				'param_name' => 'type',
				'description' =>  esc_html__( 'Select icon library.', 'vg-sentech' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' =>  esc_html__( 'Icon', 'vg-sentech' ),
				'param_name' => 'icon_fontawesome',
				'value' => 'fa fa-adjust', // default value to backend editor admin_label
				'save_always' => true,
				'settings' => array(
					'emptyIcon' => true,
					// default true, display an 'EMPTY' icon?
					'iconsPerPage' => 4000,
					// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'fontawesome',
				),
				'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'vg-sentech' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' =>  esc_html__( 'Icon', 'vg-sentech' ),
				'param_name' => 'icon_openiconic',
				'value' => 'vc-oi vc-oi-dial', // default value to backend editor admin_label
				'save_always' => true,
				'settings' => array(
					'emptyIcon' => false, // default true, display an 'EMPTY' icon?
					'type' => 'openiconic',
					'iconsPerPage' => 4000, // default 100, how many icons per/page to display
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'openiconic',
				),
				'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'vg-sentech' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' =>  esc_html__( 'Icon', 'vg-sentech' ),
				'param_name' => 'icon_typicons',
				'value' => 'typcn typcn-adjust-brightness', // default value to backend editor admin_label
				'save_always' => true,
				'settings' => array(
					'emptyIcon' => false, // default true, display an 'EMPTY' icon?
					'type' => 'typicons',
					'iconsPerPage' => 4000, // default 100, how many icons per/page to display
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'typicons',
				),
				'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'vg-sentech' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' =>  esc_html__( 'Icon', 'vg-sentech' ),
				'param_name' => 'icon_entypo',
				'value' => 'entypo-icon entypo-icon-note', // default value to backend editor admin_label
				'save_always' => true,
				'settings' => array(
					'emptyIcon' => false, // default true, display an 'EMPTY' icon?
					'type' => 'entypo',
					'iconsPerPage' => 4000, // default 100, how many icons per/page to display
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'entypo',
				),
			),
			array(
				'type' => 'iconpicker',
				'heading' =>  esc_html__( 'Icon', 'vg-sentech' ),
				'param_name' => 'icon_linecons',
				'value' => 'vc_li vc_li-heart', // default value to backend editor admin_label
				'save_always' => true,
				'settings' => array(
					'emptyIcon' => false, // default true, display an 'EMPTY' icon?
					'type' => 'linecons',
					'iconsPerPage' => 4000, // default 100, how many icons per/page to display
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'linecons',
				),
				'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'vg-sentech' ),
			),
			array(
				'type' => 'textfield',
				'heading' =>  esc_html__( 'Extra class name', 'vg-sentech' ),
				'param_name' => 'el_class',
				'description' =>  esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'vg-sentech' ),
			),
		)
	));
	
	//NEWSLETTER
	vc_map(array(
		'name' => esc_html__('Newsletter', 'vg-sentech'),
		'base' => 'newsletter',
		'class' => '',
		'category' => esc_html__('VG Sentech', 'vg-sentech'),
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' =>  esc_html__( 'Newsletter title', 'vg-sentech' ),
				'param_name' => 'title',
				'description' =>  esc_html__( 'Enter text used as widget title (Note: located above content element).', 'vg-sentech' ),
			),
			array(
				'type' => 'css_editor',
				'heading' =>  esc_html__( 'CSS box', 'vg-sentech' ),
				'param_name' => 'css',
				'group' =>  esc_html__( 'Design Options', 'vg-sentech' ),
			),
			array(
				'type' => 'textfield',
				'heading' =>  esc_html__( 'Newsletter form ID', 'vg-sentech' ),
				'param_name' => 'newsletter_id',
				'description' =>  esc_html__( 'The form ID of MailPoet plugin.).', 'vg-sentech' ),
				"value" => '1',
			),
			array(
				'type' => 'checkbox',
				'heading' =>  esc_html__( 'Show Social', 'vg-sentech' ),
				'param_name' => 'add_show_social',
				'description' =>  esc_html__( 'Show Social bot.', 'vg-sentech' ),
				'value' => array(  esc_html__( 'Yes', 'vg-sentech' ) => 'Yes' ),
				'save_always' => true,
			),
			array(
				'type' => 'textfield',
				'heading' =>  esc_html__( 'Extra class name', 'vg-sentech' ),
				'param_name' => 'el_class',
				'description' =>  esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'vg-sentech' ),
			),
		)
	));
	
	//Testimonials
	vc_map( array(
		"name" => esc_html__( "Testimonials", "vg-sentech" ),
		"base" => "woothemes_testimonials",
		"class" => "",
		"category" => esc_html__( "VG Sentech", "vg-sentech"),
		"params" => array(
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => esc_html__( "Number of testimonial", "vg-sentech" ),
				"param_name" => "limit",
				"value" => esc_html__( "10", "vg-sentech" ),
			),
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => esc_html__( "Image size", "vg-sentech" ),
				"param_name" => "size",
				"value" => esc_html__( "120", "vg-sentech" ),
			),
		)
	) );
	
	//Swallow Code Custom elements Visual Composer
	if ( is_plugin_active( 'vg-woocarousel/vg-woocarousel.php' ) ) {
		//List VG WooCarousel
		vc_map(array(
			'name' => esc_html__('VG WooCarousel', 'vg-sentech'),
			'base' => 'listvgwccarousel',
			'icon' => 'icon-wpb-vg icon-wpb-woocarousel',
			'class' => '',
			'category' => esc_html__('VG Sentech', 'vg-sentech'),
			'description' => esc_html__('VG WooCarousel Extensions', 'vg-sentech'),
			'params' => array(
				array(
					'type' => 'textfield',
					'heading' =>  esc_html__( 'Widget title', 'vg-sentech' ),
					'param_name' => 'title',
					'description' =>  esc_html__( 'Enter text used as widget title (Note: located above content element).', 'vg-sentech' ),
				),
				array(
					'type' => 'css_editor',
					'heading' =>  esc_html__( 'CSS box', 'vg-sentech' ),
					'param_name' => 'css',
					'group' =>  esc_html__( 'Design Options', 'vg-sentech' ),
				),
				array(
					'type' => 'dropdown',
					'holder' => 'div',
					'class' => '',
					'heading' => esc_html__('VG WooCarousel', 'vg-sentech'),
					'param_name' => 'alias',
					'admin_label' => true,
					'value' => (isset($vgwctitle)) ? $vgwctitle : '',
					'save_always' => true,
					'description' =>  esc_html__( 'Select your VG WooCarousel.', 'vg-sentech' ),
				),
				array(
					'type' => 'checkbox',
					'heading' =>  esc_html__( 'Show Icon?', 'vg-sentech' ),
					'param_name' => 'add_show_icon',
					'description' =>  esc_html__( 'Show Icon.', 'vg-sentech' ),
					'value' => array(  esc_html__( 'Yes', 'vg-sentech' ) => 'Yes' ),
					'save_always' => true,
				),
				array(
					'type' => 'dropdown',
					'heading' =>  esc_html__( 'Icon library', 'vg-sentech' ),
					'value' => array(
						 esc_html__( 'Font Awesome', 'vg-sentech' ) => 'fontawesome',
						 esc_html__( 'Open Iconic', 'vg-sentech' ) => 'openiconic',
						 esc_html__( 'Typicons', 'vg-sentech' ) => 'typicons',
						 esc_html__( 'Entypo', 'vg-sentech' ) => 'entypo',
						 esc_html__( 'Linecons', 'vg-sentech' ) => 'linecons',
					),
					'save_always' => true,
					'admin_label' => true,
					'param_name' => 'type',
					'description' =>  esc_html__( 'Select icon library.', 'vg-sentech' ),
				),
				array(
					'type' => 'iconpicker',
					'heading' =>  esc_html__( 'Icon', 'vg-sentech' ),
					'param_name' => 'icon_fontawesome',
					'value' => 'fa fa-adjust', // default value to backend editor admin_label
					'save_always' => true,
					'settings' => array(
						'emptyIcon' => true,
						// default true, display an 'EMPTY' icon?
						'iconsPerPage' => 4000,
						// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'fontawesome',
					),
					'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'vg-sentech' ),
				),
				array(
					'type' => 'iconpicker',
					'heading' =>  esc_html__( 'Icon', 'vg-sentech' ),
					'param_name' => 'icon_openiconic',
					'value' => 'vc-oi vc-oi-dial', // default value to backend editor admin_label
					'save_always' => true,
					'settings' => array(
						'emptyIcon' => false, // default true, display an 'EMPTY' icon?
						'type' => 'openiconic',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'openiconic',
					),
					'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'vg-sentech' ),
				),
				array(
					'type' => 'iconpicker',
					'heading' =>  esc_html__( 'Icon', 'vg-sentech' ),
					'param_name' => 'icon_typicons',
					'value' => 'typcn typcn-adjust-brightness', // default value to backend editor admin_label
					'save_always' => true,
					'settings' => array(
						'emptyIcon' => false, // default true, display an 'EMPTY' icon?
						'type' => 'typicons',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'typicons',
					),
					'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'vg-sentech' ),
				),
				array(
					'type' => 'iconpicker',
					'heading' =>  esc_html__( 'Icon', 'vg-sentech' ),
					'param_name' => 'icon_entypo',
					'value' => 'entypo-icon entypo-icon-note', // default value to backend editor admin_label
					'save_always' => true,
					'settings' => array(
						'emptyIcon' => false, // default true, display an 'EMPTY' icon?
						'type' => 'entypo',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'entypo',
					),
				),
				array(
					'type' => 'iconpicker',
					'heading' =>  esc_html__( 'Icon', 'vg-sentech' ),
					'param_name' => 'icon_linecons',
					'value' => 'vc_li vc_li-heart', // default value to backend editor admin_label
					'save_always' => true,
					'settings' => array(
						'emptyIcon' => false, // default true, display an 'EMPTY' icon?
						'type' => 'linecons',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'linecons',
					),
					'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'vg-sentech' ),
				),
				array(
					'type' => 'textfield',
					'heading' =>  esc_html__( 'Extra class name', 'vg-sentech' ),
					'param_name' => 'el_class',
					'description' =>  esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'vg-sentech' ),
				),
			)
		));
	}
	
	if ( is_plugin_active( 'vg-postcarousel/vg-postcarousel.php' ) ) {
		//List VG PostCarousel
		vc_map(array(
			'name' => esc_html__('VG PostCarousel', 'vg-sentech'),
			'base' => 'listvgpccarousel',
			'icon' => 'icon-wpb-vg icon-wpb-postcarousel',
			'class' => '',
			'category' => esc_html__('VG Sentech', 'vg-sentech'),
			'description' => esc_html__('VG PostCarousel Extensions', 'vg-sentech'),
			'params' => array(
				array(
					'type' => 'textfield',
					'heading' =>  esc_html__( 'Widget title', 'vg-sentech' ),
					'param_name' => 'title',
					'description' =>  esc_html__( 'Enter text used as widget title (Note: located above content element).', 'vg-sentech' ),
				),
				array(
					'type' => 'css_editor',
					'heading' =>  esc_html__( 'CSS box', 'vg-sentech' ),
					'param_name' => 'css',
					'group' =>  esc_html__( 'Design Options', 'vg-sentech' ),
				),
				array(
					'type' => 'dropdown',
					'holder' => 'div',
					'class' => '',
					'heading' => esc_html__('VG WooCarousel', 'vg-sentech'),
					'param_name' => 'alias',
					'admin_label' => true,
					'value' => $vgpctitle,
					'save_always' => true,
					'description' =>  esc_html__( 'Select your VG WooCarousel.', 'vg-sentech' ),
				),
				array(
					'type' => 'checkbox',
					'heading' =>  esc_html__( 'Show Icon?', 'vg-sentech' ),
					'param_name' => 'add_show_icon',
					'description' =>  esc_html__( 'Show Icon.', 'vg-sentech' ),
					'value' => array(  esc_html__( 'Yes', 'vg-sentech' ) => 'Yes' ),
					'save_always' => true,
				),
				array(
					'type' => 'dropdown',
					'heading' =>  esc_html__( 'Icon library', 'vg-sentech' ),
					'value' => array(
						 esc_html__( 'Font Awesome', 'vg-sentech' ) => 'fontawesome',
						 esc_html__( 'Open Iconic', 'vg-sentech' ) => 'openiconic',
						 esc_html__( 'Typicons', 'vg-sentech' ) => 'typicons',
						 esc_html__( 'Entypo', 'vg-sentech' ) => 'entypo',
						 esc_html__( 'Linecons', 'vg-sentech' ) => 'linecons',
					),
					'save_always' => true,
					'admin_label' => true,
					'param_name' => 'type',
					'description' =>  esc_html__( 'Select icon library.', 'vg-sentech' ),
				),
				array(
					'type' => 'iconpicker',
					'heading' =>  esc_html__( 'Icon', 'vg-sentech' ),
					'param_name' => 'icon_fontawesome',
					'value' => 'fa fa-adjust', // default value to backend editor admin_label
					'save_always' => true,
					'settings' => array(
						'emptyIcon' => true,
						// default true, display an 'EMPTY' icon?
						'iconsPerPage' => 4000,
						// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'fontawesome',
					),
					'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'vg-sentech' ),
				),
				array(
					'type' => 'iconpicker',
					'heading' =>  esc_html__( 'Icon', 'vg-sentech' ),
					'param_name' => 'icon_openiconic',
					'value' => 'vc-oi vc-oi-dial', // default value to backend editor admin_label
					'save_always' => true,
					'settings' => array(
						'emptyIcon' => false, // default true, display an 'EMPTY' icon?
						'type' => 'openiconic',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'openiconic',
					),
					'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'vg-sentech' ),
				),
				array(
					'type' => 'iconpicker',
					'heading' =>  esc_html__( 'Icon', 'vg-sentech' ),
					'param_name' => 'icon_typicons',
					'value' => 'typcn typcn-adjust-brightness', // default value to backend editor admin_label
					'save_always' => true,
					'settings' => array(
						'emptyIcon' => false, // default true, display an 'EMPTY' icon?
						'type' => 'typicons',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'typicons',
					),
					'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'vg-sentech' ),
				),
				array(
					'type' => 'iconpicker',
					'heading' =>  esc_html__( 'Icon', 'vg-sentech' ),
					'param_name' => 'icon_entypo',
					'value' => 'entypo-icon entypo-icon-note', // default value to backend editor admin_label
					'save_always' => true,
					'settings' => array(
						'emptyIcon' => false, // default true, display an 'EMPTY' icon?
						'type' => 'entypo',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'entypo',
					),
				),
				array(
					'type' => 'iconpicker',
					'heading' =>  esc_html__( 'Icon', 'vg-sentech' ),
					'param_name' => 'icon_linecons',
					'value' => 'vc_li vc_li-heart', // default value to backend editor admin_label
					'save_always' => true,
					'settings' => array(
						'emptyIcon' => false, // default true, display an 'EMPTY' icon?
						'type' => 'linecons',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'linecons',
					),
					'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'vg-sentech' ),
				),
				array(
					'type' => 'textfield',
					'heading' =>  esc_html__( 'Extra class name', 'vg-sentech' ),
					'param_name' => 'el_class',
					'description' =>  esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'vg-sentech' ),
				),
			)
		));
	}
	//End Swallow Code Custom elements Visual Composer
}
?>